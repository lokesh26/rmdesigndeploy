Ex_InfoBox.prototype = new google.maps.OverlayView();

	/** @constructor */
	function Ex_InfoBox(options, map) {

  // Initialize all properties.
  this.options_ = options;
  this.map_ = map;

  // Define a property to hold the text's div. We'll
  // actually create this div upon receipt of the onAdd()
  // method so we'll leave it null for now.
  this.div_ = null;

  // Explicitly call setMap on this overlay.
  this.setMap(map);
}

Ex_InfoBox.prototype.onAdd = function() {

	var div = document.createElement('div');
	div.style.position = 'absolute';
	div.className = "ex_infobox";

  // Create the img element and attach it to the div.
  var ib_text = document.createElement('div');
  ib_text.style.width = '100%';
  ib_text.style.height = '100%';
  ib_text.innerHTML = this.options_.content;
  div.appendChild(ib_text);

  this.div_ = div;

  // Add the element to the "overlayLayer" pane.
  var panes = this.getPanes();
  panes.floatPane.appendChild(div);
};

Ex_InfoBox.prototype.draw = function() {

  // We use the south-west and north-east
  // coordinates of the overlay to peg it to the correct position and size.
  // To do this, we need to retrieve the projection from the overlay.
  var overlayProjection = this.getProjection();

  // Retrieve the south-west and north-east coordinates of this overlay
  // in LatLngs and convert them to pixel coordinates.
  // We'll use these coordinates to resize the div.
  var coordinates = overlayProjection.fromLatLngToDivPixel(this.options_.latLng);

  // Resize the image's div to fit the indicated dimensions.
  var div = this.div_;
  div.style.left = coordinates.x + 18 + 'px';
  div.style.top = coordinates.y - 14 + 'px';
};

Ex_InfoBox.prototype.setContent = function (content) {
	this.textContainer.innerHTML(content);
}

// The onRemove() method will be called automatically from the API if
// we ever set the overlay's map property to 'null'.
Ex_InfoBox.prototype.onRemove = function() {
	this.div_.parentNode.removeChild(this.div_);
	this.div_ = null;
};

// Map icons - shades
var Markers = {
    whiteIcon: {
        'path': google.maps.SymbolPath.CIRCLE,
        'strokeWeight': 2,
        'strokeColor': "#000000",
        'fillColor': "#ffffff",
        'fillOpacity': 1,
        'scale': 5
    },

    redIcon: {
        'path': google.maps.SymbolPath.CIRCLE,
        'strokeWeight': 2,
        'strokeColor': "#ffffff",
        'fillColor': "#fe5435",
        'fillOpacity': 1,
        'scale': 9
    }
};

function RM_Marker(markerOptions) {
    this.markerData = markerOptions;
    this._div = null;

    this.setMap(this.markerData.map);
}

RM_Marker.prototype = new google.maps.OverlayView();

RM_Marker.prototype.onAdd = function () {
    var self = this;
    var div;
    div = document.createElement('div');

    //google.maps.event.addDomListener(div, 'mousedown', cancelEvent); //Cancel Drag & Click
    //google.maps.event.addDomListener(div, 'click', cancelEvent); //Cancel Click
    //google.maps.event.addDomListener(div, 'dblclick', cancelEvent); //Cancel Double Click
    //google.maps.event.addDomListener(div, 'contextmenu', cancelEvent); //Cancel Double Right Click

    // Create the DIV representing our CustomMarker
    div.style.position = "absolute";
    div.style.cursor = 'pointer';
    div.style.zIndex = "300";

    var imgWrap = document.createElement('div');
    imgWrap.className = "imgwrap";
    imgWrap.style = "position: relative;";
    div.appendChild(imgWrap);

    this.img = document.createElement('img');
    this.isActive = !this.markerData.isActive;
    this.toggleActive();
    imgWrap.appendChild(this.img);

    var markerLabel = document.createElement('div');
    markerLabel.innerHTML = this.markerData.iconText;
    markerLabel.className = "count-map-loc";
    markerLabel.style = "position: absolute; top: 8px; left: 13px; color: #ffffff; font-weight: bold;";
    imgWrap.appendChild(markerLabel);

    google.maps.event.addDomListener(div, "click", function (event) {
        google.maps.event.trigger(self, "click");
    });

    google.maps.event.addDomListener(div, 'mouseover', function () {
        this.style.zIndex = "10000";
    });

    google.maps.event.addListener(div, 'mouseout', function () {
        this.style.zIndex = "300";
    });

    this._div = div;

    // Then add the marker to the DOM
    var panes = this.getPanes();
    panes.overlayImage.appendChild(div);
}

RM_Marker.prototype.toggleActive = function () {
    if (this.isActive) {
        this.img.src = "../images/marker-2.png";
        this.isActive = false;
    } else {
        this.img.src = "../images/marker-1.png";
        this.isActive = true;
    }
}

RM_Marker.prototype.setActive = function () {
    this.img.src = "../images/marker-1.png";
    this.isActive = true;
}

RM_Marker.prototype.setInactive = function () {
    if(!this.img) return;
    
    this.img.src = "../images/marker-2.png";
    this.isActive = false;
}

RM_Marker.prototype.draw = function () {
    // Position the marker
    var pagePositions = this.getProjection().fromLatLngToDivPixel(this.markerData.latlng);
    if (pagePositions) {
        this._div.style.left = parseInt(pagePositions.x) - 0 + 'px';
        this._div.style.top = parseInt(pagePositions.y) - 25 + 'px';
        this._div.style.cursor = "default";
        if (this.markerData.enPan && this.newPanFlag) { this.panMap(this); this.newPanFlag = false; }
    }
}

RM_Marker.prototype.onRemove = function () {
    if (this._div) {
        this._div.parentNode.removeChild(this._div);
        this._div = null;
    }
}

RM_Marker.prototype.setLatlng = function (latlng) {
    this.markerData.latlng = latlng;
}

RM_Marker.prototype.getPosition = function () {
    return this.markerData.latlng;
}

RM_Marker.prototype.panMap = function (self) {
    var pagePositions = self.getProjection().fromLatLngToContainerPixel(this.markerData.latlng);

    var bounds = this.markerData.map.getBounds();
    var NE = bounds.getNorthEast();
    var SW = bounds.getSouthWest();

    var pNE = self.getProjection().fromLatLngToContainerPixel(NE);
    var pSW = self.getProjection().fromLatLngToContainerPixel(SW);

    if ((pagePositions.x - 150) < 0) dx = pagePositions.x - 150;
    else if ((pagePositions.x + this._div.offsetWidth + 150) > pNE.x) dx = (pagePositions.x - pNE.x) + this._div.offsetWidth + 150;
    else dx = 0;

    if ((pagePositions.y - 150) < 0) dy = pagePositions.y - 150;
    else if ((pagePositions.y + this._div.offsetHeight + 150) > pSW.y) dy = (pagePositions.y - pSW.y) + this._div.offsetHeight + 150;
    else dy = 0;

    if (dx != 0 || dy != 0) {
        this.markerData.map.panBy(dx, dy);
    }
}
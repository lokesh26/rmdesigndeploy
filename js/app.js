// Production Server default
Config = {
    baseUrl: "http://www.roadmojo.com:3031",
    isPublicPage: false,
    googleClientId: "218340750479-g9b8m86ba3dnimcrpg1t426q7cdo1k37.apps.googleusercontent.com",
    fbClientId: "280725785381144",
    //fbClientId: "817545481613065",    
    //baseUrl: "http://localhost:3000"
};

// Staging Server
var domainUrl = document.domain;
if (document.domain == "staging.roadmojo.com" || document.domain == "54.88.153.27"){
    Config = {
        baseUrl: "http://staging.roadmojo.com:3031",
        isPublicPage: false,
        googleClientId: "218340750479-g9b8m86ba3dnimcrpg1t426q7cdo1k37.apps.googleusercontent.com",
        fbClientId: "660712930715759",
        //baseUrl: "http://localhost:3000"
    };
}

var registerHandlebarsHelpers = function(){
    if (typeof Handlebars !== "undefined" && Handlebars) {
        Handlebars.registerHelper('if_all', function() {
            var args = [].slice.apply(arguments);
            var opts = args.pop();

            var fn = opts.fn;
            for(var i = 0; i < args.length; ++i) {
                if(args[i])
                    continue;
                fn = opts.inverse;
                break;
            }
            return fn(this);
        });
    }
};

function callback(data) {
    console.log(data);
}


// Roadmojo JavaScript

$(window).load(init);
    
function init() {
    // initialize foundation
    $(document).foundation();
    

    common.fillProfileData();

    switch (window.rmojo.id) {
        case "landing_page":
            signup.initialize(true);
            signin.initialize(true);
            forgotPassword.initialize();
            landing_page.initialize();
            Config.isPublicPage = true;
            break;

        case "feed":
            feed.initialize();
            break;

        case "me_trips":
            me_trips.initialize(false);
            signup.initialize();
            signin.initialize();
            forgotPassword.initialize();
            break;

        case "me_followingPlaces":
            me_followingPlaces.initialize();
            break;

        case "profile_edit":
            profile_edit.initialize();
            break;

        case "profile_changePassword":
            profile_changePassword.initialize();
            break;

        case "profile_notifications":
            profile_notifications.initialize();
            break;

        case "me_followingFindInvite":
            me_followingFindInvite.initialize();
            break;

        case "profile_activity":
            profile_activity.initialize();
            break;

        case "explore":
            explore.initialize();
            signup.initialize();
            signin.initialize();
            forgotPassword.initialize();
            break;

        case "road_trip_view":
            road_trip_view.initialize();
            signup.initialize();
            signin.initialize();
            forgotPassword.initialize();
            break;

        case "faq_page":
            signup.initialize();
            signin.initialize();
            forgotPassword.initialize();
            break;

        case "how_page":
            signup.initialize();
            signin.initialize();
            forgotPassword.initialize();
            break;

        case "create_roadtrip":
            create_roadtrip.initialize();
            break;
    }

    common.initialize();
    registerHandlebarsHelpers();
    $('.__c_socialsoon').tooltipster({
        position: 'top',
        interactive: true,
        content: $('<span>Planned for next release.<br> You can sign up using email <br> or Facebook.</span>'),
        functionReady: function () {
            $(window).trigger("resize");
        }
    });


    $(window).resize(function () {
        $('.tooltipster,.tooltipstersort').tooltipster('reposition');
    });
    $(window).trigger("resize");

    $('.social-fb').click(function(ev) {
        ev.preventDefault();
            // $('#fbSignupModal').foundation('reveal', 'open');
        landing_page.fbSignIn();
    });

}

var utilities = {
    //Fill a HTML <select></select> with list of values in JavaScript array
    fillDropDown: function($el, data, name, value) {
        for (var i = 0; i < data.length; i++) {
            /**
             * Fix #2: Updated method to create elements using jQuery, rather than raw HTML
             */
            $('<option/>').text(data[i][name]).attr('value', data[i][value]).appendTo($($el));
        }
    },

    //Fill a HTML <select></select> with list of values in JavaScript data object
    fillDropDown2: function($el, data, nameKey, valueKey) {
        $.each(data, function(key, value) {
            if (valueKey) key = value[valueKey];
            if (nameKey) value = value[nameKey];
            $('<option/>').text(value).attr('value', key).appendTo($($el));
        });
    },

    //Fill a custom HTML dropdowns with list of values in JavaScript data object
    fillDropDown3: function($el, data, nameKey, valueKey) {
        $.each(data, function(key, value) {
            if (valueKey) key = value[valueKey];
            if (nameKey) value = value[nameKey];
            $('<li/>').text(value).data('value', key).appendTo($($el));
        });
    },

    //Fill a custom HTML dropdowns with list of values in JavaScript array
    fillDropDown4: function($el, data, name, value) {
        for (var i = 0; i < data.length; i++) {
            $('<li/>').text(data[i][name]).data('value', data[i][value]).appendTo($($el));
        }
    },

    // Set first element of custom dropdown as selected
    setFirstActive: function($ul) {
        var $cSelectWrapper = $($ul).parent().parent(),
            $firstLi = $($ul).find("li").first();

        $firstLi.addClass("active");
        $cSelectWrapper.find(".selected span:first-child").text($firstLi.text());
        $cSelectWrapper.find(".selected input[type='hidden']").val($firstLi.data('value'));
    },

    //Method to create a HTML DOM element
    createElement: function(element, parent, attributes) {
        /**
         * @params
         *
         * element
         * Type: Selector
         * e.g. "<div/>"
         * String name of element which is required to be created
         *
         * parent
         * Type: String | Element
         * e.g. "#foo", $("#foo"), document.getElementById('foo')
         * Parent element of the newly created element
         *
         * attributes
         * Type: Object
         * e.g. {'className': 'bar', html: "Lorem Ipsum", id: "foo"}
         * Object of attributes to be applied to this element
         */

        if (!attributes) attributes = {};

        //create specified element
        var $el = $(element, attributes);

        //append this element to parent, if available
        if (parent) $el.appendTo($(parent));

        //return the newly created element
        return $el;
    },

    //Method to create JavaScript object of all fields in a <form></form>
    formToObject: function(form) {
        /*
         * Fix #3: Updated method to use jQuery rather that raw HTML
         */
        var $form = $(form),
            json = {};

        //Get Inputs
        $('input', $form).each(function(index, el) {
            var $el = $(el);

            //stay away, if element is disabled
            if ($el.is(":disabled")) return;

            name = $el.attr("name");
            switch ($el.attr("type")) {
                case 'checkbox':
                    //For checkboxes, we need to send values false/true rather than 'on', which is natively provided by HTML
                    $el.prop("checked") ? json[name] = true : json[name] = false;
                    break;

                case 'radio':
                    if ($el.prop("checked")) json[name] = $el.val();
                    break;

                default:
                    json[name] = $el.val();
                    break;
            }
        });

        //Get Selects and textareas
        $('select,textarea', $form).each(function(index, el) {
            var $el = $(el);

            //stay away, if element is disabled
            if ($el.is(":disabled")) return;

            json[$el.attr("name")] = $el.val();
        });

        return json;
    },

    //Method for removal for multiple keys(in array) from a JavaScript Object
    dropKeysFromObject: function(obj, dropFields) {
        for (var i = 0; i < dropFields.length; i++) {
            if (obj[dropFields[i]]) delete obj[dropFields[i]];
        }

        return obj;
    },

    // load a javascript file from server
    loadJScript: function(fileName, callback) {
        var JSPATH = "/html/js/";

        $.getScript(JSPATH + fileName + ".js", callback);
    },

    //Method to load Google Maps API
    loadGMapScript: function(callback) {
        /*
         * Fix #4: Revised method to load Google Maps API - From Method 1 to Method 2
         *
         * Google Maps API can be loaded using 2 methods
         * Method 1 creates a script tag and appends to document.body, e.g. $('<script/>').attr('src', "http://maps.googleapis.com/maps/api/js?sensor=false").appendTo(document.body);
         * Method 2 requests Google Maps API via a jQuery getScript method and calls all dependent methods henceforth
         */
        $.getScript("http://maps.googleapis.com/maps/api/js?sensor=false", function(data, textStatus, jqxhr) {
            if (callback) callback();
        });
    },

    /**
     * Create a new map, using Google Maps API, which should already be imported
     * Google Maps URL: http://maps.googleapis.com/maps/api/js?sensor=false
     */
    createGMap: function(el) {
        //if (typeof google.maps === "undefined") throw new Error("Google Maps required, but not imported.");

        var mapOptions = {
            zoom: 2,
            center: new google.maps.LatLng(27.219721, 78.019480),
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            disableDefaultUI: true,
            scrollwheel: false
        };

        //This method returns the Google Map object
        return new google.maps.Map(el, mapOptions);
    },

    /**
     * Set Autocomplete with Google Places
     * Google Maps with Place URL: http://maps.googleapis.com/maps/api/js?sensor=false&libraries=places
     */
    setAutocompleteSearch: function(map, inputElement) {
        //if (typeof google.maps.places === "undefined") throw new Error("Google Places library required, but not imported.");

        var autocomplete = new google.maps.places.Autocomplete(inputElement);
        autocomplete.bindTo('bounds', map);

        return autocomplete;
    },

    /**
     * Get address of a location by its latitude and longitude. This method expects a callback which receives a formatted address string and address componets as params.
     *
     * Requires Google Maps
     * URL: http://maps.googleapis.com/maps/api/js?sensor=false
     */
    getAddressByLatLng: function(latitude, longitude, callback) {
        var geocoder = new google.maps.Geocoder();

        geocoder.geocode({
            'location': new google.maps.LatLng(latitude, longitude)
        }, function(geocoderResult, geocoderStatus) {
            if (geocoderStatus == google.maps.GeocoderStatus.OK) {
                callback(geocoderResult[0].formatted_address, geocoderResult[0].address_components);
            } else {
                console.log("Google Geocoding Error: " + geocoderStatus);
            }
        });
    },

    //read photo from <input type="file" /> element($inputElement) and display it in <img src="" />($imageElement) tag
    readAndDisplayPhoto: function($inputElement, $imageElement) {
        $inputElement = $($inputElement);

        if ($inputElement.prop('files') && $inputElement.prop('files')[0]) {
            var fileReader = new FileReader();
            fileReader.onload = function(ev) {
                $($imageElement).attr('src', ev.target.result);
            };
            fileReader.readAsDataURL($inputElement.prop('files')[0]);
        }
    },

    /*
     * this method uses an external plugin to set ellipsis
     * jquery.dot.dot.dot.min.js (https://github.com/FrDH/jQuery.dotdotdot)
     *
     * Provide a jQuery style selector to apply ellipsis
     */
    setdotdotdot: function(selector) {
        $(selector).dotdotdot({
            'ellipsis': "...",
            'wrap': "word",
            'fallbackToLetter': true,
            'after': null,
            'watch': true
        });
    },

    //convert JSON string to JavaScript object
    JSONtoObject: function(json) {
        if (JSON) return JSON.parse(json);
        else return eval('(' + json + ')');
    },

    //get cookie from browser, for key provided
    getCookieValue: function(key) {
        return decodeURIComponent(document.cookie.replace(new RegExp("(?:(?:^|.*;)\\s*" + encodeURIComponent(key).replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=\\s*([^;]*).*$)|^.*$"), "$1")) || null;
    },

    //convert a data array to object mapper
    createMapFromArray: function(dataArray, key) {
        var mapper = {};
        if (!key) key = "id";
        for (var i = 0, l = dataArray.length; i < l; i++) {
            mapper[dataArray[i][key]] = dataArray[i];
        }

        return mapper;
    },

    //get number of keys in object
    getObjectLength: function(a) {
        return $.map(a, function(n, i) {
            return i;
        }).length;
    },

    //compare first string with second, upto n number of characters
    strncmp: function(a, b, n) {
        return a.substring(0, n) == b.substring(0, n);
    },

    // get parameter value from URL
    getURLVar: function(name) {
        return (RegExp(name + '=' + '(.+?)(&|$)').exec(location.search) || [, null])[1];
    },

    /**
     * Hash a string using SHA256 algorithm
     *
     * Requires sha256.min.js
     * Crypto-js http://code.google.com/p/crypto-js/
     *
     * returns SHA256 hash
     */
    sha256: function(strval) {
        if (!CryptoJS.SHA256) {
            console.log("Crypto library SHA256 not included!");
            return false;
        }

        return CryptoJS.SHA256(strval).toString(CryptoJS.enc.Hex);
    },

    // extract bits from a 'value' from 'start_pos' to 'end_pos'
    extractBits: function(value, start_pos, end_pos) {
        var mask = (1 << (end_pos - start_pos)) - 1;
        return (value >> start_pos) & mask;
    },

    // scroll smoothly to top of page
    scrollToTop: function() {
        $("html, body").animate({
            scrollTop: 0
        }, "slow");
    },

    getFormattedDate: function(date) {
        if (date == null || date == "") return "";
        var m = date.match(/^(\d{1,2})\/(\d{1,2})\/(\d{4})$/);
        return (m[1] + "-" + m[2] + "-" + m[3]);
    },

    getDisplayDate: function(date) {
        if (!date) return "";
        var m = date.match(/^(\d{4})-(\d{1,2})-(\d{1,2})$/);
        return (m[3] + "/" + m[2] + "/" + m[1]);
    },

    cookies: {
        getItem: function(sKey) {
            return decodeURIComponent(document.cookie.replace(new RegExp("(?:(?:^|.*;)\\s*" + encodeURIComponent(sKey).replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=\\s*([^;]*).*$)|^.*$"), "$1")) || null;
        },

        //docCookies.setItem(name, value[, end[, path[, domain[, secure]]]])
        setItem: function(sKey, sValue, vEnd, sPath, sDomain, bSecure) {
            if (!sKey || /^(?:expires|max\-age|path|domain|secure)$/i.test(sKey)) {
                return false;
            }
            var sExpires = "";
            if (vEnd) {
                switch (vEnd.constructor) {
                    case Number:
                        sExpires = vEnd === Infinity ? "; expires=Fri, 31 Dec 9999 23:59:59 GMT" : "; max-age=" + vEnd;
                        break;
                    case String:
                        sExpires = "; expires=" + vEnd;
                        break;
                    case Date:
                        sExpires = "; expires=" + vEnd.toUTCString();
                        break;
                }
            }
            document.cookie = encodeURIComponent(sKey) + "=" + encodeURIComponent(sValue) + sExpires + (sDomain ? "; domain=" + sDomain : "") + (sPath ? "; path=" + sPath : "") + (bSecure ? "; secure" : "");
            return true;
        },

        removeItem: function(sKey, sPath, sDomain) {
            if (!sKey || !this.hasItem(sKey)) {
                return false;
            }
            document.cookie = encodeURIComponent(sKey) + "=; expires=Thu, 01 Jan 1970 00:00:00 GMT" + (sDomain ? "; domain=" + sDomain : "") + (sPath ? "; path=" + sPath : "");
            return true;
        },

        hasItem: function(sKey) {
            return (new RegExp("(?:^|;\\s*)" + encodeURIComponent(sKey).replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=")).test(document.cookie);
        },

        keys: function() {
            var aKeys = document.cookie.replace(/((?:^|\s*;)[^\=]+)(?=;|$)|^\s*|\s*(?:\=[^;]*)?(?:\1|$)/g, "").split(/\s*(?:\=[^;]*)?;\s*/);
            for (var nIdx = 0; nIdx < aKeys.length; nIdx++) {
                aKeys[nIdx] = decodeURIComponent(aKeys[nIdx]);
            }
            return aKeys;
        }
    },

    local_storage: {
        add: function(key, value) {
            localStorage.setItem(key, value);
        },

        get: function(key) {
            return localStorage.getItem(key);
        },

        remove: function(key) {
            localStorage.removeItem(key);
        },

        clear: function() {
            localStorage.clear();
        }
    },

    // Debounce calls to "callback" routine so that multiple calls
    // made to the event handler before the expiration of "delay" are
    // coalesced into a single call to "callback". Also causes the
    // wait period to be reset upon receipt of a call to the
    // debounced routine.
    //
    debounce: function(delay, callback) {
        var timeout = null;
        return function() {
            //
            // if a timeout has been registered before then
            // cancel it so that we can setup a fresh timeout
            //
            if (timeout) {
                clearTimeout(timeout);
            }
            var args = arguments;
            timeout = setTimeout(function() {
                callback.apply(null, args);
                timeout = null;
            }, delay);
        };
    }
};

var landing_page = {
    initialize: function() {
        var userID = utilities.local_storage.get("id");
        var hasusername = utilities.local_storage.get("has_username");
        if (userID !== null && userID.length > 0 && hasusername === true) {
            window.location = "/html/feed.html";
        }

        var self = this;

/*        $('.social-fb').click(function(ev) {
            ev.preventDefault();
            // $('#fbSignupModal').foundation('reveal', 'open');
            self.fbSignIn();
        });*/

        $(".social-gplus").click(function(ev) {
            ev.preventDefault();
            self.googleSignIn();
        });

        this.fetchTrendingTrips(me_trips.printFeeds);
/*        this.fetchTwitterFeed();*/


    },

    fbSignIn: function() {

        var self = this;

        FB.login(function(response) {
            if (response.authResponse) {
                var cookie_name = "fbsr_"+Config.fbClientId;

                var match = document.cookie.match(new RegExp(cookie_name + '=([^;]+)'));
                if(match === null) {
                    document.cookie = cookie_name + "=" + response.authResponse.signedRequest;
                }

                $.ajax({
                    type: "GET",
                    async: true,
                    dataType: "jsonp",
                    url: Config.baseUrl + "/users/auth/facebook/callback",
                    success: function(data) {
                        utilities.cookies.setItem("auth_token", data["auth_token"], 31536e3, "/");
                        signin.saveDataInLocalStorage(data);
                        if( !data.has_username) {
                            $('#fbSignupModal').foundation('reveal', 'open');
                            $(document).on('open.fndtn.reveal', '#fbSignupModal', function() {
                                document.getElementById("fbSignupForm").reset();
                                validator.resetForm();
                                $("#fbSignupForm").find("input").removeClass("has-error");
                            });

                            var validator = $("#fbSignupForm").validate({
                                highlight: function(element, errorClass, validClass) {
                                    $(element).addClass("has-error");
                                },
                                unhighlight: function(element, errorClass, validClass) {
                                    $(element).removeClass("has-error");
                                },
                                submitHandler: function(form) {
                                    self.checkUserName();
                                    return false;
                                }
                            });
                        } else {
                            if (window.rmojo.id === "landing_page") {
                                window.location.href = "html/feed.html"
                            }else{
                                window.location.href = "feed.html"
                            }
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        console.log("unable to fblogin the user " + errorThrown);
                    }
                });
            }
        }, { scope: 'email' });
    },

    checkUserName: function() {
        $.ajax({
            type: "POST",
            async: true,
            data: {
                'username': $("#fbSignupModal_username").val()
            },
            url: Config.baseUrl + "/users/" + utilities.local_storage.get('id') +  "/update_username",
            success: function(data, textStatus, jqXHR) {
                window.location.href = "html/feed.html";
            }
        });
    },

    twitterSignIn: function() {

        FB.login(function(response) {
            if (response.authResponse) {
                var cookie_name = "fbsr_"+Config.fbClientId;

                var match = document.cookie.match(new RegExp(cookie_name + '=([^;]+)'));
                if(match === null) {
                    document.cookie = cookie_name + "=" + response.authResponse.signedRequest;
                }

                $.ajax({
                    type: "GET",
                    async: true,
                    jsonp: "callback",                 
                    dataType: "jsonp",
                    url: Config.baseUrl + "/users/auth/twitter/callback",
                    success: function(data, textStatus, jqXHR) {                        
                        utilities.cookies.setItem("auth_token", data["auth_token"], 31536e3, "/");
                        signin.saveDataInLocalStorage(data);
                        window.location.href = "html/feed.html";
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        console.log("unable to fblogin the user " + errorThrown);
                    }
                });
            }
        }, { scope: 'email' });
    },

    googleSignIn: function(){
        var scope = 'https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile';
        gapi.auth.authorize({client_id: Config.googleClientId, scope: "email profile", response_type: 'code',immediate: true,cookie_policy: 'http://staging.roadmojo.com'}, function(result) {
            if(result && !result.error) {
                $.ajax({
                    type: "GET",
                    async: true,
                    jsonp: "callback",                    
                    dataType: "jsonp",
                    crossdomain: true,

                    url: Config.baseUrl + "/users/auth/google_oauth2/callback",
                    data: {code: result},
                    success: function(data, textStatus, jqXHR) {
                        utilities.cookies.setItem("auth_token", data["auth_token"], 31536e3, "/");
                        signin.saveDataInLocalStorage(data);
                        window.location.href = "html/feed.html";
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        // Handle error case
                    }
                });
            } else {
            // Handle unauthorized case
            }
        });
    },

    fetchTrendingTrips: function(fetchCallback) {
        // Auth token not required here
        $.ajax({
            type: "GET",
            async: true,
            data: {
                'count': 3
            },
            url: Config.baseUrl + "/trips/trending_trips",
            success: function(data, textStatus, jqXHR) {
                data = data.slice(0, 3);
                if (fetchCallback) fetchCallback.apply(me_trips, [data, "#tripsContainer", null , "/html/"]);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log("unable to fetch trending trips " + errorThrown);
            }
        });
    },

    // Fetch Twitter Feed for landing page
    fetchTwitterFeed: function(fetchCallback) {
        var self = this;

        // Auth token not required here
        $.ajax({
            type: "GET",
            async: true,
            url: Config.baseUrl + "/twitter_feeds",
            success: function(data, textStatus, jqXHR) {
                if (fetchCallback) fetchCallback.apply(self, [data]);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log("unable to fetch twitter feed " + errorThrown);
            }
        });
    }
};

var signup = {
    initialize: function(isIndex) {
        var self = this;

        if (isIndex) this.redirectTo = "html/" + this.redirectTo;

        $(document).on('open.fndtn.reveal', '#signupModal', function() {
            document.getElementById("signupForm").reset();
            validator.resetForm();
            $("#signupForm").find("input").removeClass("has-error");
        });

        var validator = $("#signupForm").validate({
            highlight: function(element, errorClass, validClass) {
                $(element).addClass("has-error");
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).removeClass("has-error");
            },
            submitHandler: function(form) {
/*                var $element = $("input[name='invite_code']");
                if( $element.val() == "ROADCODE013") {
                    self.doSignup();
                } else {
                    $element.addClass("has-error");
                }*/
                self.doSignup();
                return false;
            }
        });
    },

    redirectTo: "feed.html",

    doSignup: function() {
        var self = this,
            postData = utilities.formToObject("#signupForm");

        delete postData[""];

        $.ajax({
            type: "POST",
            async: true,
            data: {
                'user': postData
            },
            url: Config.baseUrl + "/users",
            success: function(data, textStatus, jqXHR) {
                $("#signupModal").foundation('reveal', 'close');

                common.notify({
                    'title': "Ahoy Roadtripper!",
                    'text': "You have signed up successfully. We have sent you an e-mail with the confirmation link. <br/> In case you don't recieve an email in the next 2 hours, please check your spam folder and then drop us an email to support@roadmojo.com",
                    'type': "success"
                });

            },
            error: function(jqXHR, textStatus, errorThrown) {
                var errors = jqXHR.responseJSON.errors || {},
                    errorMsg = (errors.email ? "Email " + errors.email : "") + "<br />" + (errors.username ? "Username " + errors.username : "") + "<br />" + (errors.password_confirmation ? "Password Confirmation " + errors.password_confirmation : "");

                errors.email && $("#signupForm").find("input[name='email']").addClass("has-error");
                errors.username && $("#signupForm").find("input[name='username']").addClass("has-error");
                if (errors.password_confirmation) {
                    $("#signupForm").find("input[type='password']").addClass("has-error").val("");
                }

                common.notify({
                    'title': "Error",
                    'text': errorMsg,
                    'type': "error"
                });
                console.log("unable to sign up " + errorThrown);
            }
        });
    }
};

var signin = {
    initialize: function(isIndex) {
        var self = this;

        if (isIndex) this.redirectTo = "html/" + this.redirectTo;

        $(document).on('open.fndtn.reveal', '#loginModal', function() {
            document.getElementById("signinForm").reset();
            validator.resetForm();
            $("#signinForm").find("input").removeClass("has-error");
        });

        var validator = $("#signinForm").validate({
            highlight: function(element, errorClass, validClass) {
                $(element).addClass("has-error");
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).removeClass("has-error");
            },
            submitHandler: function(form) {
                self.doSignin();

                return false;
            }
        });
    },

    redirectTo: "feed.html",

    doSignin: function() {
        var self = this,
            postData = utilities.formToObject("#signinForm");

        delete postData[""];

        $.ajax({
            type: "POST",
            async: true,
            data: postData,
            url: Config.baseUrl + "/sessions",
            success: function(data, textStatus, jqXHR) {
                utilities.cookies.setItem("auth_token", data["auth_token"], 31536e3, "/");
                self.saveDataInLocalStorage(data);
                window.location = self.redirectTo;
            },
            error: function(jqXHR, textStatus, errorThrown) {
                var errorMsg = jqXHR.responseJSON.errors || "";

                $("#signinForm").find("#email-field").addClass("has-error");
                $("#signinForm").find("#password-field").addClass("has-error").val("");

                common.notify({
                    'title': "Unable to Login",
                    'text': errorMsg,
                    'type': "error"
                });
                console.log("unable to sign in " + errorThrown);
            }
        });
    },

    saveDataInLocalStorage: function(userMeta) {
        $.each(userMeta, function(key, value) {
            switch (key) {
                case "suggested_locations":
                case "suggested_trips":
                case "notification_setting":
                    value = JSON.stringify(value);
                    break;
            }

            utilities.local_storage.add(key, value);
        });
    }
};

var forgotPassword = {
    initialize: function() {
        var self = this;

        $(document).on('open.fndtn.reveal', '#resetModal', function() {
            document.getElementById("resetPasswordForm").reset();
            validator.resetForm();
            $("#resetPasswordForm").find("input").removeClass("has-error");
        });

        var validator = $("#resetPasswordForm").validate({
            highlight: function(element, errorClass, validClass) {
                $(element).addClass("has-error");
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).removeClass("has-error");
            },
            submitHandler: function(form) {
                self.doReset();

                return false;
            }
        });
    },

    doReset: function() {
        var self = this,
            postData = utilities.formToObject("#resetPasswordForm");

        delete postData[""];

        $.ajax({
            type: "POST",
            async: true,
            data: postData,
            url: Config.baseUrl + "/users/forgot_password",
            success: function(data, textStatus, jqXHR) {
                $("#resetPasswordForm").foundation('reveal', 'close');
                common.notify({
                    'title': "Reset Successful!",
                    'text': "We have sent you an e-mail with the temporary password.",
                    'type': "success"
                });
            },
            error: function(jqXHR, textStatus, errorThrown) {
                common.notify({
                    'title': "Oops!",
                    'text': "Sorry, we were not able to reset your password.",
                    'type': "error"
                });
                console.log("unable to reset password " + errorThrown);
            }
        });
    }
};

/*var resetPassword = {
    initialize: function() {
        var self = this;

        var validator = $("#resetPasswordForm").validate({
            highlight: function(element, errorClass, validClass) {
                $(element).addClass("has-error");
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).removeClass("has-error");
            },
            submitHandler: function(form) {
                self.doReset();

                return false;
            }
        });
    },

    doReset: function() {
        var self = this,
            postData = utilities.formToObject("#resetPasswordForm");

        delete postData[""];

        $.ajax({
            type: "POST",
            async: true,
            data: {
                "user": postData
            },
            url: Config.baseUrl + "/users/f0f922e7cd190c48e8d417e4db954553/reset_password",
            success: function(data, textStatus, jqXHR) {
                window.location("../index.html");
            },
            error: function(jqXHR, textStatus, errorThrown) {
                common.notify({
                    'title': "Error",
                    'text': jqXHR.responseJSON.errors || "",
                    'type': "error"
                });
                console.log("unable to reset password " + errorThrown);
            }
        });
    }
};
*/
var feed = {
    initialize: function() {
        var self = this,
            _loc = utilities.getURLVar("_loc");

        switch (_loc) {
            case "nb":
                self.fetchNearbyTrips(me_trips.printFeeds);
                break;

            case "tr":
                self.fetchTrendingTrips(me_trips.printFeeds);
                break;

            case "lk":
                self.fetchLikedTrips(me_trips.printFeeds);
                break;

            case "rec":
                self.fetchSortedTrips("created_at", "desc", me_trips.printFeeds);
                break;

            case "gtl":
                self.fetchTripsForGlobalTimeline(me_trips.printFeeds);
                break;

            default:
                me_trips.initialize(true);
                break;
        }
        common.fetchProfileData(function(data) {
            signin.saveDataInLocalStorage(data);
            common.fillProfileData(data);   
            $(".follow-info-people").html( data.following + " people");
            $(".follow-info-places").html( data.following_locations + " places");
        });
        // For features, Coming Soon, notify user
        $(".__c_comingSoon").on("click", function(ev) {
            ev.preventDefault();
            ev.stopPropagation();

            common.notify({
                'title': "Coming Soon",
                'text': "This features is still work in progress. We will launch this in on of our future releases",
                'type': ""
            });
        });
    },

    fetchSortedTrips: function(sortBy, sortType, fetchCallback) {
        var self = this;

        $.ajax({
            type: "GET",
            async: true,
            headers: {
                "X-Auth-Token": utilities.cookies.getItem("auth_token")
            },
            url: Config.baseUrl + "/trips/sort?sort_by=" + sortBy + "&sort_type=" + sortType,
            success: function(data, textStatus, jqXHR) {
                if (fetchCallback) fetchCallback.apply(me_trips, [data]);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log("unable to fetch sorted feed " + errorThrown);
            }
        });
    },

    fetchTrendingTrips: function(fetchCallback) {
        var self = this;

        $.ajax({
            type: "GET",
            async: true,
            headers: {
                "X-Auth-Token": utilities.cookies.getItem("auth_token")
            },
            url: Config.baseUrl + "/trips/trending_trips",
            success: function(data, textStatus, jqXHR) {
                if (fetchCallback) fetchCallback.apply(me_trips, [data]);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log("unable to fetch trending trips " + errorThrown);
            }
        });
    },

    fetchNearbyTrips: function(fetchCallback) {
        var self = this;
        var argData = {lat: "", lng: ""};

        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(_showPosition, _handleError);
        } else {
            console.log("Geolocation is not supported by this browser or permission denied.");
            _fetchNearbyTrips();
        }

        function _showPosition(position) {
            argData.lat = position.coords.latitude;
            argData.lng = position.coords.longitude;
            _fetchNearbyTrips();
        }

        function _handleError(error) {
            _fetchNearbyTrips();
        }

        function _fetchNearbyTrips() {
            $.ajax({
                type: "GET",
                async: true,
                data: argData,
                headers: {
                    "X-Auth-Token": utilities.cookies.getItem("auth_token")
                },
                url: Config.baseUrl + "/trips/nearby_trips",
                success: function(data, textStatus, jqXHR) {
                    if (fetchCallback) fetchCallback.apply(me_trips, [data]);
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log("unable to fetch nearby trips " + errorThrown);
                }
            });
        }
    },

    fetchTripsForGlobalTimeline: function(fetchCallback) {
        var self = this;

        $.ajax({
            type: "GET",
            async: true,
            headers: {
                "X-Auth-Token": utilities.cookies.getItem("auth_token")
            },
            url: Config.baseUrl + "/trips",
            success: function(data, textStatus, jqXHR) {
                if (fetchCallback) fetchCallback.apply(me_trips, [data]);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log("unable to fetch global timeline trips " + errorThrown);
            }
        });
    }
};

var me_trips = {
    initialize: function(isFeedPage) {

        this._isFeedPage = isFeedPage;

        if (this._isFeedPage) {
            this.geturl += "/users/feed";
        } else {
            this.geturl += "/users/my_trips";
        }

        // for drafts page
        if (utilities.getURLVar("_loc") === "drafts") {
            this._isDraftsPage = true;

            var $roadtripTab = $("#roadtripTab"),
                $likedTripsTab = $("#likedTripsTab"),
                $draftsTab = $("#draftsTab");

            $roadtripTab.removeClass("current");
            $roadtripTab.find("span").removeClass("icon-pxl-roadtrip-black").addClass("icon-pxl-roadtrip-gray");

            $likedTripsTab.removeClass("current");
            $likedTripsTab.find("span").removeClass("icon-pxl-roadtrip-black").addClass("icon-pxl-roadtrip-gray");

            $draftsTab.addClass("current");
            $draftsTab.find("span:first").removeClass("icon-pxl-cloud-draft-gray").addClass("icon-pxl-cloud-draft-black");

            this.fetchDrafts(this.printFeeds);

            // Refresh User data
            common.fetchProfileData(function(data) {
                signin.saveDataInLocalStorage(data);
                common.fillProfileData(data);
            });
        } else if (utilities.getURLVar("_loc") === "liked") {

            var $roadtripTab = $("#roadtripTab"),
                $likedTripsTab = $("#likedTripsTab"),
                $draftsTab = $("#draftsTab");

            $roadtripTab.removeClass("current");
            $roadtripTab.find("span").removeClass("icon-pxl-roadtrip-black").addClass("icon-pxl-roadtrip-gray");

            $draftsTab.removeClass("current");
            $draftsTab.find("span").removeClass("icon-pxl-roadtrip-black").addClass("icon-pxl-roadtrip-gray");

            $likedTripsTab.addClass("current");
            $likedTripsTab.find("span:first").removeClass("icon-pxl-cloud-draft-gray").addClass("icon-pxl-cloud-draft-black");

            this.fetchLikedTrips(this.printFeeds);

            // Refresh User data
            common.fetchProfileData(function(data) {
                signin.saveDataInLocalStorage(data);
                common.fillProfileData(data);
            });
        } else if (utilities.getURLVar("_loc") === "otherProfile" && utilities.getURLVar("id") != utilities.local_storage.get("id")) { // for other's profile page
            var userID = utilities.getURLVar("id");

            this.geturl = Config.baseUrl + "/users/" + userID + "/trips";

            if (utilities.getURLVar("_pg") === "liked") {
                this.geturl = Config.baseUrl + "/users/" + userID + "/liked_trips";
            }

            this._isOtherProfilePage = true;

            $(".__c_privateSection").hide();
            $("#draftsTab,#activityTab,#recommendedTab").next().remove();
            $("#draftsTab,#activityTab,#recommendedTab").remove();

            this.fetchOtherUserData(userID, common.fillProfileData);
            this.fetchData(this.printFeeds);
        } else {
            this.fetchData(this.printFeeds);
            var self = this;
            // Refresh User data
            common.fetchProfileData(function(data) {
                signin.saveDataInLocalStorage(data);
                common.fillProfileData(data);
            
            });
        }

        var otherProfileId = utilities.getURLVar("id");
        if (typeof otherProfileId !== "undefined" && otherProfileId !== null && otherProfileId.length > 0) {
            $("#roadtripTab").attr("href","profile_view.html?_loc=otherProfile&id=" + otherProfileId);
            $("#followingPlacesTab").attr("href","follow_me.html?_loc=otherProfile&id=" + otherProfileId);
            $("#followingPeopleTab").attr("href","follow_me_find_and_invite.html?_loc=otherProfile&id=" + otherProfileId);
            $("#followingViewAllTab").attr("href","follow_me_find_and_invite.html?_loc=otherProfile&id=" + otherProfileId);
            $("#likedTripsTab").attr("href","profile_view.html?_loc=otherProfile&_pg=liked&id=" + otherProfileId);
        }

        // < Bug >
        //$(document).on('click', '.feed .photo', function(){
        //  window.location = "road_trip_view.html?id="+common.getTripId(this);
        //});
    },

    otherPrint: function(data) {
        this.printFeeds(data);
    },

    geturl: Config.baseUrl,

    _isFeedPage: false,

    // Flag to indicate if current page is a drafts page
    _isDraftsPage: false,

    // Flag to indicate if current page is other's profile
    _isOtherProfilePage: false,

    // create a feed HTML
    createFeed: function(feedDetails, linksPrefix) {
        var self = this;

        linksPrefix = linksPrefix || "";

        var viewLink = linksPrefix + "road_trip_view.html?id=" + feedDetails.friendly_id;

        if (this._isDraftsPage) viewLink = "#";


        var $feed = utilities.createElement("<div/>", null, {
            'class': "feed " + "trip-" + feedDetails.id,
            'data-trip-id': feedDetails.id
        });

        var $head = utilities.createElement("<div/>", $feed, {
            'class': "head clearfix"
        });

        if (utilities.local_storage.get("id") === null || typeof utilities.local_storage.get("id") == "undefined" ||
        utilities.local_storage.get("id") === "")
        {
            var $headLink = utilities.createElement("<a/>", $head, {
                'href': "#",
                'data-reveal-id': "loginModal"
            });

        }else{
            var $headLink = utilities.createElement("<a/>", $head, {
                'href': linksPrefix + "profile_view.html?_loc=otherProfile&id=" + feedDetails.user_id
            });
        }
        if (feedDetails.userPic) {
            utilities.createElement("<div/>", $headLink, {
                'class': "user-photo left",
                'style': "background-image: url('" + feedDetails.userPic + "')"
            });
        }else{         
            utilities.createElement("<div/>", $headLink, {
                'class': "user-photo left"
            });
        }
        if (feedDetails.userName!= null) {
            utilities.createElement("<span/>", $headLink, {
                'html': feedDetails.userName
            });
        }else{
            utilities.createElement("<span/>", $headLink, {
                'html': feedDetails.screenName
            });        
        }

        utilities.createElement("<span/>", $head, {
            'class': "views right",
            'html': feedDetails.minsAgo + " ago &bull; " + (feedDetails.noViews || "0") + " views"
        });

        var $photo = utilities.createElement("<div/>", null, {
            'class': "photo"
        }).appendTo(utilities.createElement("<a/>", $feed, {
            'href': viewLink
        }));

        if (feedDetails.tripPic)
            utilities.createElement("<img/>", $photo, {
                'src': feedDetails.tripPic,
                'alt': "photo"
            });
        else if (feedDetails.locations) {
            utilities.createElement("<img/>", $photo, {
                'src': "http://maps.googleapis.com/maps/api/staticmap?markers=color:red|" + feedDetails.locations.start_point.latitude + "," + feedDetails.locations.start_point.longitude + "|" + feedDetails.locations.end_point.latitude + "," + feedDetails.locations.end_point.longitude + "&size=432x240",
                'alt': "photo"
            });
        }

        if (feedDetails.user_id == utilities.local_storage.get('id')) {
            var $overlay = utilities.createElement("<div/>", $photo, {
                'class': "overlay"
            });

            if (this._isDraftsPage) utilities.createElement("<a/>", $overlay, {
                'href': linksPrefix + "create_road_trip.html?id=" + feedDetails.id,
                'class': "button button-edit tiny radius",
                'html': "<span class='icon-pxl icon-pxl-edit-white vam'></span>&nbsp;&nbsp;EDIT & PUBLISH"
            });
            else utilities.createElement("<a/>", $overlay, {
                'href': linksPrefix + "create_road_trip.html?id=" + feedDetails.id,
                'class': "button button-edit tiny radius",
                'html': "<span class='icon-pxl icon-pxl-edit-white vam'></span>&nbsp;&nbsp;EDIT"
            });

            var $deleteButton = utilities.createElement("<a/>", $overlay, {
                'href': "#",
                'class': "button button-delete tiny radius",
                'html': '<span class="icon-pxl icon-pxl-trash-white vam feed-ex"></span>',
                'data-reveal-id': "confirmDeleteModal"
            });
            $deleteButton.on("click", function() {
                common._confirmsDelete = {
                    callback: function() {
                        // Re-arrange items
                        $('.cols-wrapper').masonry('remove', $feed);

                        // Refresh User data
                        common.fetchProfileData(function(data) {
                            signin.saveDataInLocalStorage(data);
                            common.fillProfileData(data);
                        });
                    },

                    details: feedDetails
                };
            });
        }

        var $content = utilities.createElement("<div/>", $feed, {
            'class': "content"
        });

        utilities.createElement("<a/>", $content, {
            'href': viewLink,
            'class': "trip-title",
            'html': (feedDetails.tripName || "")
        });
        utilities.createElement("<a/>", $content, {
            'href': viewLink,
            'html': "<p>" + (feedDetails.tripDesc || "") + "</p>"
        });

        utilities.createElement("<div/>", $feed, {
            'class': "separator",
            'html': "<hr />"
        });

        var $foot = utilities.createElement("<div/>", $feed, {
            'class': "foot clearfix"
        });
        utilities.createElement("<span/>", $foot, {
            'html': (feedDetails.noMilestones || "0") + " milestones, " + (feedDetails.noMoments ? (feedDetails.noMoments + " moments, ") : "") + (feedDetails.noPhotos || "0") + " photos"
        });

        if (!this._isDraftsPage) {
            var $right = utilities.createElement("<span/>", $foot, {
                'class': "right"
            });
            var prependText = "";
            var appendText = "";
            if (linksPrefix == "/html/" || utilities.local_storage.get("id") === null ||
                typeof utilities.local_storage.get("id") == "undefined" ||
                utilities.local_storage.get("id") === "") {
                prependText = "<span href='#' data-reveal-id='loginModal' onclick='return false;'>";
                appendText = "</span>";

                utilities.createElement("<span/>", $right, {
                    'class': "like",
                    'html': prependText + '<span class="icon-pxl icon-pxl-like-gray has-hover-like vam"></span>&nbsp;<span class="likeCount">' + (feedDetails.likes || "0") + "</span>" + appendText
                });
                utilities.createElement("<span/>", $right, {
                    'class': "comment",
                    'html': prependText + '<span class="icon-pxl icon-pxl-comment-gray  has-hover-comment vam"></span>&nbsp;' + (feedDetails.comments || "0") + "</span>" + appendText
                });
            } else {
                utilities.createElement("<span/>", $right, {
                    'class': "like " + (feedDetails.isLiked ? "nav-text" : ""),
                    'html': prependText + '<span class="icon-pxl icon-pxl-like-' + (feedDetails.isLiked ? "red" : "gray") + ' has-hover-like vam"></span>&nbsp;<span class="likeCount">' + (feedDetails.likes || "0") + "</span>" + appendText
                });
                utilities.createElement("<span/>", $right, {
                    'class': "comment " + (feedDetails.hasCommented ? "dark-text" : ""),
                    'html': prependText + '<span class="icon-pxl icon-pxl-comment-' + (feedDetails.hasCommented ? "black" : "gray") + '  has-hover-comment vam"></span>&nbsp;<span>' + (feedDetails.comments || "0") + "</span>" + appendText
                });
            }
        }


        return $feed;
    },

    // AJAX call to fetch list of feeds
    fetchDrafts: function(fetchCallback) {
        var self = this;

        $.ajax({
            type: "GET",
            async: true,
            headers: {
                "X-Auth-Token": utilities.cookies.getItem("auth_token")
            },
            url: Config.baseUrl + "/users/my_drafts",
            success: function(data, textStatus, jqXHR) {
                if (fetchCallback) fetchCallback.apply(self, [data]);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log("unable to fetch drafts " + errorThrown);
            }
        });
    },

    fetchLikedTrips: function(fetchCallback) {
        var self = this;

        $.ajax({
            type: "GET",
            async: true,
            headers: {
                "X-Auth-Token": utilities.cookies.getItem("auth_token")
            },
            url: Config.baseUrl + "/users/" + utilities.local_storage.get("id") + "/liked_trips",
            success: function(data, textStatus, jqXHR) {
                if (fetchCallback) fetchCallback.apply(me_trips, [data]);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log("unable to fetch trending trips " + errorThrown);
            }
        });
    },

    // AJAX call to fetch list of feeds
    fetchData: function(fetchCallback) {
        var self = this;

        $.ajax({
            type: "GET",
            async: true,
            headers: {
                "X-Auth-Token": utilities.cookies.getItem("auth_token")
            },
            url: this.geturl,
            success: function(data, textStatus, jqXHR) {
                if (fetchCallback) fetchCallback.apply(self, [data]);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log("unable to fetch list of feeds/trips " + errorThrown);
            }
        });
    },

    // Fetch user data for other profile
    fetchOtherUserData: function(id, fetchCallback) {
        var self = this;

        $.ajax({
            type: "GET",
            async: true,
            headers: {
                "X-Auth-Token": utilities.cookies.getItem("auth_token")
            },
            url: Config.baseUrl + "/users/" + id,
            success: function(data, textStatus, jqXHR) {
                common.createFollowButton(".edit-button", {
                    'data': {
                        'id': id
                    }
                }, {
                    'data': {
                        'id': id
                    }
                }, !data.is_followed, data.is_followed, false);
                if (fetchCallback) fetchCallback.apply(self, [data]);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log("unable to fetch details of user " + errorThrown);
            }
        });
    },

    // Fetch feed data for other profile
    fetchOtherUserFeed: function(userID, fetchCallback) {
        var self = this;

        $.ajax({
            type: "GET",
            async: true,
            headers: {
                "X-Auth-Token": utilities.cookies.getItem("auth_token")
            },
            url: Config.baseUrl + "/users/" + userID + "/trips",
            success: function(data, textStatus, jqXHR) {
                if (fetchCallback) fetchCallback.apply(self, [data]);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log("unable to fetch feeds/trips for other user " + errorThrown);
            }
        });
    },

    // print list of feeds on page
    printFeeds: function(listOfFeeds, $container, feedClass, linksPrefix) {
        var $feed = null,
            l = listOfFeeds.length,
            i = 0;

        if (!$container) $container = '.cols-wrapper';
        $container = $($container);

        // clear all feed data, initially
        $container.html("");

        // In case there are no trips
        // Authored by Lokesh
        if (l <= 0 || l == undefined) {
            if (utilities.getURLVar("_loc") === "liked"){
                $(".__c_no_trips_like_msg").show();
                return;
            }
            else if (utilities.getURLVar("_loc") === "otherProfile" && utilities.getURLVar("id") != utilities.local_storage.get("id"))
            {   
                $(".__c_no_trips_others_msg").show();
                return;
            }
            else
            {
                $(".__c_no_trips_msg").show();
                return;
            }

        } 
        else {
            $(".__c_no_trips_msg").hide();
            $(".__c_no_trips_like_msg").hide();
            $(".__c_no_trips_others_msg").hide();
        }

        for (; i < l; i++) {
            $feed = this.createFeed(listOfFeeds[i], linksPrefix);

            // Apply additional feed class if available
            if (feedClass) $feed.addClass(feedClass);

            $container.append($feed);
        }

        // masonrize feeds
        var showMasonry = true;
        if ($container.data('show-masonry') == "0") {
            showMasonry = false;
        }

        if(showMasonry) {
            $container.imagesLoaded(function() {
                if ($container.data('masonry-active') === "1") {
                    $container.masonry('destroy'); // Destroy masonry
                    $container.data('masonry-active', "0");
                }

                $container.masonry({
                    // columnWidth: 160,
                    "gutter": 18,
                    itemSelector: '.feed'
                });
                $container.data('masonry-active', "1");

                $container.masonry('on', 'removeComplete', function(msnryInstance, removedItems) {
                    $container.masonry();
                });
            });
        }
    },

    likeTrip: function(tripID, likeCallback) {
        var self = this;

        $.ajax({
            type: "GET",
            async: true,
            headers: {
                "X-Auth-Token": utilities.cookies.getItem("auth_token")
            },
            url: Config.baseUrl + "/trips/" + tripID + "/like",
            success: function(data, textStatus, jqXHR) {
                if (likeCallback) likeCallback.apply(self, [data]);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log("unable to like trip " + errorThrown);
            }
        });
    }
};
/*me_followingFindInvite*/
var me_followingFindInvite = {
    initialize: function() {
        var self = this;

        // Quick inits
        this.initInvite();
        

        if (utilities.getURLVar("_loc") === "otherProfile" && utilities.getURLVar("id") != utilities.local_storage.get("id")) {
            var userID = utilities.getURLVar("id");
            this.followingPeopleUrl = Config.baseUrl + "/users/" + userID + "/following_people";

            this._isOtherProfilePage = true;

            $(".__c_privateSection").hide();
            $("#draftsTab,#activityTab,#recommendedTab").next().remove();
            $("#draftsTab,#activityTab,#recommendedTab").remove();

            me_trips.fetchOtherUserData(userID, common.fillProfileData);

            this.getListOfFollowersOthers(userID, this.displayFollowers);
            //this.fetchOtherUserFeed(userID, this.printFeeds);

            $("#roadtripTab").attr("href","profile_view.html?_loc=otherProfile&id=" + userID);
            $("#followingPlacesTab").attr("href","follow_me.html?_loc=otherProfile&id=" + userID);
            $("#followingPeopleTab").attr("href","follow_me_find_and_invite.html?_loc=otherProfile&id=" + userID);
            $("#followingViewAllTab").attr("href","follow_me_find_and_invite.html?_loc=otherProfile&id=" + otherProfileId);
            $("#likedTripsTab").attr("href","profile_view.html?_loc=otherProfile&_pg=liked&id=" + userID);
        } else {
            
            this.getListOfFollowers(this.displayFollowers);

            // Refresh User data
            common.fetchProfileData(function(data) {
                signin.saveDataInLocalStorage(data);
                common.fillProfileData(data);
            });
        }

        var otherProfileId = utilities.getURLVar("id");
        if (typeof otherProfileId !== "undefined" && otherProfileId !== null && otherProfileId.length > 0) {
            $("#profileEditButton").remove();
            $(".tabs dd:last-child").remove();
            $(".tabs dd:first-child").addClass("active");
            $(".tabs").removeClass('even-3').addClass('even-2');
            $("#roadtripTab").attr("href","profile_view.html?_loc=otherProfile&id=" + otherProfileId);
            $("#followingPlacesTab").attr("href","follow_me.html?_loc=otherProfile&id=" + otherProfileId);
            $("#followingPeopleTab").attr("href","follow_me_find_and_invite.html?_loc=otherProfile&id=" + otherProfileId);
            $("#followingViewAllTab").attr("href","follow_me_find_and_invite.html?_loc=otherProfile&id=" + otherProfileId);
            $("#likedTripsTab").attr("href","profile_view.html?_loc=otherProfile&_pg=liked&id=" + otherProfileId);
        } else {
            $(".tabs dd").removeClass("active");
            if( window.location.hash && window.location.hash == "#suggestedContainer") {
                $(".tabs dd:last-child").addClass("active");
                $(".tabs-content div:first-child").removeClass("active");
                $(".tabs-content div:last-child").addClass("active");

                $(".tabs-inner dd").removeClass("active");
                $(".tabs-inner dd:nth-child(3)").addClass("active");
                $("#suggestedContainer").siblings('div').removeClass('active');
                $("#suggestedContainer").addClass("active");
            } else {
                $(".tabs dd:first-child").addClass("active");
            }
        }

        // AJAX call goes HERE
        this.getListOfFollowingPeople(this.displayFollowingPeople);
        this.getListOfSuggestedUsers(this.displaySuggestedUsers);
    },

    followingPeople: [],

    followingPeopleUrl: Config.baseUrl + "/users/following",

    followers: [],

    suggestedUsers: [],

    // Create a search results row
    _createResultRow: function(resultDetails, followCallback, unfollowCallback) {
        var self = this;

        var follower_text = (resultDetails.followingCount);
        if (+follower_text === 1) follower_text = " Follower</span>";
        else follower_text = " Followers</span>";

        var roadtrip_text = (resultDetails.roadtripsCount);
        if (+roadtrip_text === 1) roadtrip_text = " <span>Roadtrip, </span><span>";
        else roadtrip_text = " <span>Roadtrips, </span><span>";

        var $row = utilities.createElement("<div/>", null, {
            'class': "content-wrapper following-people"
        });
        var $rowContainer = utilities.createElement("<div/>", $row, {
            'class': "row"
        });
        var $col1 = utilities.createElement("<div/>", $rowContainer, {
            'class': "small-1 medium-1 large-1 columns"
        });
        var $col2 = utilities.createElement("<div/>", $rowContainer, {
            'class': "small-11 medium-11 large-11 columns"
        });
        var $followingPeopleDetails = utilities.createElement("<div/>", $col2, {
            'class': "following-people-details"
        });
        if (resultDetails.userPic) {
             utilities.createElement("<a/>", $col1, {
                'href': "profile_view.html?_loc=otherProfile&id=" + resultDetails.id,
                'class': "user-photo",
                'style': "background-image: url('" + resultDetails.userPic + "')"
             });
         }
         else
         {
             utilities.createElement("<a/>", $col1, {
                'href': "profile_view.html?_loc=otherProfile&id=" + resultDetails.id,
                'class': "user-photo"
             });
         }
        if (resultDetails.name!= null) {
            utilities.createElement("<div/>", $followingPeopleDetails, {
                'class': "following-people-name",
                'html': "<a href=profile_view.html?_loc=otherProfile&id=" + resultDetails.id + " target='_blank'>" + (resultDetails.name || "") + "</a>"
            });            
        }else{
            utilities.createElement("<div/>", $followingPeopleDetails, {
                'class': "following-people-name",
                'html': "<a href=profile_view.html?_loc=otherProfile&id=" + resultDetails.id + " target='_blank'>" + (resultDetails.username) + "</a>"
            });            
        }

        utilities.createElement("<div/>", $followingPeopleDetails, {
            'class': "following-people-desc",
            'html': (resultDetails.desc || "")
        });
        utilities.createElement("<div/>", $followingPeopleDetails, {
            'class': "following-people-desc",
            'html': "<span>" + (resultDetails.tripMiles*1.6).toFixed() + " km, </span>" + resultDetails.roadtripsCount + roadtrip_text + resultDetails.followingCount + follower_text
        });

        common.createFollowButton($col2, {
            'data': resultDetails,
            'method': followCallback
        }, {
            'data': resultDetails,
            'method': followCallback
        }, !resultDetails.is_followed, resultDetails.is_followed);

        return $row;
    },

    // Get list of following people from server - Tab 1
    getListOfFollowingPeople: function(fetchCallback) {
        var self = this;

        // Fetch & display Currently Following People

        $.ajax({
            type: "GET",
            async: true,
            headers: {
                "X-Auth-Token": utilities.cookies.getItem("auth_token")
            },
            url: this.followingPeopleUrl,
            success: function(data, textStatus, jqXHR) {
                fetchCallback && fetchCallback.apply(self, [data]);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log("unable to fetch following people " + errorThrown);
            }
        });
    },

    // Set number of people being followed
    setFollowingPeopleCount: function() {
        $("#followingPeopleCount").html(this.followingPeople.length);
    },

    /**
     * This method should be invoked, whenever this page is loaded
     * @params
     * searchResults - Array of search results/data result via ajax
     */
    displayFollowingPeople: function(listOfPeople) {
        var $resultsContainer = $("#followingContainer");

        for (var i = 0, l = listOfPeople.length; i < l; i++) {
            $resultsContainer.append(this._createResultRow(listOfPeople[i]));
        }

        this.followingPeople = listOfPeople;

        this.setFollowingPeopleCount();
    },

    printRow: function(listOfPeople) {
        var $resultsContainer = $("#suggested-inner");

        for (var i = 0, l = listOfPeople.length; i < l; i++) {
            $resultsContainer.append(this._createResultRow(listOfPeople[i]));
        }
    },

    // Initialize invite section
    initInvite: function() {
        var self = this;

        $("#inviteForm").validate({
            highlight: function(element, errorClass, validClass) {
                $(element).addClass("has-error");
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).removeClass("has-error");
            },
            submitHandler: function(form) {
                self.sendInvite();
                $('#inviteForm')[0].reset();

                return false;
            }
        });
    },

    sendInvite: function() {
        var inviteData = utilities.formToObject("#inviteForm");

        $.ajax({
            type: "POST",
            async: true,
            data: inviteData,
            headers: {
                "X-Auth-Token": utilities.cookies.getItem("auth_token")
            },
            url: Config.baseUrl + "/users/" + utilities.local_storage.get("id") + "/invite",
            success: function(data, textStatus, jqXHR) {
                common.notify({
                    'title': "Success!",
                    'text': "Your Invite has been sent successfully.",
                    'type': "success"
                });
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log("unable to update password " + errorThrown);

                common.notify({
                    title: "Error",
                    text: "Unable to send invite",
                    type: "error",
                });
            }
        });
    },

    // Get list of followers from server - Tab 2
    getListOfFollowers: function(fetchCallback) {
        var self = this;

        // Fetch & display Followers

        $.ajax({
            type: "GET",
            async: true,
            headers: {
                "X-Auth-Token": utilities.cookies.getItem("auth_token")
            },
            url: Config.baseUrl + "/users/followers",
            success: function(data, textStatus, jqXHR) {
                fetchCallback && fetchCallback.apply(self, [data]);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log("unable to fetch followers " + errorThrown);
            }
        });
    },

    // Get list of followers from server - Tab 2
    getListOfFollowersOthers: function(userID, fetchCallback) {
        var self = this;

        // Fetch & display Followers

        $.ajax({
            type: "GET",
            async: true,
            headers: {
                "X-Auth-Token": utilities.cookies.getItem("auth_token")
            },
            url: Config.baseUrl + "/users/" + userID + "/followers_other",
            success: function(data, textStatus, jqXHR) {
                fetchCallback && fetchCallback.apply(self, [data]);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log("unable to fetch followers " + errorThrown);
            }
        });
    },

    displayFollowers: function(listOfPeople) {
        var $toFollowContainer = $("#followersContainer"),
            i = 0,
            l = listOfPeople.length;

        for (; i < l; i++) {
            $toFollowContainer.append(this.createPersonRow(listOfPeople[i]));
        }

        this.followers = listOfPeople;

        this.setFollowersCount();
    },

    createPersonRow: function(personDetails) {

        var follower_text = (personDetails.followingCount);
        if (+follower_text === 1) follower_text = " Follower</span>";
        else follower_text = " Followers</span>";

        var roadtrip_text = (personDetails.roadtripsCount);
        if (+roadtrip_text === 1) roadtrip_text = " <span>Roadtrip, </span><span>";
        else roadtrip_text = " <span>Roadtrips, </span><span>";

        var $person = utilities.createElement("<div/>", null, {
            'class': "content-wrapper following-people"
        });
        var $row = utilities.createElement("<div/>", $person, {
            'class': "row"
        });
        var $col = utilities.createElement("<div/>", $row, {
            'class': "small-2 large-1 columns"
        });

        if (personDetails.userPic) {
            utilities.createElement("<a/>", $col, {
                'href': "profile_view.html?_loc=otherProfile&id=" + personDetails.id,
                'class': "user-photo",
                'style': "background-image: url('" + personDetails.userPic + "')"
            });
        }
        else
        {
            utilities.createElement("<a/>", $col, {
                'href': "profile_view.html?_loc=otherProfile&id=" + personDetails.id,
                'class': "user-photo"
            });
        }

        $col = utilities.createElement("<div/>", $row, {
            'class': "small-10 large-11 columns"
        });
        var $temp = utilities.createElement("<div/>", $col, {
            'class': "following-people-details"
        });
        if (personDetails.name!= null) {
            utilities.createElement("<div/>", $temp, {
                'class': "following-people-name",
                'html': "<a href=profile_view.html?_loc=otherProfile&id=" + personDetails.id + " target='_blank'>" + (personDetails.name || "") + "</a>"
            });
        }else{
            utilities.createElement("<div/>", $temp, {
                'class': "following-people-name",
                'html': "<a href=profile_view.html?_loc=otherProfile&id=" + personDetails.id + " target='_blank'>" + (personDetails.username || "") + "</a>"
            });
        }
        utilities.createElement("<div/>", $temp, {
            'class': "following-people-desc",
            'html': personDetails.desc
        });
        utilities.createElement("<div/>", $temp, {
            'class': "following-people-info",
            'html': "<span>" + (personDetails.tripMiles*1.6).toFixed() + " km, </span> <span>" + personDetails.roadtripsCount + roadtrip_text + personDetails.followingCount + follower_text
        });

        common.createFollowButton($col, {
            'data': personDetails,
            'method': null
        }, {
            'data': personDetails,
            'method': null
        }, !personDetails.is_followed, personDetails.is_followed);

        return $person;
    },

    // Set number of followers
    setFollowersCount: function() {
        var count_followers = this.followers.length;
        if (+count_followers === 1) count_followers = count_followers + " FOLLOWER";
        else count_followers =count_followers + " FOLLOWERS";

        $("#followersCount").html(count_followers);
    },

    // Get list of suggested users
    getListOfSuggestedUsers: function(fetchCallback) {
        var self = this;

        $.ajax({
            type: "GET",
            async: true,
            headers: {
                "X-Auth-Token": utilities.cookies.getItem("auth_token")
            },
            url: Config.baseUrl + "/users/suggest_users",
            success: function(data, textStatus, jqXHR) {
                fetchCallback && fetchCallback.apply(self, [data]);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log("unable to fetch suggested users " + errorThrown);
            }
        });
    },

    displaySuggestedUsers: function(listOfPeople) {
        var $suggestedContainer = $("#suggestedContainer"),
            i = 0,
            l = listOfPeople.length;

        for (; i < l; i++) {
            $suggestedContainer.append(this.createPersonRow(listOfPeople[i]));
        }

        this.suggestedUsers = listOfPeople;

        this.setSuggestedUsersCount();
    },

    // Set number of suggested
    setSuggestedUsersCount: function() {
        $("#suggestedUsersCount").html(this.suggestedUsers.length);
    },
};

var me_followingPlaces = {
    initialize: function() {
        var self = this;

        if (utilities.getURLVar("_loc") === "otherProfile" && utilities.getURLVar("id") != utilities.local_storage.get("id")) {
            var userID = utilities.getURLVar("id");
            this.followingPlacesUrl = Config.baseUrl + "/users/" + userID + "/following_places";
            
            this._isOtherProfilePage = true;

            $(".__c_privateSection").hide();
            $("#draftsTab,#activityTab,#recommendedTab").next().remove();
            $("#draftsTab,#activityTab,#recommendedTab").remove();

            me_trips.fetchOtherUserData(userID, common.fillProfileData);
            //this.fetchOtherUserFeed(userID, this.printFeeds);

            $("#roadtripTab").attr("href","profile_view.html?_loc=otherProfile&id=" + userID);
            $("#followingPlacesTab").attr("href","follow_me.html?_loc=otherProfile&id=" + userID);
            $("#followingPeopleTab").attr("href","follow_me_find_and_invite.html?_loc=otherProfile&id=" + userID);
            $("#followingViewAllTab").attr("href","follow_me_find_and_invite.html?_loc=otherProfile&id=" + otherProfileId);
            $("#likedTripsTab").attr("href","profile_view.html?_loc=otherProfile&_pg=liked&id=" + userID);
        } else {
            // Refresh User data
            common.fetchProfileData(function(data) {
                signin.saveDataInLocalStorage(data);
                common.fillProfileData(data);
            });
        }
        
        // Fetch & display Currently Following Places
        this.getFollowingPlaces(self.searchComplete);

        // If results are fetched successfully
        //self.searchComplete(self.followingPlaces, true);


        // Fetch & display Suggested Places
        this.fetchSuggestedLocations(function(data) {
            self.suggestedPlaces = data;

            self.setSuggestedPlacesCount();
            self.suggetionsFound(self.suggestedPlaces, true);
        });

        if( window.location.hash && window.location.hash == "#suggested") {
            $(".tabs dd").removeClass("active");
            $(".tabs dd:last-child").addClass("active");

            $(".tabs-content div").removeClass("active");
            $(".tabs-content div:last-child").addClass("active");
        }

        /*
            nextSelector should always be a anchor tag, with its "href" pointing to the next page URL (next set of results)
        */
        $('#searchResultsContainer').infinitescroll({
            debug: true,
            behavior: undefined,
            binder: $(window),
            nextSelector: "#searchFurther",
            navSelector: "#searchFurther",
            itemSelector: ".content-wrapper",
            animate: true,
            dataType: 'json',
            bufferPx: 40,
        });

        // Instant search for places
        $("#placesSearch").on("keyup", Foundation.utils.throttle(function(e) {
            var $searchKeywords = $(this).val();

            // Show all places before filtering
            $("#searchResultsContainer .content-wrapper").show();

            if ($searchKeywords == "") $("#searchFurther").hide();
            else $("#searchFurther").show();

            $("#searchResultsContainer .content-wrapper").filter(function(index) {
                // Hide useless results, not containing the search term
                return $(this).find(".place .place-title").html().toLowerCase().indexOf($searchKeywords.toLowerCase()) == -1;
            }).hide();

        }, 300));

        // Further Search Results
        $("#searchFurther").on("click", function(ev) {
            ev.preventDefault();

            self.searchFurther();
        });

        // More suggestions
        $("#moreSuggestions").on("click", function(ev) {
            ev.preventDefault();

            self.moreSuggestions();
        });
    },

    followingPlacesUrl : Config.baseUrl + "/users/following_locations",

    followingPlaces: [],

    suggestedPlaces: [],

    // Get list of places being followed - Tab 1
    getFollowingPlaces: function(fetchCallback) {
        var self = this;

        $.ajax({
            type: "GET",
            async: true,
            headers: {
                "X-Auth-Token": utilities.cookies.getItem("auth_token")
            },
            url: this.followingPlacesUrl,
            success: function(data, textStatus, jqXHR) {
                // Data returned from this request should have the key 'isBeingFollowed' set to true, always
                for (var i = 0, l = data.length; i < l; i++)
                    data[i]['isBeingFollowed'] = true;

                fetchCallback && fetchCallback.apply(self, [data, true]);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log("unable to fetch places being followed " + errorThrown);
            }
        });
    },

    // Get list of suggested places - Tab 2
    fetchSuggestedLocations: function(fetchCallback) {
        var self = this;

        $.ajax({
            type: "GET",
            async: true,
            headers: {
                "X-Auth-Token": utilities.cookies.getItem("auth_token")
            },
            url: Config.baseUrl + "/locations",
            success: function(data, textStatus, jqXHR) {
                fetchCallback && fetchCallback.apply(self, [data]);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log("unable to fetch suggested locations " + errorThrown);
            }
        });
    },

    // Set number of places being followed
    setFollowingPlacesCount: function() {
        var count = this.followingPlaces.length;
        $("#followingPlacesCount").html(count);
        if (count > 0) $("#suggested .following-place-info").hide();
        else $("#suggested .following-place-info").show();
    },

    // Set number of places being followed
    setSuggestedPlacesCount: function() {
        $("#suggestedPlacesCount").html(this.suggestedPlaces.length);
    },

    /**
     * This method should be invoked, whenever a search is complete
     * @params
     * searchResults - Array of search results
     * clearPreviousSuggestions - Flag (true/false) determining whether to clear previous search results or not
     */
    searchComplete: function(searchResults, clearPreviousResults) {
        var self = this;

        // clean up results container
        if (clearPreviousResults) $("#searchResultsContainer").html("");

        for (var i = 0, l = searchResults.length; i < l; i++) {
            self._addFollowingPlace(searchResults[i]);
        }

        this.followingPlaces = searchResults;

        this.setFollowingPlacesCount();
    },

    _addFollowingPlace: function(placeDetails) {
        var self = this,
            $resultsContainer = $("#searchResultsContainer");

        $resultsContainer.append(this._createResultRow(placeDetails, function(resultDetails) {
            self._placesFollowCallback.apply(self, [resultDetails]);
        }, function(resultDetails) {
            self._placesUnfollowCallback.apply(self, [resultDetails]);
        }));
    },

    /**
     * This method should be invoked, when suggestions for following places are found
     * @params
     * suggestions - Array of suggestions
     * clearPreviousSuggestions - Flag (true/false) determining whether to clear previous suggestions or not
     */
    suggetionsFound: function(suggestions, clearPreviousSuggestions) {
        var self = this,
            $resultsContainer = $("#suggestedContainer");

        // clean up results container
        if (clearPreviousSuggestions) $resultsContainer.html("");

        for (var i = 0, l = suggestions.length; i < l; i++) {
            $resultsContainer.append(this._createResultRow(suggestions[i], function(resultDetails) {
                self._suggestionsFollowCallback.apply(self, [resultDetails]);
            }, function(resultDetails) {
                // Not exactly required since the row have already been removed, but just in case
                self._suggestionsUnfollowCallback.apply(self, [resultDetails]);
            }));
        }

        // Show container required for fetching more results
        $("#moreSuggestions").show();
    },

    // Create a search results row
    _createResultRow: function(resultDetails, followCallback, unfollowCallback) {
        var self = this;
        /*
         * Button Styles (Reference):
         *
         *
         * Following
         * <a href="#" class="hidden tiny button tiny-ex button-following radius right"><span class="icon-tick-following links-text "></span>&nbsp;Following</a>
         *
         * Follow
         * <a href="#" class="tiny button tiny-ex button-follow radius right"><span class="icon-plus "></span>&nbsp;&nbsp;&nbsp;Follow</a>
         *
         * Unfollow
         * <a href="#" class="hidden tiny button tiny-ex button-unfollow radius right"><span class="icon-cross-unfollow text-red"></span>&nbsp;&nbsp;&nbsp;unfollow</a>
         */
        var $row = utilities.createElement("<div/>", null, {
            'class': "content-wrapper"
        });
        var $textContent = utilities.createElement("<div/>", $row, {
            'class': "place"
        });

        utilities.createElement("<p/>", $textContent, {
            'class': "place-title",
            'html': resultDetails.name
        });
        utilities.createElement("<p/>", $textContent, {
            'class': "place-info",
            'html': '<span>' + (resultDetails.roadtripsCount || "0") + ' Roadtrips</span><span> ' + (resultDetails.followingCount || "0") + ' Following</span>' + (resultDetails.followersCount ? '<span>' + resultDetails.followersCount + ' Followers</span>' : '')
        });

        resultDetails._row = $row;
        common.createFollowButton($row, {
            'data': resultDetails,
            'method': followCallback
        }, {
            'data': resultDetails,
            'method': unfollowCallback
        }, !resultDetails.isBeingFollowed, resultDetails.isBeingFollowed, true);

        return $row;
    },

    // Method to be invoked when Follow button is clicked in Following Places tab
    _placesFollowCallback: function(resultDetails) {
        // If followed successfully
        this.followingPlaces.push(resultDetails);

        // set following places count again
        this.setFollowingPlacesCount();
    },

    // Method to be invoked when Unfollow button is clicked in Following Places tab
    _placesUnfollowCallback: function(resultDetails) {
        for (var i = 0, l = this.followingPlaces.length; i < l; i++) {
            this.followingPlaces.splice(i, 1);
            break;
        }

        // set following places count again
        this.setFollowingPlacesCount();
    },

    // Method to be invoked when Follow button is clicked in Suggestions tab
    _suggestionsFollowCallback: function(resultDetails) {
        // Remove this row from suggested locations
        resultDetails._row.remove();

        // Add this suggestion to following places tab
        resultDetails['isBeingFollowed'] = true;
        this._placesFollowCallback(resultDetails);
        this._addFollowingPlace(resultDetails);

        // If un-followed successfully
        for (var i = 0, l = this.suggestedPlaces.length; i < l; i++) {
            if (this.suggestedPlaces[i]['id'] == resultDetails['id']) {
                this.suggestedPlaces.splice(i, 1);
                break;
            }
        }

        // Set count of suggested locations
        this.setSuggestedPlacesCount();
    },

    // Method to be invoked when Unfollow button is clicked
    _suggestionsUnfollowCallback: function(resultDetails) {
        var self = this;
        // AJAX call goes HERE

        // If un-followed successfully
        for (var i = 0, l = self.followingPlaces.length; i < l; i++) {
            if (self.followingPlaces[i]['id'] == resultDetails['id']) {
                self.followingPlaces.splice(i, 1);
                break;
            }
        }

        // set following places count again
        self.setFollowingPlacesCount();
    },

    // Fetch more results - Invoked when "Search Further" link below search results is clicked
    searchFurther: function() {
        /*var self = this;

        // AJAX call goes HERE

        // SAMPLE DATA (remove for production use):
        var searchResults = [
        { 'id': 5, 'location': "NY, USA", 'roadtripsCount': "32", 'followingCount': "08", 'isBeingFollowed': false },
        { 'id': 6, 'location': "Gambia, Africa", 'roadtripsCount': "05", 'followingCount': "38", 'isBeingFollowed': false },
        { 'id': 7, 'location': "Paris, France", 'roadtripsCount': "2", 'followingCount': "14", 'isBeingFollowed': false },
        { 'id': 8, 'location': "Naples, Italy", 'roadtripsCount': "71", 'followingCount': "25", 'isBeingFollowed': false }
        ];

        // If results are fetched successfully
        self.searchComplete(searchResults, false);*/

        $(".elasticSearchTrigger").trigger("click");
    },

    // Fetch more suggestions - Invoked when "Show More Suggestions" link below suggestions is clicked
    moreSuggestions: function() {
        var self = this;

        // AJAX call goes HERE

        // SAMPLE DATA (remove for production use):
        var suggestions = [{
            'id': 5,
            'location': "NY, USA",
            'roadtripsCount': "32",
            'followingCount': "08",
            'isBeingFollowed': false
        }, {
            'id': 6,
            'location': "Gambia, Africa",
            'roadtripsCount': "05",
            'followingCount': "38",
            'isBeingFollowed': false
        }, {
            'id': 7,
            'location': "Paris, France",
            'roadtripsCount': "2",
            'followingCount': "14",
            'isBeingFollowed': false
        }, {
            'id': 8,
            'location': "Naples, Italy",
            'roadtripsCount': "71",
            'followingCount': "25",
            'isBeingFollowed': false
        }];

        // If results are fetched successfully
        self.suggetionsFound(suggestions, false);
    }
};

/* Page: profile_activity.html */
var profile_activity = {
    initialize: function() {

        common.fetchProfileData(function(data) {
            signin.saveDataInLocalStorage(data);
            common.fillProfileData(data);
        });

        this.fetchActivity(this.printActivities);
        var self = this;
        $(".view-older").on("click", function(ev) {
            ev.preventDefault();
            self.fetchOldActivity(self.printActivities);
        });

        $(".mark-unread").on("click", function(ev) {
            ev.preventDefault();
            self.markUnreadActivity(self.printActivities);
        });
    },

    markUnreadActivity: function(fetchCallback) {
        var self = this;

        var notifications = [];

        for (var i = 1; i <= 200; i++) {
           notifications.push(i);
        }

        $.ajax({
            type: "PUT",
            async: true,
            data: {'notification_ids[]': notifications},
            headers: {
                "X-Auth-Token": utilities.cookies.getItem("auth_token")
            },
            url: Config.baseUrl + "/users/mark_as_unread_notifications",
            success: function(data, textStatus, jqXHR) {
                fetchCallback && fetchCallback.apply(self, [data, true]);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log("unable to fetch profile activity " + errorThrown);
            }
        });
    },

    fetchOldActivity: function(fetchCallback) {
        var self = this;

        $.ajax({
            type: "GET",
            async: true,
            headers: {
                "X-Auth-Token": utilities.cookies.getItem("auth_token")
            },
            url: Config.baseUrl + "/users/all_notifications",
            success: function(data, textStatus, jqXHR) {
                fetchCallback && fetchCallback.apply(self, [data, true]);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log("unable to fetch profile activity " + errorThrown);
            }
        });
    },

    fetchActivity: function(fetchCallback) {
        var self = this;

        $.ajax({
            type: "GET",
            async: true,
            headers: {
                "X-Auth-Token": utilities.cookies.getItem("auth_token")
            },
            url: Config.baseUrl + "/users/notifications",
            success: function(data, textStatus, jqXHR) {
                fetchCallback && fetchCallback.apply(self, [data, true]);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log("unable to fetch profile activity " + errorThrown);
            }
        });
    },

    printActivities: function(listOfActivities) {
        if ( listOfActivities && listOfActivities["notifications"].length > 0) {

            listOfActivities = listOfActivities["notifications"].reverse();
            var i = 0,
                l = listOfActivities.length,
                $activitiesContainer = $("#activitiesContainer");
            $activitiesContainer.html("");
            for (; i < l; i++) {
                if (listOfActivities[i].subject_id !== null && listOfActivities[i].actor !== null 
                    && listOfActivities[i].subject !== null) {
                    $activitiesContainer.append(this.createActivityRow(listOfActivities[i]));
                }

            }
        }
    },

    createActivityRow: function(activityDetails) {
        var $activity = utilities.createElement("<li/>", null, {
            'class': "row wthcntnt"
        });

        var $col = utilities.createElement("<div/>", $activity, {
            'class': "small-10 medium-10 large-10 columns"
        });
        if (activityDetails["actor_img_url"]) 
        {
            utilities.createElement("<a/>", $col, {
                'href': "profile_view.html?_loc=otherProfile&id=" + activityDetails["actor_id"],
                'class': "user-photo margin-right-15 fl",
                'style': "background-image: url('" + activityDetails["actor_img_url"] + "')"
            });
        }
        else
        {
            utilities.createElement("<a/>", $col, {
                'href': "profile_view.html?_loc=otherProfile&id=" + activityDetails["actor_id"],
                'class': "user-photo margin-right-15 fl"
            });
        }
        var $content = utilities.createElement("<div/>", $col, {
            'class': "mid fl"
        });
        utilities.createElement("<a/>", $content, {
            'href': "profile_view.html?_loc=otherProfile&id=" + activityDetails["actor_id"],
            'class': "summry",
            'html': "<strong>" + activityDetails["actor"] + "</strong> "
        });
        utilities.createElement("<span/>", $content, {
            'class': "summry",
            'html': activityDetails["action"]
        });
            if (activityDetails["action"] !== "followed") {
                utilities.createElement("<a/>", $content, {
                    'class': "summry",
                    'href': "road_trip_view.html?id=" + activityDetails["subject_id"],
                    'html': " <strong>" + activityDetails["subject"] + "</strong>"
                });
            }
        utilities.createElement("<p/>", $content, {
            'class': "time",
            'html': activityDetails["created_at"]
        });

        $col = utilities.createElement("<div/>", $activity, {
            'class': "small-2 medium-2 large-2 columns"
        });

        common.createFollowButton($col, null, null, null, null, false);

        return $activity;
    }
};

/*Page: profile_edit.html*/
var profile_edit = {
    initialize: function() {
        var self = this,
            $profilePhotoForm = $("#profilePhotoForm");

        // Quick methods
        this.social_icons.initialize();
        $("#auth_token").val(utilities.cookies.getItem("auth_token"));

        // Enable uploading a photo
        $("#uploadPhotoBtn").on("click", function() {
            $("#photo").click();
        });
        $("#photo").on("change", function() {
            $profilePhotoForm.attr("action", Config.baseUrl + "/users/" + utilities.local_storage.get('id') + "/upload_image");

            $profilePhotoForm.submit();
        });


        // Handle form submit for profile info
        $("#profileInfoForm").validate({
            highlight: function(element, errorClass, validClass) {
                $(element).addClass("has-error");
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).removeClass("has-error");
            },
            submitHandler: function(form) {
                self.saveProfileInfo(self.profileInfoSaved);

                return false;
            }
        });

        this.setBioChars();
        $("#bio").on("keyup", function(ev) {
            // Allow arrow keys
            if (ev.keyCode >= 37 && ev.keyCode <= 40) return;

            // Allow delete and update the number of characters
            if (ev.keyCode == 8 || ev.keyCode == 46) {
                self.setBioChars();
                return;
            }

            // If max limit is reached, prevent adding more characters
            if (self.currentBioChars == self.maxBioChars) ev.preventDefault();

            self.setBioChars();
        });

        // Fetch & display existing user profile data
        this.fetchProfileInfo(this.displayProfileInfo);
    },

    maxBioChars: 200,

    currentBioChars: 0,

    setBioChars: function() {
        this.currentBioChars = $("#bio").val().length;
        $("#bioRemainingChars").text(this.maxBioChars - this.currentBioChars);
    },

    // Fetch profile data from server
    fetchProfileInfo: function(fetchCallback) {
        var self = this;
        // AJAX call goes HERE
        $.ajax({
            type: "GET",
            async: true,
            headers: {
                "X-Auth-Token": utilities.cookies.getItem("auth_token")
            },
            url: Config.baseUrl + "/users/" + utilities.local_storage.get('id'),
            success: function(data, textStatus, jqXHR) {
                if (fetchCallback) fetchCallback.apply(self, [data]);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log("unable to fetch profileInfo" + errorThrown);
            }
        });
    },

    // Fill in profile info in textboxes
    displayProfileInfo: function(profileData) {
        $.each(profileData, function(key, value) {
            key = $("#" + key);
            if (key.is("input[type='checkbox']")) {
                key.prop("checked", +value);
            } else {
                key.val(value);
            }
        });

        if (profileData.userPic) {
            $(".user-photo").css("background-image", "url('" + profileData.userPic + "')");
        }
        var i = 0,
            l = profileData.authentications.length;

        if (l > 0) {
            for (; i < l; i++) {
                switch (profileData.authentications[i].provider) {
                    case "Facebook":
                        if (profileData.authentications[i].is_visible) this.social_icons.activate($(".social-icon:eq(1)"));
                        else this.social_icons.deactivate($(".social-icon:eq(1)"));
                        break;

                    case "twitter":
                        if (profileData.authentications[i].is_visible) this.social_icons.activate($(".social-icon:eq(0)"));
                        else this.social_icons.deactivate($(".social-icon:eq(0)"));
                        break;

                    case "google-oauth2":
                        if (profileData.authentications[i].is_visible) this.social_icons.activate($(".social-icon:eq(2)"));
                        else this.social_icons.deactivate($(".social-icon:eq(2)"));
                        break;
                }
            }
        } else {
            this.social_icons.deactivate($(".social-icon:eq(0)"));
            this.social_icons.deactivate($(".social-icon:eq(1)"));
            this.social_icons.deactivate($(".social-icon:eq(2)"));
        }

        this.setBioChars();
    },

    // Save user profile data
    saveProfileInfo: function(saveCallback) {
        var self = this,
            profileData = utilities.formToObject("#profileInfoForm");

        // AJAX call goes HERE
        $.ajax({
            type: "PUT",
            async: true,
            headers: {
                "X-Auth-Token": utilities.cookies.getItem("auth_token")
            },
            url: Config.baseUrl + "/users/" + utilities.local_storage.get('id'),
            data: profileData,
            success: function(data, textStatus, jqXHR) {
                if (saveCallback) saveCallback.apply(self, [data]);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                common.notify({
                    title: "Oops",
                    text: "We are unable to save your info at this time. Please try again later.",
                    type: "error"
                });
            }
        });
    },

    profileInfoSaved: function(profileData) {
        utilities.local_storage.clear();
        signin.saveDataInLocalStorage(profileData);

        //common.notify({ title: "Congrats!", text: "Your Profile Information was updated successfully", type: "success" });

        window.location = "profile_view.html";
    },

    social_icons: {
        initialize: function() {
            var self = this;

            $(".social-icon").on("click", function(ev) {
                ev.preventDefault();
                ev.stopPropagation();

/*                self.toggle(this);*/
            });
            $('.social-icon').tooltipster({
                position: 'top',
                interactive: true,
                content: $('<span>Planned for next release.</span>'),
                functionReady: function () {
                    $(window).trigger("resize");
                }
            });

            $(window).resize(function () {
                $('.tooltipster,.tooltipstersort').tooltipster('reposition');
            });
            $(window).trigger("resize");
        },

        deactivate: function($el) {
            $el = $($el);

            $el.addClass("_isDisabled");
            $el.find("label:nth-child(1)").removeClass().addClass("button no-connection");
            $el.find("label:nth-child(2)").removeClass().addClass("button check no-connection");
        },

        activate: function($el) {
            $el = $($el);

            var index = $el.index();

            $el.find("label").removeClass("no-connection");
            $el.removeClass("_isDisabled");

            switch (index) {
                case 0:
                    $el.find("label").addClass("twitter");
                    break;

                case 1:
                    $el.find("label").addClass("facebook");
                    break;

                case 2:
                    $el.find("label").addClass("gplus");
                    break;
            }
        },

        toggle: function($el) {
            $el = $($el);
            var isVisible = true;

            if ($el.hasClass("_isDisabled")) {
                this.activate($el);
            } else {
                this.deactivate($el);
                isVisible = false;
            }

            $.ajax({
                type: "POST",
                async: true,
                data: {
                    'is_visible': isVisible
                },
                headers: {
                    "X-Auth-Token": utilities.cookies.getItem("auth_token")
                },
                url: Config.baseUrl + "/users/toggle_profile_url_status/" + $el.data('provider'),
                success: function(data, textStatus, jqXHR) {
                    // Do something here
                    common.notify({
                        title: "Success",
                        text: "Successfully connected to social network",
                        type: "success",
                    });
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log("unable to toggle social connection " + errorThrown);
                }
            });
        }
    }
};

var profile_changePassword = {
    initialize: function() {
        var self = this;

        // Focus current password textbox by default
        $("#currentPassword").focus();

        // Handle form submit for profile info
        $("#changePasswordForm").validate({
            highlight: function(element, errorClass, validClass) {
                $(element).addClass("has-error");
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).removeClass("has-error");
            },
            submitHandler: function(form) {
                self.sendChange(self._onPasswordChanged);

                return false;
            }
        });
    },

    checkNewPasswords: function(newPassword1, newPassword2) {
        if (newPassword1 != "" && newPassword1 == newPassword2) return true;

        return false;
    },

    sendChange: function(saveCallback) {
        var passwords = utilities.formToObject("#changePasswordForm");

        if (!this.checkNewPasswords(passwords.new_password, passwords.new_password_confirmation)) {
            // New passwords do not match - display a message for the same
            common.notify({
                title: "Error",
                text: "The passwords you have entered do not match",
                type: "error",
            });

            $("#changePasswordForm").find("input[name='new_password'], input[name='new_password_confirmation']").addClass("has-error");

            return;
        }

        $.ajax({
            type: "POST",
            async: true,
            data: passwords,
            headers: {
                "X-Auth-Token": utilities.cookies.getItem("auth_token")
            },
            url: Config.baseUrl + "/users/" + utilities.local_storage.get("id") + "/change_password",
            success: function(data, textStatus, jqXHR) {
                if (saveCallback) saveCallback.apply(self, [data]);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log("unable to update password " + errorThrown);

                common.notify({
                    title: "Error",
                    text: "Please check your current password.",
                    type: "error",
                });
            }
        });
    },

    _onPasswordChanged: function(accountInfo) {
        signin.saveDataInLocalStorage(accountInfo);
        $('#changePasswordForm').trigger("reset");
        common.notify({
            title: "Congrats!",
            text: "Your password was changed Successfully.",
            type: "success",
        });
    }
};

var profile_notifications = {
    initialize: function() {
        var self = this;

        $("input[name='default_as_miles']").on("click", function() {

            $("input[name='default_as_miles']").parent().removeClass("active");

            if ($(this).is(':checked')) {
                $(".checkIcon").html("");

                $(this).parent().addClass("active");
                $(this).parent().find(".checkIcon").html('&nbsp;<span class="icon-pxl icon-pxl-tick-bold-black "></span>');
            }
        });

        $("#profileSettingsForm").on("submit", function() {
            self.save();

            return false;
        });
    },

    // Save notification settings
    save: function(saveCallback) {
        var self = this,
            profileSettings = utilities.formToObject("#profileSettingsForm");

        switch (profileSettings.default_as_miles) {
            case "km":
                profileSettings.default_as_miles = false;
                break;

            case "mi":
                profileSettings.default_as_miles = true;
                break;
        }

        $.ajax({
            type: "POST",
            async: true,
            data: {
                "notification_setting": profileSettings
            },
            headers: {
                "X-Auth-Token": utilities.cookies.getItem("auth_token")
            },
            url: Config.baseUrl + "/users/" + utilities.local_storage.get("id") + "/notification_setting",
            success: function(data, textStatus, jqXHR) {
                common.notify({
                    title: "Congrats!",
                    text: "Your preferences were saved successfully",
                    type: "success",
                });
                if (saveCallback) saveCallback.apply(self, [data]);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log("unable to save notification settings " + errorThrown);

                common.notify({
                    title: "Error",
                    text: "Unable to save your preferences",
                    type: "error",
                });
            }
        });
    }
};

var explore = {
    initialize: function() {
        var self = this;

        // Quick Methods/inits
        //$("#titleLocation").html("New Delhi, India"); // Set page title location
        //this.getTrendingFeed(this.displayTrendingFeed);
        //this.getRecentFeed(this.displayRecentFeed);
        this.getRelatedTags(this.displayRelatedTags);
        this.getRelatedLocations(this.displayRelatedLocations);
        //this.getLocationExtras(this.displayLocationExtras);


        // Lazy Methods
        this.map = utilities.createGMap(document.getElementById("map_canvas"));
        this.createZoomControl(this.map);
        this.createHelpControl(this.map);

        common.fetchProfileData(function(data) {
            signin.saveDataInLocalStorage(data);
            common.fillProfileData(data);
        });

        // Listen to map zoom
        // Note: This event is fired automatically on map load
        google.maps.event.addListener(this.map, "bounds_changed", utilities.debounce(500, function(ev) {
            var bounds = self.map.getBounds(),
                ne = bounds.getNorthEast(),
                sw = bounds.getSouthWest(),
                center = bounds.getCenter(),
                boundsData = {
                    'ne': {
                        'latitude': ne.lat(),
                        'longitude': ne.lng()
                    },
                    'sw': {
                        'latitude': sw.lat(),
                        'longitude': sw.lng()
                    },
                    'center': {
                        'latitude': center.lat(),
                        'longitude': center.lng()
                    },
                    'zoom': self.map.getZoom()
                };

            self.getMapLocations(boundsData, self.plotLocations);
        }));
    },

    // fetch locations to plot on map
    getMapLocations: function(bounds, fetchCallback) {
        var self = this;

        $.ajax({
            type: "POST",
            async: true,
            headers: {
                "X-Auth-Token": utilities.cookies.getItem("auth_token")
            },
            data: bounds,
            url: Config.baseUrl + "/trips/explore",
            success: function(data) {
                if (fetchCallback) fetchCallback.apply(self, [data]);
            },
            error: function() {
                console.log("Unable to fetch locations.");
            }
        });
    },

    plotLocations: function(mapLocations) {
        var marker,
            i = 0,
            l = mapLocations.length,
            latlng,
            self = this;

        // Initially remove all markers
        this.removeAllMarkers();

        for (; i < l; i++) {
            marker = this.createWhiteMarker(this.map);
            marker.setIcon(Markers.whiteIcon);
            marker.setPosition(new google.maps.LatLng(mapLocations[i].latitude, mapLocations[i].longitude));
            marker.clickFlag = 0;
            marker.locationID = mapLocations[i].id;

            google.maps.event.addListener(marker, "mouseover", (function(mapLocation) {
                return function() {
                    self.showInfoWindow(this, mapLocation);
                }
            })(mapLocations[i]));

            google.maps.event.addListener(marker, "mouseout", (function(mapLocation) {
                return function() {
                    self.hideInfoWindow();
                }
            })(mapLocations[i]));

            if (mapLocations[i].id == 22) {
                
                $(".__c_titleLocation").html(mapLocations[i].name);

                $("#followButtonContainer").html("");

                marker.setIcon(Markers.redIcon);

                self.currentlyClickedMarker = marker;

                // Get feed for this location
                self.getTrendingFeed(mapLocations[i].id, function(data) {
                    self.displayTrendingFeed(data.trending_trips);
                    self.displayRecentFeed([data.recent_trips]);
                    self.displayLocationExtras(data.location_details);

                    // Remove Previous follow button
                    $("#followButtonContainer").empty();

                    // Create new follow button
                    common.createFollowButton("#followButtonContainer", {
                        'data': mapLocations[i]
                    }, {
                        'data': mapLocations[i]
                    }, !data.is_followed, data.is_followed, true);
                });
                //self.getRecentFeed(self.displayRecentFeed);
            }

            google.maps.event.addListener(marker, "click", (function(mapLocation) {
                return function() {
                    $(".__c_titleLocation").html(mapLocation.name);

                    $("#followButtonContainer").html("");

                    if ($("#map-help-text").css("display") != "none") $("#map-help-text").fadeOut(1200);

                    if (self.currentlyClickedMarker) {
                        self.currentlyClickedMarker.setIcon(Markers.whiteIcon);
                    }

                    this.setIcon(Markers.redIcon);

                    self.currentlyClickedMarker = this;

                    // Get feed for this location
                    self.getTrendingFeed(mapLocation.id, function(data) {
                        self.displayTrendingFeed(data.trending_trips);
                        self.displayRecentFeed([data.recent_trips]);
                        self.displayLocationExtras(data.location_details);

                        // Remove Previous follow button
                        $("#followButtonContainer").empty();

                        // Create new follow button
                        common.createFollowButton("#followButtonContainer", {
                            'data': mapLocation
                        }, {
                            'data': mapLocation
                        }, !data.is_followed, data.is_followed, true);
                    });
                    //self.getRecentFeed(self.displayRecentFeed);

                    // Scroll to location details section
                    $('html, body').animate({
                        scrollTop: $("#titleLocation").offset().top - 70
                    }, 1500);
                }
            })(mapLocations[i]));

            // Add all newly created markers to global markers array
            this.markers.push(marker);
        }
    },

    infoWindow: null,

    markers: [],

    currentlyClickedMarker: null,

    // Remove all markers from map and clear array
    removeAllMarkers: function() {
        for (var i = 0; i < this.markers.length; i++) {
            this.markers[i].setMap(null);
        }

        this.markers = [];
    },

    showInfoWindow: function(marker, mapLocation) {
        this.infoWindow = new Ex_InfoBox({
            'latLng': marker.getPosition(),
            'content': mapLocation.name
        }, this.map);
    },

    hideInfoWindow: function() {
        this.infoWindow.setMap(null);
    },

    createZoomControl: function(map) {
        // Create a div to hold the control.
        var controlDiv = document.createElement('div');

        // Set CSS styles for the DIV containing the control
        // Setting padding to 45 px will offset the control
        // from the edge of the map.
        controlDiv.style.padding = '45px';

        var zoomInHelper = this.createZoomHelper("+");
        google.maps.event.addDomListener(zoomInHelper, 'click', function() {
            if (map.getZoom() == 5) return;
            map.setZoom(map.getZoom() + 1);
        });
        controlDiv.appendChild(zoomInHelper);

        var zoomOutHelper = this.createZoomHelper("-");
        google.maps.event.addDomListener(zoomOutHelper, 'click', function() {
            if (map.getZoom() == 2) return;
            map.setZoom(map.getZoom() - 1);
        });
        controlDiv.appendChild(zoomOutHelper);

        // We don't really need to set an index value here, but
        // this would be how you do it. Note that we set this
        // value as a property of the DIV itself.
        controlDiv.index = 1;

        // Add the control to the map at a designated control position
        // by pushing it on the position's array. This code will
        // implicitly add the control to the DOM, through the Map
        // object. You should not attach the control manually.
        map.controls[google.maps.ControlPosition.BOTTOM_LEFT].push(controlDiv);
    },

    createZoomHelper: function(text) {
        // Set CSS for the control border.
        var controlUI = document.createElement('div');
        controlUI.title = 'Zoom In';
        controlUI.className = "zoomControl";

        // Set CSS for the control interior.
        var controlText = document.createElement('div');
        controlText.style.fontSize = '24px';
        controlText.style.fontFamily = 'Arial';
        controlText.style.fontWeight = '700';
        controlText.style.lineHeight = '32px';
        controlText.style.color = 'white';
        controlText.innerHTML = text;
        controlUI.appendChild(controlText);

        return controlUI;
    },

    createHelpControl: function(map) {
        // Create a div to hold the control.
        var controlDiv = document.createElement('div');

        // Set CSS styles for the DIV containing the control
        // Setting padding to 45 px will offset the control
        // from the edge of the map.
        controlDiv.style.padding = '57px';

        var htBlock = this.createHelpTextBlock("Select region of your interest. Zoom into places to discover more");
        controlDiv.appendChild(htBlock);

        // We don't really need to set an index value here, but
        // this would be how you do it. Note that we set this
        // value as a property of the DIV itself.
        controlDiv.index = 2;

        // Add the control to the map at a designated control position
        // by pushing it on the position's array. This code will
        // implicitly add the control to the DOM, through the Map
        // object. You should not attach the control manually.
        map.controls[google.maps.ControlPosition.BOTTOM_CENTER].push(controlDiv);
    },

    createHelpTextBlock: function(text) {
        // Set CSS for the control border.
        var controlUI = document.createElement('div');
        controlUI.className = "helpText clearfix";
        controlUI.id = "map-help-text";

        // Set CSS for the control interior.
        var controlText = document.createElement('div');
        controlText.innerHTML = text;
        controlText.className = "help-text";
        controlUI.appendChild(controlText);

        // Set CSS for control tooltip
        /* var controlTooltip = document.createElement("a");
        controlTooltip.innerHTML = "?";
        controlTooltip.className = "help-tooltip-helper";
        controlUI.appendChild(controlTooltip);*/

        return controlUI;
    },

    createWhiteMarker: function(map) {
        return new google.maps.Marker({
            'draggable': false,
            'map': map
        });
    },

    getTrendingFeed: function(locationID, fetchCallback) {
        var self = this;

        $.ajax({
            type: "GET",
            async: true,
            data: {
                location_id: locationID
            },
            headers: {
                "X-Auth-Token": utilities.cookies.getItem("auth_token")
            },
            url: Config.baseUrl + "/trips/explore_location",
            success: function(data, textStatus, jqXHR) {
                if (fetchCallback) fetchCallback.apply(self, [data]);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log("unable to fetch trending feed " + errorThrown);
            }
        });
    },

    getRecentFeed: function(fetchCallback) {
        var self = this;

        $.ajax({
            type: "GET",
            async: true,
            headers: {
                "X-Auth-Token": utilities.cookies.getItem("auth_token")
            },
            url: Config.baseUrl + "/trips/sort?sort_by=created_at&sort_type=desc",
            success: function(data, textStatus, jqXHR) {
                if (fetchCallback) fetchCallback.apply(self, [data]);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log("unable to fetch recent feed " + errorThrown);
            }
        });
    },

    displayTrendingFeed: function(listOfFeed) {
        // clear all trending feed initially
        $("#trendingFeed").html("");

        me_trips.printFeeds(listOfFeed, "#trendingFeed");
    },

    displayRecentFeed: function(listOfFeed) {
        // clear all recent feed initially
        $("#recentFeed").html("");

        me_trips.printFeeds(listOfFeed, "#recentFeed", "oneColFeed");
    },

    getRelatedTags: function(fetchCallback) {
        var self = this;

        // AJAX call goes here

        // SAMPLE DATA (remove for production use):
        var arrayOfTags = [{
            'id': 1,
            'tagHref': "#",
            'tagText': "#India"
        }, {
            'id': 2,
            'tagHref': "#",
            'tagText': "#Indian-food"
        }, {
            'id': 3,
            'tagHref': "#",
            'tagText': "#culture"
        }, {
            'id': 4,
            'tagHref': "#",
            'tagText': "#red-fort"
        }, {
            'id': 5,
            'tagHref': "#",
            'tagText': "#example-tag"
        }, {
            'id': 6,
            'tagHref': "#",
            'tagText': "#roadmojo"
        }, {
            'id': 7,
            'tagHref': "#",
            'tagText': "#travel"
        }];

        // If list of tags is fetched successfully, send data in callback

        if (fetchCallback) fetchCallback.apply(self, [arrayOfTags]);
    },

    getRelatedLocations: function(fetchCallback) {
        var self = this;

        // AJAX call goes here

        // SAMPLE DATA (remove for production use):
        var arrayOfLocations = [{
            'id': 1,
            'tagHref': "#",
            'tagText': "Leh"
        }, {
            'id': 2,
            'tagHref': "#",
            'tagText': "Chandigarh"
        }, {
            'id': 3,
            'tagHref': "#",
            'tagText': "Agra"
        }, {
            'id': 4,
            'tagHref': "#",
            'tagText': "Dharamshala"
        }, {
            'id': 5,
            'tagHref': "#",
            'tagText': "Kerala"
        }, {
            'id': 6,
            'tagHref': "#",
            'tagText': "Bangalore"
        }, {
            'id': 7,
            'tagHref': "#",
            'tagText': "Example location name"
        }];

        // If list of locations is fetched successfully, send data in callback

        if (fetchCallback) fetchCallback.apply(self, [arrayOfLocations]);
    },

    displayRelatedTags: function(listOfTags) {
        var i = 0,
            l = listOfTags.length,
            $tagsContainer = $("#relatedTagsContainer");

        for (; i < l; i++) {
            $tagsContainer.append(this.createTag(listOfTags[i]));
        }
    },

    displayRelatedLocations: function(listOfLocations) {
        var i = 0,
            l = listOfLocations.length,
            $tagsContainer = $("#relatedLocationsContainer");

        for (; i < l; i++) {
            $tagsContainer.append(this.createTag(listOfLocations[i]));
        }
    },

    createTag: function(tagDetails) {
        // <span class="button radius tag">#India</span>
        var $tag = utilities.createElement("<a/>", null, {
            'href': tagDetails.tagHref
        });
        utilities.createElement("<span/>", $tag, {
            'class': "button radius tag",
            'html': tagDetails.tagText
        });

        return $tag;
    },

    // Get extra info of this location
    getLocationExtras: function(fetchCallback) {
        var self = this;

        // AJAX call goes here

        // SAMPLE DATA (remove for production use):
        var locationExtras = {
            'roadtripsCount': 7851,
            'followersCount': 183,
            'photoCredits': "Akshay Chauhan"
        };

        // If list of locations is fetched successfully, send data in callback

        if (fetchCallback) fetchCallback.apply(self, [locationExtras]);
    },

    // Display the extra info
    displayLocationExtras: function(locationDetails) {
        // Print roadtrips & followers count on image
        $("#roadtripsCount").text(locationDetails.roadtrips_count);
        $("#followersCount").text(locationDetails.followers_count);

        // Print photo credits
        $("#photoCredits").text(locationDetails.photoCredits);
    }
};

var road_trip_view = {
    initialize: function() {
        var self = this;
        this.tripID = utilities.getURLVar("id");

        this.getTripData(this.printTripData);
        this.sidebarmap = create_roadtrip.createSidebarMap();
        this.createZoomControl(this.sidebarmap);
        this.sidebarmap.setOptions({
            "disableDoubleClickZoom": true
        });

        this.sidebarmapbounds = new google.maps.LatLngBounds();

        this.fullViewMap = create_roadtrip.createFullScreenMap();

        common.fetchProfileData(function(data) {
            signin.saveDataInLocalStorage(data);
            common.fillProfileData(data);
        });

        $(document).on('opened.fndtn.reveal', '#fullMapModal', function() {
            google.maps.event.trigger(self.fullViewMap, "resize");
            self.fitBounds(self.fullViewMap, self.fullMapMarkers);
        });

        $("#fullMapModal").on("click", ".__c_milestonesList a", function(ev) {
            ev.preventDefault();

            var milestoneID = +$(this).data("milestone-id");
            self._doInactiveMarkers(self.fullMapMarkers);
            self.fullMapMarkers[milestoneID].toggleActive();
        });

        var $root = $('html, body');
        $("aside").on("click", ".__c_milestonesList a", function() {
            var milestoneID = +$(this).data("milestone-id");
            self._doInactiveMarkers(self.sidebarMapMarkers);
            self.sidebarMapMarkers[milestoneID].toggleActive();

            $root.animate({
                scrollTop: $($.attr(this, 'href')).offset().top
            }, 800);
            return false;
        });

        // show image viewer on image click
        $(".myThumbDivAutoAdd").on("click", function() {
            $('#imageViewer').foundation('reveal', 'open');
        });

        $(".likeTripBtn").on("click", function(ev) {
            ev.preventDefault();
            if (utilities.local_storage.get("id") !== null && typeof utilities.local_storage.get("id") !== "undefined" &&
                utilities.local_storage.get("id") !== ""){
                self.likeThisTrip();
            } 
        });

        new Share(".shareTripBtn", {
            networks: {
                pinterest: {
                    enabled: false
                },
                email: {
                    enabled: false
                }
            }
        });

        $(".shareTripBtn").on("click", function(ev) {
            ev.preventDefault();

            //$(this).find(".share-tooltip").toggle();
        });

        $(document).on('click','.commentTripBtn', function(ev) {
            ev.preventDefault();
            $("#addCommentForm").find("input").focus();
        });

        $("#addCommentForm").on("submit", function() {
            if (utilities.local_storage.get("id") !== null && typeof utilities.local_storage.get("id") !== "undefined" &&
                utilities.local_storage.get("id") !== ""){
                self.saveComment(self.addComment, utilities.formToObject(this));
                if (!$(".commentTripBtn").hasClass("trip-action")) {
                    $(".commentTripBtn").addClass("trip-action");
                    num_comments = parseInt($(".commentTripBtn").find(".count").html());
                    $(".commentTripBtn").find(".count").html(num_comments + 1);
                }
            }
            $(this).closest('form').find("input[type=text], textarea").val("");

            return false;
        });

        var _reportCommentID = undefined;
        if (utilities.local_storage.get("id") === null || typeof utilities.local_storage.get("id") == "undefined" ||
            utilities.local_storage.get("id") === "") {
            $(document).on("click", ".__c_reportComment", function(ev) {
                ev.preventDefault();

                $('#loginModal').foundation('reveal', 'open');
            });  
        }else{
            $(document).on("click", ".__c_reportComment", function(ev) {
                ev.preventDefault();

                _reportCommentID = $(this).data('id');

                $('#reportModal').foundation('reveal', 'open');
            });  
        }

        $("#reportForm input[name='reason']").on("click", function() {
            if ($(this).val() == 3) {
                $("#reportForm input[name='remarks']").show().val("");
            } else {
                $("#reportForm input[name='remarks']").hide().val("");
            }
        });

        $("#reportForm").on("submit", function() {
            self.reportUser(_reportCommentID, function() {
                // Close the report modal
                $('#reportModal').foundation('reveal', 'close');

                // Display a notification
                common.notify({
                    'title': "",
                    'text': "Comment Reported. Thank you.",
                    'type': "success"
                });
            });

            return false;
        });

        $(document).on("scroll", utilities.debounce(250, function() {
            self._highlightMilestone();
        }));

    },

    _highlightMilestone: function() {
        // Get all milestone rows
        var $rows = $("#milestonesContainer > .row.no-margin");

        // Calculate top of each row
        var tops = [],
            top = undefined;

        for (var i = 0, l = $rows.length; i < l; i++) {
            top = $($rows[i]).offset().top - $(document).scrollTop();
            if (top < 0) top = Infinity;

            tops.push(top);
        }

        // Find out the minimum top
        // Math.min.apply( Math, tops )

        // Fetch element with least top
        var el = $rows[tops.indexOf(Math.min.apply(Math, tops))];

        if (!el) return;

        // Get milestone id
        var milestoneID = $(el).data("milestone-id");

        // Highlight appropriate milestone
        $("aside a[href=#__c_milestone" + milestoneID + "]").parent().trigger("setActive");
        this._doInactiveMarkers(this.sidebarMapMarkers);
        this.sidebarMapMarkers[milestoneID].toggleActive();
    },

    sidebarmap: null,

    fullViewMap: null,

    sidebarMapMarkers: {},
    fullMapMarkers: {},

    markers: [],

    sidebarmapbounds: null,

    tripID: null,

    // Fetch trip info from server
    getTripData: function(fetchCallback) {
        var self = this;

        // Fetch complete trip data
        $.ajax({
            type: "GET",
            async: true,
            headers: {
                "X-Auth-Token": utilities.cookies.getItem("auth_token")
            },
            url: Config.baseUrl + "/trips/" + this.tripID,
            success: function(data, textStatus, jqXHR) {
                if (fetchCallback) fetchCallback.apply(self, [data]);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log("unable to fetch trip data " + errorThrown);
            }
        });
    },

    // Print details of trip
    printTripData: function(tripDetails) {
        this.setFeaturedImageDeafult(tripDetails.coverImg);

        $(".__c_tripTitle").text(tripDetails.title);
        $(".__c_tripDesc").text(tripDetails.description);
        $(".__c_tripStartPoint").text(tripDetails.locations.start_point.name);
        $(".__c_tripEndPoint").text(tripDetails.locations.end_point.name);
        $(".__c_tripViewsCount").text(tripDetails.view_count);
        $(".__c_tripPublishDate").text(tripDetails.published_at);
        $(".__c_tripSharedCount").hide();

        while (('' + tripDetails.num_days).length < 2) tripDetails.num_days = "0" + tripDetails.num_days;
        while (('' + tripDetails.num_miles).length < 2) tripDetails.num_miles = "0" + tripDetails.num_miles;
        while (('' + tripDetails.num_milestones).length < 2) tripDetails.num_milestones = "0" + tripDetails.num_milestones;
        while (('' + tripDetails.num_moments).length < 2) tripDetails.num_moments = "0" + tripDetails.num_moments;

        $("#tripDays").html(tripDetails.num_days);
        $("#tripMiles").html((tripDetails.num_miles*1.6).toFixed());
        $("#tripMilestone").html(tripDetails.num_milestones);
        $("#tripMoment").html(tripDetails.num_moments);
        $("#transportModeIcon").addClass(common.transportModes[+tripDetails.transport_mode - 1]['iconClass']);

        // Clear all milestones, initially
        $("#milestonesContainer").empty();

        if(tripDetails.isLiked)
        {
            $(".likeTripBtn").addClass("trip-action");
        }
        if (tripDetails.hasCommented) {
            $(".commentTripBtn").addClass("trip-action");
        }

        $(".likeTripBtn").find("span").html(tripDetails.likes);
        $(".commentTripBtn").find(".count").html(tripDetails.comments.length);

        for (var i = 0; i < tripDetails.milestones.length; i++) {
            tripDetails.milestones[i]["day_of_month"] = create_roadtrip.getDayFromDate(tripDetails.milestones[i].reached_on);
            tripDetails.milestones[i]["month"] = create_roadtrip.getMonthFromDate(tripDetails.milestones[i].reached_on);
            this.addMilestone(tripDetails.milestones[i]);
            if ( i < tripDetails.milestones.length -1)
                this.addRoadCondition(tripDetails.milestones[i]);

            // Display independent moments
            for (j = 0; j < tripDetails.milestones[i].moments.length; j++) {
                tripDetails.milestones[i].moments[j]['milestoneID'] = tripDetails.milestones[i].moments[j]['at_milestone'];
                this.addMoment(tripDetails.milestones[i].moments[j]);
            }
        }

        // Display independent moments
        for (i = 0; i < tripDetails.moments.length; i++) {
            tripDetails.moments[i]['milestoneID'] = tripDetails.moments[i]['after_milestone'];
            this.addMoment(tripDetails.moments[i]);
        }

        create_roadtrip.milestones.addToList(tripDetails.milestones);

        // Print comments
        for (i = 0, l = tripDetails.comments.length; i < l; i++) {
            this.addComment(tripDetails.comments[i]);
        }

        this.getUserData(this.printUserData, tripDetails.user_id);
        this.creategallery();
    },

    // Fetch trip info from server
    getUserData: function(fetchCallback, user_id) {
        var self = this;

        // Fetch complete trip data
        $.ajax({
            type: "GET",
            async: true,
            headers: {
                "X-Auth-Token": utilities.cookies.getItem("auth_token")
            },
            url: Config.baseUrl + "/users/" + user_id,
            success: function(data, textStatus, jqXHR) {
                if (fetchCallback) fetchCallback.apply(self, [data]);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log("unable to fetch trip data " + errorThrown);
            }
        });
    },

    // Print details of trip
    printUserData: function(userDetails) {
        if (userDetails.name!= null) {
            $(".names").text(userDetails.name)
            .attr('href','/html/profile_view.html?_loc=otherProfile&id=' + userDetails.id);
            $(".__c_tripUserName").text(userDetails.name)
            .attr('href','/html/profile_view.html?_loc=otherProfile&id=' + userDetails.id);
        }else{
            $(".names").text(userDetails.username)
            .attr('href','/html/profile_view.html?_loc=otherProfile&id=' + userDetails.id);
            $(".__c_tripUserName").text(userDetails.username)
            .attr('href','/html/profile_view.html?_loc=otherProfile&id=' + userDetails.id);
        }
        if (userDetails.userPic) {
            $(".trip-info-wrapper").find(".user-photo")
            .css('background-image',"url('" + userDetails.userPic + "')")
            .attr('href','/html/profile_view.html?_loc=otherProfile&id=' + userDetails.id);
        }
        else{
            $(".trip-info-wrapper").find(".user-photo")
            .css('background-image',"url(../images/default_profile.png)")
            .attr('href','/html/profile_view.html?_loc=otherProfile&id=' + userDetails.id);
        }
        $(".info").find(".count").text(userDetails.published_trips);

        $(".following").find("span").html(userDetails.following);

        if (userDetails.userPic) {
            $(".user-info").find(".user-photo")
            .css('background-image',"url('" + userDetails.userPic + "')")
            .attr('href','/html/profile_view.html?_loc=otherProfile&id=' + userDetails.id);
        }else{
            $(".user-info").find(".user-photo")
            .attr('href','/html/profile_view.html?_loc=otherProfile&id=' + userDetails.id);
        }


        $(".followers").find("span").html(userDetails.followers);
        $(".bio").find("p").html(userDetails.bio);
        if ( userDetails.id != utilities.local_storage.get("id")) {
           common.createFollowButton(".trip-foo-flw-btn2", {
                'data': {
                    'id': userDetails.id
                }
            }, {
                'data': {
                    'id': userDetails.id
                }
            }, !userDetails.is_followed, userDetails.is_followed, false);
        } else {
            $(".user-info").find(".user-photo")
            .attr('href','/html/profile_view.html');
        }
        $(".button-following").removeClass('right').addClass(".left");
        $(".button-follow").removeClass('right').addClass(".left");

        //When user is logged out

        if (utilities.local_storage.get("id") === null || typeof utilities.local_storage.get("id") == "undefined" ||
                utilities.local_storage.get("id") === "") {
            $(".names").attr('data-reveal-id','loginModal');  
            $(".trip-info-wrapper").find(".user-photo").attr('data-reveal-id','loginModal'); 
            $(".__c_tripUserName").attr('data-reveal-id','loginModal');
            $(".user-info").find(".user-photo").attr('data-reveal-id','loginModal');
            $(".trip-foo-flw-btn2").attr('data-reveal-id','loginModal');
            $("#addCommentForm").attr('data-reveal-id','loginModal');
            $(".commentTripBtn").attr('data-reveal-id','loginModal');
            $(".likeTripBtn").attr('data-reveal-id','loginModal');
            $(".comment-input").find(".user-photo").attr('data-reveal-id','loginModal');
            $(".comment-activity").find(".user-photo, .commenter").attr('data-reveal-id','loginModal');
        }

    },



    // Set featured image in banner
    setFeaturedImageDeafult: function(featuredImage) {
        if (!featuredImage){
            $bannerContainer = $(".trip-banner");
            $bannerContainer.addClass("has-banner-img");
            $bannerContainer.css("background-image", "url(/images/default_cover.jpg)");
        }

        else {
            $bannerContainer = $(".trip-banner");
            $bannerContainer.addClass("has-banner-img");
            $bannerContainer.css("background-image", "url(" + featuredImage + ")");
        }
    },

    // Set featured image in banner
    setFeaturedImage: function(featuredImage) {
        if (!featuredImage){
            $(".trip-banner").css("background-image",'none');
        }
        else {
            $bannerContainer = $(".trip-banner");
            $bannerContainer.addClass("has-banner-img");
            $bannerContainer.css("background-image", "url(" + featuredImage + ")");
        }
    },

    addMilestone: function(milestoneDetails) {
        var $milestonesContainer = $("#milestonesContainer");
        var theTemplateScript = $("#milestone_template").html();
        var theTemplate = Handlebars.compile(theTemplateScript);

        $milestonesContainer.append(theTemplate(milestoneDetails));

        // Markers in sidebar
        var marker = road_trip_view.createMarker({
            'latlng': new google.maps.LatLng(milestoneDetails.latitude, milestoneDetails.longitude),
            'index': milestoneDetails.position
        });
        marker.setMap(this.sidebarmap);
        this.sidebarMapMarkers[milestoneDetails.id] = marker;
        this.fitBounds(this.sidebarmap, this.sidebarMapMarkers);

        // Markers in full map view
        marker = road_trip_view.createMarker({
            'latlng': new google.maps.LatLng(milestoneDetails.latitude, milestoneDetails.longitude),
            'index': milestoneDetails.position
        });
        marker.setMap(this.fullViewMap);
        this.fullMapMarkers[milestoneDetails.id] = marker;

        if(milestoneDetails.images.length > 0) {
            this.fillImageViewer(milestoneDetails.images);
        }

        this.fitBounds(this.fullViewMap, this.fullMapMarkers);

        //utilities.fillDropDown(".__c_road_condition" + milestoneDetails.id, common.roadConditions, 'name', 'value');
    },

    fitBounds: function(map, markers) {
        var self = this;

        $.each(markers, function(key, value) {
            self.sidebarmapbounds.extend(value.getPosition());
        });
        map.fitBounds(this.sidebarmapbounds);
    },

    createZoomControl: function(map) {
        // Create a div to hold the control.
        var controlDiv = document.createElement('div');

        // Set CSS styles for the DIV containing the control
        // Setting padding to 45 px will offset the control
        // from the edge of the map.
        controlDiv.style.padding = '10px';

        var zoomInHelper = this.createZoomHelper("+");
        google.maps.event.addDomListener(zoomInHelper, 'click', function() {
            map.setZoom(map.getZoom() + 1);
        });
        controlDiv.appendChild(zoomInHelper);

        var zoomOutHelper = this.createZoomHelper("-");
        google.maps.event.addDomListener(zoomOutHelper, 'click', function() {
            if (map.getZoom() == 2) return;
            map.setZoom(map.getZoom() - 1);
        });
        controlDiv.appendChild(zoomOutHelper);

        // We don't really need to set an index value here, but
        // this would be how you do it. Note that we set this
        // value as a property of the DIV itself.
        controlDiv.index = 1;

        // Add the control to the map at a designated control position
        // by pushing it on the position's array. This code will
        // implicitly add the control to the DOM, through the Map
        // object. You should not attach the control manually.
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(controlDiv);
    },

    createZoomHelper: function(text) {
        // Set CSS for the control border.
        var controlUI = document.createElement('div');
        controlUI.title = 'Zoom In';
        controlUI.className = "zoomControl";

        // Set CSS for the control interior.
        var controlText = document.createElement('div');
        controlText.style.fontSize = '24px';
        controlText.style.fontFamily = 'Arial';
        controlText.style.fontWeight = '700';
        controlText.style.lineHeight = '32px';
        controlText.style.color = 'white';
        controlText.innerHTML = text;
        controlUI.appendChild(controlText);

        return controlUI;
    },

    // Create marker for sidebar map
    createMarker: function(markerDetails) {
        return new RM_Marker({
            'latlng': markerDetails.latlng,
            'isActive': false,
            'iconText': markerDetails.index
        });
    },

    showImageViewer: function() {
        $("#imageViewer").show();
    },

    // Fetch list of images for image viewer
    fetchImages: function(fetchCallback) {
        var self = this;

        // AJAX call to fetch images goes here

        var arrayOfImages = [];

        fetchCallback.apply(self, [arrayOfImages]);
    },

    countOfImages: 0,

    fillImageViewer: function(listOfImages) {
        var i = 0,
            l = listOfImages.length,
            $imagesContainer = $("#imageViewer_images");

        for (i = 0; i < l; i++) {
            $imagesContainer.append(this.createImageEl(listOfImages[i]));
        }

        this.countOfImages += listOfImages.length;
        $(".total").text(this.countOfImages);
    },

    createImageEl: function(imageDetails) {
        var $li = utilities.createElement("<li/>", null, {});
        var $img_link = utilities.createElement("<a/>", $li, {
            'href': "#",
            'style': 'height:50px;width:69px;overflow:hidden;'
        });
        utilities.createElement("<img/>", $img_link, {
            'src': imageDetails,
            'data-large': imageDetails,
            'alt': "",
            'style': "min-height:50px;min-width:69px;",
            'data-description': "image description"
        });

        return $li;
    },

    likeThisTrip: function() {
        var self = this;

        if ($(".likeTripBtn").hasClass("trip-action")){
            $(".likeTripBtn").removeClass("trip-action");
            $.ajax({
                type: "GET",
                async: true,
                headers: {
                    "X-Auth-Token": utilities.cookies.getItem("auth_token")
                },
                url: Config.baseUrl + "/trips/" + self.tripID + "/unlike",
                success: function(data) {
                    $(".likeTripBtn").find("span").html(" " + data.likes_count);
                }
            });
        }else{
            // AJAX call to like this trip goes here
            $(".likeTripBtn").addClass("trip-action");
            $.ajax({
                type: "GET",
                url: Config.baseUrl + "/trips/" + self.tripID + "/like",
                async: true,
                headers: {
                    "X-Auth-Token": utilities.cookies.getItem("auth_token")
                },
                success: function(data) {
                    // on success
                    $(".likeTripBtn").find("span").html(" " + data.likes_count);
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log("unable to like trip " + errorThrown);
                }
            });
        }
    },

    creategallery: function() {
        $(function() {
        // ======================= imagesLoaded Plugin ===============================
        // https://github.com/desandro/imagesloaded

        // $('#my-container').imagesLoaded(myFunction)
        // execute a callback when all images have loaded.
        // needed because .load() doesn't work on cached images

        // callback function gets image collection as argument
        //  this is the container

        // original: mit license. paul irish. 2010.
        // contributors: Oren Solomianik, David DeSandro, Yiannis Chatzikonstantinou

        $.fn.imagesLoaded = function( callback ) {
            var $images = this.find('img'),
                len     = $images.length,
                _this   = this,
                blank   = 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///ywAAAAAAQABAAACAUwAOw==';

            function triggerCallback() {
                callback.call( _this, $images );
            }

            function imgLoaded() {
                if ( --len <= 0 && this.src !== blank ){
                    setTimeout( triggerCallback );
                    $images.off( 'load error', imgLoaded );
                }
            }

            if ( !len ) {
                triggerCallback();
            }

            $images.on( 'load error',  imgLoaded ).each( function() {
                // cached images don't fire load sometimes, so we reset src.
                if (this.complete || this.complete === undefined){
                    var src = this.src;
                    // webkit hack from http://groups.google.com/group/jquery-dev/browse_thread/thread/eee6ab7b2da50e1f
                    // data uri bypasses webkit log warning (thx doug jones)
                    this.src = blank;
                    this.src = src;
                }
            });

            return this;
            };

            // gallery container
            var $rgGallery          = $('#rg-gallery'),
            // carousel container
            $esCarousel         = $rgGallery.find('div.es-carousel-wrapper'),
            // the carousel items
            $items              = $esCarousel.find('ul > li'),
            // total number of items
            itemsCount          = $items.length;
            
            Gallery             = (function() {
                    // index of the current item
                var current         = 0, 
                    // mode : carousel || fullview
                    mode            = 'carousel',
                    // control if one image is being loaded
                    anim            = false,
                    init            = function() {
                        
                        // (not necessary) preloading the images here...
                        $items.add('<img src="../images/ajax-loader.gif"/><img src="../images/black.png"/>').imagesLoaded( function() {
                            // add options
                            _addViewModes();
                            
                            // add large image wrapper
                            _addImageWrapper();
                            
                            // show first image
                            _showImage( $items.eq( current ) );
                                
                        });
                        
                        // initialize the carousel
                        if( mode === 'carousel' )
                            _initCarousel();
                        
                    },
                    _initCarousel   = function() {
                        
                        // we are using the elastislide plugin:
                        // http://tympanus.net/codrops/2011/09/12/elastislide-responsive-carousel/
                        $esCarousel.show().elastislide({
                            imageW  : 65,
                            onClick : function( $item ) {
                                if( anim ) return false;
                                anim    = true;
                                // on click show image
                                _showImage($item);
                                // change current
                                current = $item.index();
                                $(".nth").text(current + 1);
                            }
                        });
                        
                        // set elastislide's current to current
                        $esCarousel.elastislide( 'setCurrent', current );
                        
                    },
                    _addViewModes   = function() {
                        
                        // top right buttons: hide / show carousel
                        
                        var $viewfull   = $('<a href="#" class="rg-view-full"></a>'),
                            $viewthumbs = $('<a href="#" class="rg-view-thumbs rg-view-selected"></a>');
                        
                        // $rgGallery.prepend( $('<div class="rg-view"/>').append( $viewfull ).append( $viewthumbs ) );
                        $rgGallery.prepend( $viewthumbs );
                        
                        $viewfull.on('click.rgGallery', function( event ) {
                                if( mode === 'carousel' )
                                    $esCarousel.elastislide( 'destroy' );
                                $esCarousel.hide();
                            $viewfull.addClass('rg-view-selected');
                            $viewthumbs.removeClass('rg-view-selected');
                            mode    = 'fullview';
                            return false;
                        });
                        
                        $viewthumbs.on('click.rgGallery', function( event ) {
                            _initCarousel();
                            $viewthumbs.addClass('rg-view-selected');
                            $viewfull.removeClass('rg-view-selected');
                            mode    = 'carousel';
                            return false;
                        });
                        
                        if( mode === 'fullview' )
                            $viewfull.trigger('click');
                            
                    },
                    _addImageWrapper= function() {
                        
                        // adds the structure for the large image and the navigation buttons (if total items > 1)
                        // also initializes the navigation events
                        
                        $('#img-wrapper-tmpl').tmpl( {itemsCount : itemsCount} ).appendTo( $rgGallery );
                        
                        if( itemsCount > 1 ) {
                            // addNavigation
                            var $navPrev        = $rgGallery.find('span.es-nav-prev'),
                                $navNext        = $rgGallery.find('span.es-nav-next'),
                                $imgWrapper     = $rgGallery.find('div.rg-image');
                                
                            $navPrev.on('click.rgGallery', function( event ) {
                                _navigate( 'left' );
                                return false;
                            }); 
                            
                            $navNext.on('click.rgGallery', function( event ) {
                                _navigate( 'right' );
                                return false;
                            });
                        
                            // add touchwipe events on the large image wrapper
                            $imgWrapper.touchwipe({
                                wipeLeft            : function() {
                                    _navigate( 'right' );
                                },
                                wipeRight           : function() {
                                    _navigate( 'left' );
                                },
                                preventDefaultEvents: false
                            });
                        
                            $(document).on('keyup.rgGallery', function( event ) {
                                if (event.keyCode == 39)
                                    _navigate( 'right' );
                                else if (event.keyCode == 37)
                                    _navigate( 'left' );    
                            });
                            
                        }
                        
                    },
                    _navigate       = function( dir ) {
                        
                        // navigate through the large images
                        
                        if( anim ) return false;
                        anim    = true;
                        
                        if( dir === 'right' ) {
                            if( current + 1 >= itemsCount )
                                current = 0;
                            else
                                ++current;
                        }
                        else if( dir === 'left' ) {
                            if( current - 1 < 0 )
                                current = itemsCount - 1;
                            else
                                --current;
                        }
                        
                        _showImage( $items.eq( current ) );
                        $(".nth").text(current + 1);
                        
                    },
                    _showImage      = function( $item ) {
                        
                        // shows the large image that is associated to the $item
                        
                        var $loader = $rgGallery.find('div.rg-loading').show();
                        
                        $items.removeClass('selected');
                        $item.addClass('selected');
                             
                        var $thumb      = $item.find('img'),
                            largesrc    = $thumb.data('large'),
                            title       = $thumb.data('description');
                        
                        $('<img/>').load( function() {
                            
                            $rgGallery.find('div.rg-image').empty().append('<img src="' + largesrc + '"/>');
                            
                            if( title )
                                $rgGallery.find('div.rg-caption').show().children('p').empty().text( title );
                            
                            $loader.hide();
                            
                            if( mode === 'carousel' ) {
                                $esCarousel.elastislide( 'reload' );
                                $esCarousel.elastislide( 'setCurrent', current );
                            }
                            
                            anim    = false;
                            
                        }).attr( 'src', largesrc );
                        
                    },
                    addItems        = function( $new ) {
                    
                        $esCarousel.find('ul').append($new);
                        $items      = $items.add( $($new) );
                        itemsCount  = $items.length;
                        $esCarousel.elastislide( 'add', $new );
                    
                    };
                    setNewCurrentImage = function(url) {
                        var i = 0;
                        for(i = 0; i < $items.length; i++) {
                            var itemUrl = $items[i].firstElementChild.firstChild.src;
                            if (url == itemUrl) {
                                $esCarousel.elastislide( 'setCurrent', i );
                                $(".nth").text(i + 1);
                                _showImage( $items.eq( i ) );
                                current = i;
                                break;
                            }
                        }
                    };
                
                return {
                    init        : init,
                    addItems    : addItems,
                    setNewCurrentImage : setNewCurrentImage
                };
            
            })();

            Gallery.init();

            // show image viewer on image click
            $(".myThumbDivAutoAdd").on("click", function() {
                Gallery.setNewCurrentImage($(this).attr("src"));
                $('#imageViewer').foundation('reveal', 'open');
            });
            
            /*
            Example to add more items to the gallery:
            
            var $new  = $('<li><a href="#"><img src="images/thumbs/1.jpg" data-large="images/1.jpg" alt="image01" data-description="From off a hill whose concave womb reworded" /></a></li>');
            Gallery.addItems( $new );
            */
        });
    },

    // save a newly added comment to database
    saveComment: function(fetchCallback, postData) {
        var self = this;

        $.ajax({
            type: "POST",
            url: Config.baseUrl + "/trips/" + this.tripID + "/comments",
            async: true,
            headers: {
                "X-Auth-Token": utilities.cookies.getItem("auth_token")
            },
            data: {
                "comment": postData
            },
            success: function(data, textStatus, jqXHR) {
                // result received here

                if (fetchCallback) fetchCallback.apply(self, [data]);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log("unable to add comment " + errorThrown);
            }
        });
    },

    // add a comment to list of comments
    addComment: function(commentDetails) {
        var $commentsContainer = $("#commentsContainer");
        var theTemplateScript = $("#comments_template").html();
        var theTemplate = Handlebars.compile(theTemplateScript);

        $commentsContainer.prepend(theTemplate(commentDetails));
    },

    reportUser: function(commentID, fetchCallback) {
        var self = this,
            postData = utilities.formToObject("#reportForm");

        $.ajax({
            type: "POST",
            url: Config.baseUrl + "/comments/" + commentID + "/report_abuse",
            async: true,
            headers: {
                "X-Auth-Token": utilities.cookies.getItem("auth_token")
            },
            data: {
                'comment': postData
            },
            success: function(data, textStatus, jqXHR) {
                // result received here
                if (fetchCallback) fetchCallback.apply(self, [data]);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log("unable to report user " + errorThrown);
            }
        });
    },

    addRoadCondition: function(milestoneDetails) {
        var roadConditionContainer = $("#roadCondition" + milestoneDetails.id);
        var theTemplateScript = $("#road_condition_template").html();
        var theTemplate = Handlebars.compile(theTemplateScript);

        milestoneDetails.road_condition_name = "Not provided by the creator";
        if (milestoneDetails.road_condition !== null)
            for( var i = 0; i < common.roadConditions.length; i++) {
                if (common.roadConditions[i]["value"] == milestoneDetails.road_condition) {
                    milestoneDetails.road_condition_name = common.roadConditions[i]["name"];
                    break;
                }
            }

        roadConditionContainer.append(theTemplate(milestoneDetails));
    },

    addMoment: function(momentDetails) {
        momentDetails.milestoneID = momentDetails.after_milestone || momentDetails.at_milestone;
        var momentsContainer = $("#momentsContainer" + momentDetails.milestoneID);
        var theTemplateScript = $("#moment_template").html();
        var theTemplate = Handlebars.compile(theTemplateScript);

        momentsContainer.append(theTemplate(momentDetails));

        if(momentDetails.images.length > 0) {
            this.fillImageViewer(momentDetails.images);
        }
    },
    _doInactiveMarkers: function(markers) {
        $.each(markers, function(key, value) {
            value.setInactive();
        });
    }
};

var create_roadtrip = {
    initialize: function() {
        var self = this;

        this.tripID = utilities.getURLVar("id") || 0;

        this.sidebarmap = this.createSidebarMap();
        this.sidebarmap.setOptions({
            "disableDoubleClickZoom": true
        });
        this.mapbounds = new google.maps.LatLngBounds();

        this.fullViewMap = this.createFullScreenMap();

        common.fetchProfileData(function(data) {
            signin.saveDataInLocalStorage(data);
            common.fillProfileData(data);
        });

        $(document).on('opened.fndtn.reveal', '#fullMapModal', function() {
            google.maps.event.trigger(self.fullViewMap, "resize");
            self.fitBounds(self.fullViewMap, self.fullMapMarkers);
        });

        $("#fullMapModal").on("click", ".__c_milestonesList a", function(ev) {
            ev.preventDefault();

            var milestoneID = +$(this).data("milestone-id");
            self._doInactiveMarkers(self.fullMapMarkers);
            self.fullMapMarkers[milestoneID].toggleActive();
        });

        var $root = $('html, body');
        $("aside").on("click", ".__c_milestonesList a", function() {
            var milestoneID = +$(this).data("milestone-id");
            self._doInactiveMarkers(self.sidebarMapMarkers);
            self.sidebarMapMarkers[milestoneID].toggleActive();

            $root.animate({
                scrollTop: $($.attr(this, 'href')).offset().top
            }, 800);
            return false;
        });

        $(document).on('click','#milestoneEditButton', function(ev) {
            ev.preventDefault();
            $("#__c_milestone" + $(this).data('milestone-id')).find(".inline-milestone-desc").focus();
        });


        $(document).on('click','#momentEditButton', function(ev) {
            ev.preventDefault();
            $("#newtitlemoment" + $(this).attr("data-id")).find(".place-moment-heading-editor").focus();
        });


        $(document).on('open.fndtn.reveal', '#milestoneAddPopup,#momentAddPopup', function() {
            var $modal = $(this);

            // Remove previous marker, if any
            self.milestoneMarker && self.milestoneMarker.setMap(null);
            self.milestoneMarker = null;

            $modal.find("input[type='text'],input[type='search']").val("");
            $modal.find("input[type='checkbox']").prop("checked", false);
        });

        $(document).on('click', 'a.__c_addMilestoneButton', function(ev) {
            ev.preventDefault();

            var milestonePosition = $(this).data('milestone-position');
            // set milestone position to after milestone
            $.each($("#milestoneAddPopup .__c_milestones_position").find('li'), function(index, el) {
                if (milestonePosition == $(el).data('value')) {
                    $(el).trigger('click');
                }
            });

            // show #milestoneAddPopup
            $('#milestoneAddPopup').foundation('reveal', 'open');
        });

        $(document).on('click', 'a.del-thumb', function(ev)  {
            ev.preventDefault();
            var link = $(this).parent().find("img").attr('src').split('/');
            if ( link && link.length > 8) {
                common._confirmsDelete = link[8];
                common._confirmsDeleteType = "milestones";
            }
        });

        $(document).on('click', 'a.del-thumb-moment', function(ev)  {
            ev.preventDefault();
            var link = $(this).parent().find("img").attr('src').split('/');
            if ( link && link.length > 8) {
                common._confirmsDelete = link[8];
                common._confirmsDeleteType = "moments";
            }
        });

        $(document).on('click', 'a.del-milestone', function(ev)  {
            ev.preventDefault();
            common._confirmsDelete = $(this).attr("data-id");
            common._confirmsDeleteType = "milestones";
        });

        $(document).on('click', 'a.del-moment', function(ev)  {
            ev.preventDefault();
            common._confirmsDelete = $(this).attr("data-id");
            common._confirmsDeleteType = "moments";
        });

        $(document).on('click', 'a.__c_addMomentButton', function(ev) {
            ev.preventDefault();

            var milestoneID = $(this).data('milestone-id');

            self.moments.initCategorySelection();
            $("#momentAddPopup").find(".icon-pxl-moment-stay-active").removeClass("icon-pxl-moment-stay-active").addClass("icon-pxl-moment-stay");
            $("#momentAddPopup").find(".icon-pxl-moment-food-active").removeClass("icon-pxl-moment-food-active").addClass("icon-pxl-moment-food");
            $("#momentAddPopup").find(".icon-pxl-moment-see-active").removeClass("icon-pxl-moment-see-active").addClass("icon-pxl-moment-see");
            $("#momentAddPopup").find(".icon-pxl-moment-do-do").removeClass("icon-pxl-moment-stay-do").addClass("icon-pxl-moment-do");
            $("#momentAddPopup").find(".icon-pxl-moment-experience-active").removeClass("icon-pxl-moment-experience-active").addClass("icon-pxl-moment-experience");
            // $("#momentAddPopup").find(".icon-pxl-moment-stay-active").removeClass("icon-pxl-moment-stay-active").addClass("icon-pxl-moment-stay");

            
            // $("#momentAddPopup").find("input[name='sub_category_id']").val("0");
            //reset the subcat html
            $("#momentAddPopup").find("input[name='sub_category_id']").val("0");
            $("#momentAddPopup .c-select-wrapper").find(".selected span:first-child").html("Select Subcategory");
            $("#momentAddPopup").find(".__c_subcats").html("");
            self.moments.selectedCategory = null;

            // set milestone ID to at/after milestone
            $.each($("#momentAddPopup .__c_milestones").find('li'), function(index, el) {
                if (milestoneID == $(el).data('value')) {
                    $(el).trigger('click');
                    $(".__c_afterMilestoneDrp").find("span:first").html($(el).html());
                }
            });
            $(".__c_momentAtDrp").find("span:first").html("At milestone");

            // show #momentAddPopup
            $('#momentAddPopup').foundation('reveal', 'open');
        });

        /*$(document).on('open.fndtn.reveal', '#momentAddPopup', function() {
            var $modal = $(this);

            $modal.find("input[type='text']").val("");
        })*/

         // For features, Coming Soon, notify user
        $(".__c_comingSoon").on("click", function(ev) {
            ev.preventDefault();
            ev.stopPropagation();

            common.notify({
                'title': "Coming Soon",
                'text': "This features is still work in progress. We will launch this in on of our future releases",
                'type': ""
            });
        });
        this.createModalMap_2();
        var imagesDzn = this.createDropzone();
        this.fetchCategories(this.categoriesFetched);

        this.moments.initCategorySelection();

        // Initialize datepicker
        $('.datepicker').fdatepicker();

        $("textarea").autosize();

        // Milestone modal setup
        $(".latLngWrap").hide();
        $("#latLngSwitch,#locationSwitch").on("click", function(ev) {
            ev.preventDefault();

            $(".locationWrap,.latLngWrap").find("input").val("");
            $(".locationWrap,.latLngWrap").toggle();
        });

        this.getTripData(this.displayCompleteTripData);

        // upload featured image
        var featuredImageUploadBtnState = 0,
            $featuredImageForm = $("#featuredImageForm"),
            $imageCropForm = $("#imageCropForm");



        $featuredImageForm.on("submit", function() {
            if (featuredImageUploadBtnState == 1) return;

            $("#featuredImageUploadBtn").text("Uploading...");
            featuredImageUploadBtnState = 1;

            var formData = new FormData();
            formData.append("attachment",$("input[name='attachment'")[0].files[0]);
            $.ajax({
                contentType: false,
                processData: false,
                xhr: function() {
                    var xhr = new window.XMLHttpRequest();
                    xhr.upload.addEventListener("progress", function(evt) {
                        if (evt.lengthComputable) {
                            var percentComplete = (Math.round(evt.loaded / evt.total * 100) + "%");
                            $("#featuredImageUploadBtn").text("Uploading..." + percentComplete);
                        }
                   }, false);

                   xhr.addEventListener("progress", function(evt) {
                       if (evt.lengthComputable) {
                            var percentComplete = (Math.round(evt.loaded / evt.total * 100) + "%");
                            $("#featuredImageUploadBtn").text("Uploading..." + percentComplete);
                       }
                   }, false);

                   return xhr;
                },
                type: 'POST',
                url: Config.baseUrl + "/trips/" + self.tripID + "/upload_image",
                data: formData,
                headers: {
                    "X-Auth-Token": utilities.cookies.getItem("auth_token")
                },
                
                success: function(data) {
                    $("#featuredImageUploadBtn").text("Upload");
                    featuredImageUploadBtnState = 0;

                    // Image Crop Tool initialize
                    /*$("#coverImageToCrop").attr('src', responseText.coverImg).load(function() {
                        var width = $(this).width();
                        // Initialize crop tool
                        $(this).Jcrop({
                            aspectRatio: 1200 / 300,
                            setSelect: [0, 0, width, width / 4],
                            onSelect: function(c) {
                                self.ciCropCoords = c;
                            }
                        });
                    });*/

                    //$featuredImageForm.hide();
                    //$imageCropForm.show();

                    // Close the modal when image is uploaded
                    $("#uploadImage").foundation('reveal', 'close');

                    // Set cover image
                    road_trip_view.setFeaturedImage(data.coverImg);

                    common.notify({
                        'title': "Featured Image updated!",
                        'text': "The featured image has been updated",
                        'type': "success"
                    });

                    return false;
                }
            });

            return false;
        });

        // Cover Image Crop
        /*$imageCropForm.on("submit", function() {
            $.ajax({
                type: "POST",
                url: "",
                data: self.ciCropCoords,
                async: true,
                headers: {
                    "X-Auth-Token": utilities.cookies.getItem("auth_token")
                },
                success: function(data) {
                    // coordinates received successfully
                },
                error: function() {
                    console.log("Unable to crop image. Coordinates not sent.");
                }
            });

            $featuredImageForm.hide();
            $imageCropForm.show();

            return false;
        });*/

        $("#cancelButton").on("click", function(ev) {
            ev.preventDefault();
            ev.stopPropagation();
            $('#confirmDeleteModal').foundation('reveal', 'open');
        });

        $("#cancelButtonConfirm").on("click", function(ev) {
            ev.preventDefault();
            ev.stopPropagation();
            self.cancelTrip();
        });

        // Save current roadtrip as draft
        $("#saveDraftButton").on("click", function(ev) {
            ev.preventDefault();
            ev.stopPropagation();

            self.saveTripDraft();
        });

        $("#roadtripPublishCheck").on("click", function(ev) {
            ev.preventDefault();
            ev.stopPropagation();

            if( $("#title").val().length == "0") {
                $('#noTitleModal').foundation('reveal', 'open');
            } else {
                // $("#showReleaseTooltip").tooltip({
                //     title : 'Feature coming in the next release!'
                // });
                // $("#showReleaseTooltip").click(function() {
                //     $("#showReleaseTooltip").tooltip('show');
                // });
                $('#roadtripPublishPopup').foundation('reveal', 'open');
            }
        });

        // Publish roadtrip - Actually, this just sets the trip status to PUBLISHED
        $("#publishButton").on("click", function(ev) {
            ev.preventDefault();
            ev.stopPropagation();

            self.publishTrip(self.onTripPublished);
        });

        $("#addMilestoneButton").on("click", function() {
            self.createMilestone(self.displayCompleteTripData);
        });

        $("#addMomentButton").on("click", function() {
            self.createMoment(self.addMoment);
        });

        // When user starts adding photos to a moment/milestone
        $(document).on('click', '.__c_addPhotos', function(ev) {
            ev.preventDefault();

            $("#dzUpload").data('type', $(this).data("type"));
            $("#dzUpload").data('id', $(this).data("id"));

            // show the photo upload modal
            $('#uploadImageMoment').foundation('reveal', 'open');
        });

        $("#uploadImageMoment").find(".close-reveal-modal").on("click", function(ev) {
            ev.preventDefault();
            imagesDzn.removeAllFiles(true);
            $('#uploadImageMoment').foundation('reveal', 'close');
            $("#uploadImageMoment").find("button").html("UPLOAD");
        });

        $(document).on('open.fndtn.reveal', '#uploadImageMoment', function() {
            var modal = $(this);

            var $dzUpload = $("#dzUpload"),
                id = $dzUpload.data('id'),
                type = $dzUpload.data('type'),
                images = self.milestones.imagesList[id],
                i = 0;
            if(typeof images !== "undefined")
                l = images.length;
            else
                l = 0;

            // imagesDzn = self.createDropzone();

            // Initially, clear the dropzone
            imagesDzn.removeAllFiles();

            $(this).find("button").html("UPLOAD");
            $dzUpload.empty();

            // Reset count of images in modal
            imagesDzn.count = 1;

            // Since we are using the maxFiles option, set initial value
            imagesDzn.options.maxFiles = 6;

            for (; i < l; i++) {
                // Create the mock file:
                var mockFile = {
                    name: "Filename",
                    size: 12345
                };

                // var thumbSize = self.getThumbnailSize(i + 1);

                var thumbSize = self.getThumbnailSizeNew(i + 1, l);

                // Update image preview template to set size
                imagesDzn.options.previewTemplate = "<div style='width: " + thumbSize.width + "px; display: inline-block;'>" + $dzTemplate + "</div>";

                // Call the default addedfile event handler
                imagesDzn.emit("addedfile", mockFile, true);

                // And optionally show the thumbnail of the file:
                imagesDzn.emit("thumbnail", mockFile, images[i]);


            }

            // Since we are using the maxFiles option, adjust value accordingly
            imagesDzn.options.maxFiles = imagesDzn.options.maxFiles - l;

            // Set count of images in modal
            imagesDzn.count = l + 1;
            imagesDzn.startImages = images;

            // self.fixThumbnailMargins(l + 1);

            // Reset preview template
            imagesDzn.options.previewTemplate = $dzTemplate;
        });

        $(document).on("blur", ".__c_milestoneForm", function() {
            self.updateMilestone(this);
        });

        $(document).on("blur", ".__c_momentForm", function() {
            self.updateMoment(this);
        });



        $(document).on("blur", ".__c_titleForm,.__c_descriptionForm", function() {
            self.updateTrip(this, self.displayCompleteTripData);
        });

        $(document).on("change", ".__c_roadConditionForm", function() {
            self.updateRoadCondition(this);
        });

        $(document).on("scroll", utilities.debounce(250, function() {
            self._highlightMilestone();
        }));

        // $(document).on("mouseover", "#milestonesContainer .row", function() {
        //     $(this).find(".popup-button-wrapper").show();
        // });

        // $(document).on("mouseout", "#milestonesContainer .row", function() {
        //     $(this).find(".popup-button-wrapper").hide();
        // });

        $(".__c_momentAtDrp").on("_change", function() {
            var value = $(this).find("input[name=moment_at]").val();

            // Toggle display of last milestone in list
            if (value == 0) {
                $(".__c_afterMilestoneDrp").find(".c-options-wrapper li:last-child").show();
            } else {
                $(".__c_afterMilestoneDrp").find(".c-options-wrapper li:last-child").hide();

            }
        });
    },

    sidebarmap: null,

    fullViewMap: null,

    sidebarMapMarkers: {},
    fullMapMarkers: {},
    milestoneMarker: null,

    mapbounds: null,

    fetchCategories: function(fetchCallback) {
        var self = this;

        $.ajax({
            type: "GET",
            url: Config.baseUrl + "/categories",
            async: true,
            headers: {
                "X-Auth-Token": utilities.cookies.getItem("auth_token")
            },
            success: function(data, textStatus, jqXHR) {
                if (fetchCallback) fetchCallback.apply(self, [data]);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log("unable to fetch list of categories " + errorThrown);
            }
        });
    },

    categoriesFetched: function(listOfCategories) {
        this.categories.catList = listOfCategories;
        this.categories.init();
    },

    // Categories & Subcategories
    categories: {
        init: function() {
            var self = this;

            $(this.subcat_bindedDropdowns).on("category_changed", function(ev, catID) {
                self.updateSubcatsInDropdown(catID);
            });
        },

        catList: [],

        updateSubcatsInDropdown: function(catID) {
            $(this.subcat_bindedDropdowns).html("");
            var subcats = [];
            for (var i = 0; i < this.catList.length; i++) {
                if (this.catList[i]['id'] == catID) {
                    subcats = this.catList[i]['sub_categories'];
                    break;
                }
            }
            utilities.fillDropDown3(this.subcat_bindedDropdowns, subcats, "name", "id");
            utilities.setFirstActive(this.subcat_bindedDropdowns);
        },

        subcat_bindedDropdowns: ".__c_subcats"
    },

    // All about milestones
    milestones: {
        list: {},

        listArray: [],

        positionList: {},

        imagesList: {},

        addToList: function(milestones) {
            // initially clear the array of milestones
            this.listArray = [];

            for (var i = 0; i < milestones.length; i++) {
                this.list[milestones[i].id] = milestones[i].location_name;
                this.listArray.push({
                    'id': milestones[i].id,
                    'name': milestones[i].location_name
                });
                if (!milestones[i].isLast) this.positionList[milestones[i].position] = milestones[i].location_name;

                // Save images of this milestone
                this.imagesList[milestones[i].id] = milestones[i].images;
            }

            this.updateListInDropdown();
            this.updatePositionListInDropdown();
            this.updateLists();
        },

        removeFromList: function(milestoneID) {
            delete this.list[milestoneID];

            for (var i = 0, l = this.listArray.length; i < l; i++) {
                if (this.listArray[i].id == milestoneID) {
                    this.listArray.splice(i, 1);
                    break;
                }
            }

            this.updateListInDropdown();
            this.updatePositionListInDropdown();
            this.updateLists();
        },

        updateListInDropdown: function() {
            $(this.bindedDropdowns).html("");
            utilities.fillDropDown4(this.bindedDropdowns, this.listArray, "name", "id");
            utilities.setFirstActive(this.bindedDropdowns);
        },

        updatePositionListInDropdown: function() {
            $(this.bindedPositionDropdowns).html("");
            utilities.fillDropDown3(this.bindedPositionDropdowns, this.positionList);
            utilities.setFirstActive(this.bindedPositionDropdowns);
        },

        updateLists: function() {
            var self = this,
                _onClick = function() {
                    $(self.bindedLists).find("li").removeClass("active");

                    // add class 'active' on li, when milestone name is clicked
                    $(this).addClass("active");
                };

            // cleanup
            $(self.bindedLists).html("");
            $(self.bindedLists).off("click");

            $(self.bindedLists).on("click, setActive", "li", _onClick);

            $.each(this.listArray, function(index, value) {
                var li = utilities.createElement("<li/>", null, {});
                utilities.createElement("<a/>", li, {
                    'href': "#__c_milestone" + value.id,
                    'html': value.name,
                    'data-milestone-id': value.id
                });

                $(self.bindedLists).append(li);
            });
        },

        bindedDropdowns: ".__c_milestones",

        bindedPositionDropdowns: ".__c_milestones_position",

        bindedLists: ".__c_milestonesList"
    },

    // All about moments
    moments: {
        categories: {
            1: {
                'id': 1,
                'name': "Stay",
                'defaultClass': "icon-pxl-moment-stay",
                'activeClass': "icon-pxl-moment-stay-active"
            },
            14: {
                'id': 14,
                'name': "Eat/Drink",
                'defaultClass': "icon-pxl-moment-food",
                'activeClass': "icon-pxl-moment-food-active"
            },
            26: {
                'id': 26,
                'name': "See",
                'defaultClass': "icon-pxl-moment-see",
                'activeClass': "icon-pxl-moment-see-active"
            },
            33: {
                'id': 33,
                'name': "Do",
                'defaultClass': "icon-pxl-moment-do",
                'activeClass': "icon-pxl-moment-do-active"
            },
            42: {
                'id': 42,
                'name': "Experience",
                'defaultClass': "icon-pxl-moment-experience",
                'activeClass': "icon-pxl-moment-experience-active"
            }
        },

        selectedCategory: null,

        initCategorySelection: function() {
            var self = this;

            $(".modal-categoryies input[type=radio]").on("click", function(ev) {
                ev.stopPropagation();

                var catID = $(this).val();

                // check if clicked category is already selected; One instruction saves a lot of computation
                if (self.selectedCategory == catID) return;

                self.resetCategorySelection();

                var $iconEl = $(ev.target).siblings(".icon-pxl");
                $iconEl.removeClass(self.categories[catID]['defaultClass']);
                $iconEl.addClass(self.categories[catID]['activeClass']);

                self.selectedCategory = catID;

                $(".__c_subcats").trigger("category_changed", [catID]);
            });
        },

        resetCategorySelection: function() {
            var self = this,
                radios = $(".modal-categoryies input[type=radio]"),
                catID = null;

            $.each(radios, function(index, el) {
                el = $(el);
                catID = el.val();
                el = el.siblings(".icon-pxl");
                el.removeClass(self.categories[catID]['activeClass'] + " " + self.categories[catID]['defaultClass']).addClass(self.categories[catID]['defaultClass']);
            });
        }
    },

    // Create a new milestone
    createMilestone: function(createCallback) {
        var self = this,
            postData = utilities.formToObject("#addMilestoneForm");

        // Lil' validation
        if (!self.milestoneMarker) {
            $("#milestone_location_error").show();
            return;
        } else $("#milestone_location_error").hide();

        postData["reached_on"] = utilities.getFormattedDate(postData["reached_on"]);

        $.ajax({
            type: "POST",
            url: Config.baseUrl + "/trips/" + this.tripID + "/milestones",
            async: true,
            headers: {
                "X-Auth-Token": utilities.cookies.getItem("auth_token")
            },
            data: {
                "milestone": postData
            },
            success: function(data, textStatus, jqXHR) {
                // close the modal
                $("#milestoneAddPopup").foundation('reveal', 'close');

                // Add a flag to indicate last milestone
                if (data && data.milestones) {
                    data.milestones[data.milestones.length - 1]['isLast'] = true;
                }

                if (createCallback) createCallback.apply(self, [data]);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log("unable to add milestone " + errorThrown);
            }
        });
    },

    // Update an existing milestone
    updateMilestone: function(milestoneForm, updateCallback) {
        var self = this,
            postData = utilities.formToObject(milestoneForm);

        //postData["reached_on"] = utilities.getFormattedDate(postData["reached_on"]);

        $.ajax({
            type: "PUT",
            url: Config.baseUrl + "/milestones/" + postData.id,
            async: true,
            headers: {
                "X-Auth-Token": utilities.cookies.getItem("auth_token")
            },
            data: {
                "milestone": postData
            },
            success: function(data, textStatus, jqXHR) {
                // Add a flag to indicate last milestone
                if (data && data.milestones) {
                    data.milestones[data.milestones.length - 1]['isLast'] = true;
                }
                if (updateCallback) updateCallback.apply(self, [data]);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log("unable to update milestone " + errorThrown);
            }
        });
    },

    updateRoadCondition: function(roadConditionForm, updateCallback) {
        var self = this,
            postData = utilities.formToObject(roadConditionForm),
            milestoneID = $(roadConditionForm).data("milestone-id");

        $.ajax({
            type: "PUT",
            url: Config.baseUrl + "/milestones/" + milestoneID,
            async: true,
            headers: {
                "X-Auth-Token": utilities.cookies.getItem("auth_token")
            },
            data: {
                'milestone': postData
            },
            success: function(data, textStatus, jqXHR) {
                if (updateCallback) updateCallback.apply(self, [data]);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log("unable to update milestone " + errorThrown);
            }
        });
    },

    _highlightMilestone: function() {
        // Get all milestone rows
        var $rows = $("#milestonesContainer > .row.no-margin");

        // Calculate top of each row
        var tops = [],
            top = undefined;

        for (var i = 0, l = $rows.length; i < l; i++) {
            top = $($rows[i]).offset().top - $(document).scrollTop();
            if (top < 0) top = Infinity;

            tops.push(top);
        }

        // Find out the minimum top
        // Math.min.apply( Math, tops )

        // Fetch element with least top
        var el = $rows[tops.indexOf(Math.min.apply(Math, tops))];

        if (!el) return;

        // Get milestone id
        var milestoneID = $(el).data("milestone-id");

        // Highlight appropriate milestone
        $("aside a[href=#__c_milestone" + milestoneID + "]").parent().trigger("setActive");
        this._doInactiveMarkers(this.sidebarMapMarkers);
        this.sidebarMapMarkers[milestoneID].toggleActive();
    },

    // Create a new moment
    createMoment: function(createCallback) {
        var self = this,
            postData = utilities.formToObject("#addMomentForm"),
            createMomentURL = "";

        // Lil' validations
        if (!postData.location_name) {
            $(".__c_location_error").show();
            return;
        } else $(".__c_location_error").hide();

        if (!postData.sub_category_id || postData.sub_category_id == "0") {
            $(".__c_category_error").show();
            return;
        } else $(".__c_category_error").hide();

        if (postData.moment_at == "1") {
            createMomentURL += "/trips/" + this.tripID + "/moments";
        } else if (postData.moment_at == "0") {
            createMomentURL += "/milestones/" + postData.after_milestone + "/moments";
            delete postData.after_milestone;
        }

        // delete moment category
        delete postData.category;

        // delete moment at, anyways
        delete postData.moment_at;

        $.ajax({
            type: "POST",
            url: Config.baseUrl + createMomentURL,
            async: true,
            headers: {
                "X-Auth-Token": utilities.cookies.getItem("auth_token")
            },
            data: {
                "moment": postData
            },
            success: function(data, textStatus, jqXHR) {
                // Increase moment count by 1
                $("#tripMoment").text(+$("#tripMoment").text() + 1);

                // close the modal
                $("#momentAddPopup").foundation('reveal', 'close');

                self.newMoment = data.id;

                if (createCallback) createCallback.apply(self, [data]);
                
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log("unable to add moment " + errorThrown);
            }
        });
    },

    newMoment: null,

    // Update an existing moment
    updateMoment: function(momentForm, updateCallback) {
        var self = this,
            postData = utilities.formToObject(momentForm);

        //postData["reached_on"] = utilities.getFormattedDate(postData["reached_on"]);

        $.ajax({
            type: "PUT",
            url: Config.baseUrl + "/moments/" + postData.id,
            async: true,
            headers: {
                "X-Auth-Token": utilities.cookies.getItem("auth_token")
            },
            data: {
                "moment": postData
            },
            success: function(data, textStatus, jqXHR) {
                if (updateCallback) updateCallback.apply(self, [data]);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log("unable to update moment " + errorThrown);
            }
        });
    },

    tripID: null,

    ciCropCoords: null,

    // Flag - true if trip is of type private
    isPrivate: false,

    // Fetch trip info from server
    getTripData: function(fetchCallback) {
        var self = this;

        $.ajax({
            type: "GET",
            url: Config.baseUrl + "/trips/" + this.tripID,
            async: true,
            headers: {
                "X-Auth-Token": utilities.cookies.getItem("auth_token")
            },
            success: function(data, textStatus, jqXHR) {
                // Add a flag to indicate last milestone
                if (data && data.milestones) {
                    data.milestones[data.milestones.length - 1]['isLast'] = true;
                }
                
                if(typeof(fetchCallback) == "function")
                    fetchCallback.apply(self, [data]);
                else
                    self.displayCompleteTripData(data);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log("unable to fetch trip details " + errorThrown);
            }
        });
    },

    updateTrip: function(tripForm, updateCallback) {
        var self = this,
            postData = utilities.formToObject(tripForm);

        //postData["reached_on"] = utilities.getFormattedDate(postData["reached_on"]);

        $.ajax({
            type: "PUT",
            url: Config.baseUrl + "/trips/" + this.tripID,
            async: true,
            headers: {
                "X-Auth-Token": utilities.cookies.getItem("auth_token")
            },
            data: {
                "trip": postData
            },
            success: function(data, textStatus, jqXHR) {
                // Add a flag to indicate last milestone
                if (data && data.milestones) {
                    data.milestones[data.milestones.length - 1]['isLast'] = true;
                }

                if (updateCallback) updateCallback.apply(self, [data]);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log("unable to update trip " + errorThrown);
            }
        });
    },

    displayCompleteTripData: function(tripDetails) {
        road_trip_view.setFeaturedImage(tripDetails.coverImg);
        $(".__c_tripTitle").text(tripDetails.title);
        $(".__c_tripDesc").text(tripDetails.description);

        while (('' + tripDetails.num_days).length < 2) tripDetails.num_days = "0" + tripDetails.num_days;
        while (('' + tripDetails.num_miles).length < 2) tripDetails.num_miles = "0" + tripDetails.num_miles;
        while (('' + tripDetails.num_milestones).length < 2) tripDetails.num_milestones = "0" + tripDetails.num_milestones;
        while (('' + tripDetails.num_moments).length < 2) tripDetails.num_moments = "0" + tripDetails.num_moments;

        $("#tripDays").html(tripDetails.num_days);
        $("#tripMiles").html((tripDetails.num_miles*1.6).toFixed());
        $("#tripMilestone").html(tripDetails.num_milestones);
        $("#tripMoment").html(tripDetails.num_moments);
        $("#transportModeIcon").addClass(common.transportModes[+tripDetails.transport_mode - 1]['iconClass']);

        //milestoneDatepicker
        var msDatepick = $('.milestoneDatepicker').fdatepicker({
            format: 'dd/mm/yyyy',
            startDate: utilities.getDisplayDate(tripDetails.start_date),
            endDate: utilities.getDisplayDate(tripDetails.end_date)
        });

        $('.milestoneDatepicker').fdatepicker('update', utilities.getDisplayDate(tripDetails.start_date))

        // Clear all milestones, initially
        $("#milestonesContainer").empty();

        // Display Milestones
        for (var i = 0; i < tripDetails.milestones.length; i++) {
            try {
                tripDetails.milestones[i]["day_of_month"] = this.getDayFromDate(tripDetails.milestones[i].reached_on);
            } catch (ex) {}

            try {
                tripDetails.milestones[i]["month"] = this.getMonthFromDate(tripDetails.milestones[i].reached_on);
            } catch (ex) {}
            showDeleteModal = true;
            if (i == 0 || i == tripDetails.milestones.length - 1) {
                showDeleteModal = false;
            }
            this.addMilestone(tripDetails.milestones[i],showDeleteModal);
        }
        this.milestones.addToList(tripDetails.milestones);

        // Display independent moments
        for (var i = 0; i < tripDetails.moments.length; i++) {
            tripDetails.moments[i]['milestoneID'] = tripDetails.moments[i]['after_milestone'];
            this.addMoment(tripDetails.moments[i]);
        } 
    },

    deleteImageMoment: function(img, updateCallback) {
        $.ajax({
            type: "DELETE",
            url: Config.baseUrl + "/moments/" + img + "/delete_image",
            async: true,
            headers: {
                "X-Auth-Token": utilities.cookies.getItem("auth_token")
            },
            success: function(data, textStatus, jqXHR) {
                if (updateCallback) updateCallback();
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log("unable to delete image " + errorThrown);
            }
        });
    },

    deleteImage: function(img, updateCallback) {
        $.ajax({
            type: "DELETE",
            url: Config.baseUrl + "/milestones/" + img + "/delete_image",
            async: true,
            headers: {
                "X-Auth-Token": utilities.cookies.getItem("auth_token")
            },
            success: function(data, textStatus, jqXHR) {
                if (updateCallback) updateCallback();
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log("unable to delete image " + errorThrown);
            }
        });
    },

    getDayFromDate: function(date) {
        if (date == null || date == "") return "";
        var m = date.match(/^(\d{4})-(\d{1,2})-(\d{1,2})$/);
        return m[3];
    },

    months: ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'],

    getMonthFromDate: function(date) {
        if (date == null || date == "") return "";
        var m = date.match(/^(\d{4})-(\d{1,2})-(\d{1,2})$/);
        return (this.months[+m[2] - 1]);
    },

    milestoneCount: 0,

    createSidebarMap: function() {
        var map = utilities.createGMap(document.getElementById("milestone-map"));

        this.createMaximizeControl(map);

        return map;
    },

    createFullScreenMap: function() {
        var map = utilities.createGMap(document.getElementById("fullViewMap"));

        return map;
    },

    createModalMap_1: function() {
        var map = utilities.createGMap(document.getElementById("modalMap-1"));
        $(document).on('opened', '[data-reveal]', function() {
            google.maps.event.trigger(map, "resize");
        });
        var momentStartAutocomplete = utilities.setAutocompleteSearch(map, document.getElementById("moment_start_location"));
        google.maps.event.addListener(momentStartAutocomplete, 'place_changed', function() {
            var place = momentStartAutocomplete.getPlace();
            if (!place.geometry) {
                return;
            }

            if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
            } else {
                map.setCenter(place.geometry.location);
                new google.maps.Marker({
                    map: map,
                    position: place.geometry.location
                });
                map.setZoom(17); // Why 17? Because it looks good.
            }
        });

        var momentEndAutocomplete = utilities.setAutocompleteSearch(map, document.getElementById("moment_end_location"));
        google.maps.event.addListener(momentEndAutocomplete, 'place_changed', function() {
            var place = momentEndAutocomplete.getPlace();
            if (!place.geometry) {
                return;
            }

            if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
            } else {
                map.setCenter(place.geometry.location);
                map.setZoom(17); // Why 17? Because it looks good.
            }
        });
    },

    createModalMap_2: function() {
        var self = this,
            map = utilities.createGMap(document.getElementById("modalMap-2"));
        $(document).on('opened', '[data-reveal]', function() {
            google.maps.event.trigger(map, "resize");
        });
        var autocomplete = utilities.setAutocompleteSearch(map, document.getElementById("location_name"));

        google.maps.event.addListener(autocomplete, 'place_changed', function() {
            var place = autocomplete.getPlace();
            if (!place.geometry) {
                return;
            }

            if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
            } else {
                map.setCenter(place.geometry.location);
                map.setZoom(17); // Why 17? Because it looks good.
            }

            self.milestoneMarker = new google.maps.Marker({
                map: map,
                position: place.geometry.location
            });
        });
    },

    createMaximizeControl: function(map) {
        var self = this;

        // Create a div to hold the control.
        var controlDiv = document.createElement('div');

        // Set CSS styles for the DIV containing the control
        // Setting padding to 45 px will offset the control
        // from the edge of the map.
        controlDiv.style.padding = '10px';

        var maximizeHelper = this.createMaximizeHelper();
        google.maps.event.addDomListener(maximizeHelper, 'click', function() {
            $('#fullMapModal').foundation('reveal', 'open');
        });
        controlDiv.appendChild(maximizeHelper);

        // We don't really need to set an index value here, but
        // this would be how you do it. Note that we set this
        // value as a property of the DIV itself.
        controlDiv.index = 1;

        // Add the control to the map at a designated control position
        // by pushing it on the position's array. This code will
        // implicitly add the control to the DOM, through the Map
        // object. You should not attach the control manually.
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(controlDiv);
    },

    createMaximizeHelper: function() {
        // Set CSS for the control border.
        var controlUI = document.createElement('div');
        controlUI.title = 'Full Screen';
        controlUI.className = "fullScreenControl";

        // Set CSS for the control interior.
        var controlText = document.createElement('img');
        controlText.src = "../images/map-zoom.png";
        controlText.style.padding = "3px";
        controlUI.appendChild(controlText);

        return controlUI;
    },

    createDropzone: function() {
        var self = this,
            $uploadBtn = $("#uploadImageMoment button");

        $dzTemplate = '<div class="dz-preview dz-file-preview">\
          <div class="dz-details">\
            <img data-dz-thumbnail />\
          </div>\
          <div class="dz-progress" style="height:1px;background-color:black;"><span style="height:1px;background-color:red;" class="dz-upload" data-dz-uploadprogress></span></div>\
          <div class="dz-success-mark"><span>✔</span></div>\
          <div class="dz-error-mark"><span>×</span></div>\
          <div class="dz-error-message"><span data-dz-errormessage></span></div>\
          <a href="#" id="featured" class="dz-make-featured"><span class="icon-pxl icon-pxl-suggested"></span></a>\
          <a href="#" class="dz-remove" data-dz-remove><span class="icon-pxl icon-pxl-remove-cross-white"></span></a>\
        </div>';

        var dzn = new Dropzone("#dzUpload", {
            // Url is set when file is added to dropzone
            url: Config.baseUrl + "",
            paramName: "attachment",
            maxFilesize: 64, // MB
            addRemoveLinks: false,
            parallelUploads: 6,
            thumbnailWidth: 400,
            clickable: true,
            // previewsContainer: '.dropzone',
            maxFiles: 6,
            autoProcessQueue: false,
            previewTemplate: $dzTemplate,
            uploadMultiple: false,
            headers: {
                "X-Auth-Token": utilities.cookies.getItem("auth_token")
            }
            // dictDefaultMessage: "<span>hello world</span>"
        });



        // remove the exceeded files 
        dzn.on("maxfilesexceeded", function(file) {
            // $('#maxFilesModal').foundation('reveal', 'open');
            $("#max_message").show();
            $("#max_message").fadeOut('slow', function() {
                $("#max_message").hide();
                return false;
            });
            // this.removeFile(file);
        });

        dzn.count = 1;
        dzn.startImages = [];

        var width = 300,
            height = 300,
            data = [],
            dataURIs = [];

        var redraw = function() {
            for (var i = 0; i < data.length; i++) {
                // Remove all previews
                // dzn.removePreview(data[i]);
                // $("#dzUpload").empty();
            }

            // Draw all files again
            for (var i = 0; i < data.length; i++) {
                var thumbSize = self.getThumbnailSizeNew(i + 1, data.length + 1);
                
                dzn.options.thumbnailWidth = thumbSize.width;
                dzn.options.thumbnailHeight = thumbSize.height;
                dzn.emit("thumbnail", data[i], dataURIs[i]);
            }
        };

        var start = 0;

        dzn.on("addedfile", function(file, isMock) {
            // Set URL
            // redraw();
            this.options.url = Config.baseUrl + "/" + $("#dzUpload").data('type') + "/" + $("#dzUpload").data('id') + "/upload_image";

            // Set state of upload button
            $uploadBtn.removeClass("inactive");
            $("#dzUpload").find(".section").remove();

            if(!dzn.startImages) {
                dzn.startImages = [];
            }

            for (var i = 0; i < dzn.startImages.length; i++) {
                var mockFile = {
                    name: "Filename",
                    size: 12345
                };

                // var thumbSize = self.getThumbnailSize(i + 1);

                var thumbSize = self.getThumbnailSizeNew(i + 1, start + 1);

                dzn.options.thumbnailWidth = thumbSize.width;
                dzn.options.thumbnailHeight = thumbSize.height;
                // dzn.options.resize(data[i]);
                //var url = dzn.createThumbnailWithDim(mockFile,thumbSize.width,thumbSize.height);

                var mockUrl = dzn.startImages[i];

                $("<img/>").attr("src", mockUrl).load(function(){
                    s = {w:this.width, h:this.height};

                    var canvas = document.getElementById('myCanvas');
                    var context = canvas.getContext('2d');
                    var imageObj = new Image();
                    imageObj.onload = function() {
                    // draw cropped image
                        var sourceX = 0;
                        var sourceY = 0;
                        var sourceWidth = s.w;
                        var sourceHeight = s.h;
                        var destWidth = thumbSize.width;
                        var destHeight = thumbSize.height;
                        var destX = 0;
                        var destY = 0;

                        context.drawImage(imageObj, sourceX, sourceY, sourceWidth, sourceHeight, destX, destY, destWidth, destHeight);
                        var dataUrl = canvas.toDataURL();

                        dzn.emit("thumbnail", mockFile, dataUrl);
                    };
                    imageObj.src = mockUrl;     
                });
            }

            for (var i = 0; i < data.length; i++) {
                var thumbSize = self.getThumbnailSizeNew(i + 1, data.length + 1);

                
                dzn.options.thumbnailWidth = thumbSize.width;
                dzn.options.thumbnailHeight = thumbSize.height;
                // dzn.options.resize(data[i]);
                var url = dzn.createThumbnailWithDim(data[i],thumbSize.width,thumbSize.height);
                dzn.emit("thumbnail", data[i], url);
            }

            $("#photoUploaderInfo").removeClass('hidden');
            if (!isMock) {
                data.push(file);
            } else {
                start++;
            }

            

            var thumbSize = self.getThumbnailSizeNew(dzn.count, dzn.count);

            //dzn.options.previewTemplate = "<div style='width: " + width + "px; height: " + height + "px; display: inline-block;'>" + $dzTemplate + "</div>";

            dzn.options.thumbnailWidth = thumbSize.width;
            dzn.options.thumbnailHeight = thumbSize.height;

            
            
            dzn.count++;
            
        });

        dzn.on("thumbnail", function(file, dataURI) {
            for (var i = 0; i < data.length; i++) {
                if (data[i] == file) {
                    dataURIs[i] = dataURI;
                    break;
                }
            }

            $(".dz-wrapper-message-section").css('bottom','0');
            $("#dzUpload").css('padding-bottom','118px');
            // $("#dzUpload").append('<div class="section" style="z-index:-1">' +
            //     '<div class="photo-uploader">' +
            //         '<span class="icon-pxl icon-pxl-photo vam ghost-element dib"></span>' +
            //         '<p>Add upto 6 images, file size should be under 2 MB</p>' +
            //     '</div>' +
            // '</div>');

            self.fixThumbnailMargins(data.length);
        });

        dzn.on("removedfile", function(file) {
            var l = data.length,
                i = 0;

            // Set state of upload button
            if (l === 0) $uploadBtn.removeClass("inactive");
            $("#dzUpload").find(".section").remove();

            // Remove the file from array
            for (; i < l; i++) {
                if (data[i] == file) {
                    data.splice(i, 1);
                    break;
                }
            }

            i = 0;

            l = data.length;

            // Reset count of images in modal
            dzn.count = 1;

            // Since we are using the maxFiles option, set initial value
            dzn.options.maxFiles = 6;

            for (; i < l; i++) {
                // var thumbSize = self.getThumbnailSize(i + 1);

                // Update image preview template to set size
                // dzn.options.previewTemplate = "<div style='width: " + thumbSize.width + "px; display: inline-block;'>" + $dzTemplate + "</div>";

                var thumbSize = self.getThumbnailSizeNew(i + 1, data.length);

                
                dzn.options.thumbnailWidth = thumbSize.width;
                dzn.options.thumbnailHeight = thumbSize.height;
                // dzn.options.resize(data[i]);
                var url = dzn.createThumbnailWithDim(data[i],thumbSize.width,thumbSize.height);
                dzn.emit("thumbnail", data[i], url);

                // Call the default addedfile event handler
                // dzn.emit("addedfile", data[i], true);

                // And optionally show the thumbnail of the file:
                // dzn.emit("thumbnail", data[i]);
            }

            // self.fixThumbnailMargins(data.length);

            // redraw();

            // Since we are using the maxFiles option, adjust value accordingly
            dzn.options.maxFiles = dzn.options.maxFiles - l;

            // Set count of images in modal
            dzn.count = l + 1;
        });

        dzn.on("sending", function(file, data) {
            $("#uploadImageMoment").find("button").html("UPLOADING...");
        });

        // Update the total progress bar
        dzn.on("totaluploadprogress", function(progress) {
            $("#uploadImageMoment").find("button").html("UPLOADING... " + Math.round(progress) + "%");
        });

        dzn.on("complete", function(file, data) {
            // Close the modal
            $("#uploadImageMoment").find("button").html("UPLOADED");
            $('#uploadImageMoment').foundation('reveal', 'close');
            $("#uploadImageMoment").find("button").html("UPLOAD");

            // Refil Trip data
            // self.displayCompleteTripData(data);
            create_roadtrip.getTripData(create_roadtrip.displayCompleteTripData);
        });

        $uploadBtn.on("click", function() {
            dzn.processQueue();
        });

        return dzn;
    },

    fixThumbnailMargins: function( length) {
        $(".dropzone-1 > div").css("margin-right", "0");
        $(".dropzone-1 > div").css("margin-bottom", "0.3125rem");
        if (length == 3) {
            $(".dropzone-1 > div:nth-child(2)").css("margin-right", "0.3125rem");
        } else if (length == 4) {
            $(".dropzone-1 > div:nth-child(2)").css("margin-right", "0.3125rem");
            $(".dropzone-1 > div:nth-child(3)").css("margin-right", "0.3125rem");
        } else if (length == 5) {
            $(".dropzone-1 > div:nth-child(2)").css("margin-right", "0.3125rem");
            $(".dropzone-1 > div:nth-child(4)").css("margin-right", "0.3125rem");
        } else if (length == 6) {
            $(".dropzone-1 > div:nth-child(2)").css("margin-right", "0.3125rem");
            $(".dropzone-1 > div:nth-child(4)").css("margin-right", "0.3125rem");
            $(".dropzone-1 > div:nth-child(5)").css("margin-right", "0.3125rem");
        }
    },

    getThumbnailSize: function(index) {
        var width, height;

        switch (index) {
            case 1:
                width = 579;
                height = 339;
                break;

            case 2:
            case 3:
                width = 286;
                height = 220;
                break;

            default:
                width = 188;
                height = 145;
                break;
        }

        return {
            'width': width,
            'height': height
        };
    },

    

    getThumbnailSizeNew: function(index, l) {
        var width, height;
        if (index == 1) {
            width = 579;
            height = 339;
        } else {
            if (l == 2) {
                width = 579;
                height = 339;
            } else if (l == 3 || l == 5) {
                width = 286;
                height = 220;
            } else if (l == 4) {
                width = 188;
                height = 145;
            } else if (l == 6) {
                switch (index) {
                    case 2:
                    case 3:
                        width = 286;
                        height = 220;
                        break;

                    default:
                        width = 188;
                        height = 145;
                        break;
                }
            }
        }

        return {
            'width': width,
            'height': height
        };
    },

    addMilestone: function(milestoneDetails, showDeleteModal) {
        var $milestonesContainer = $("#milestonesContainer");
        var theTemplateScript = $("#milestone_template").html();
        var theTemplate = Handlebars.compile(theTemplateScript);

        $milestonesContainer.append(theTemplate(milestoneDetails));

        if (!showDeleteModal) {
            $("#__c_milestone" + milestoneDetails.id).find(".del-milestone").attr("data-reveal-id","").addClass("milestone-inactive");
        }
/*        $('.milestone-inactive').tooltipster({
                position: 'bottom',
                interactive: true,
                content: $('<span>First and last<br> milestones cannot be<br> deleted.</span>'),
                functionReady: function () {
                    $(window).trigger("resize");
            }
        });

        $(window).resize(function () {
            $('.milestone-inactive').tooltipster('reposition');
        });
        $(window).trigger("resize");*/

        // Markers in sidebar
        var marker = road_trip_view.createMarker({
            'latlng': new google.maps.LatLng(milestoneDetails.latitude, milestoneDetails.longitude),
            'index': milestoneDetails.position
        });
        marker.setMap(this.sidebarmap);
        this.sidebarMapMarkers[milestoneDetails.id] = marker;
        this.fitBounds(this.sidebarmap, this.sidebarMapMarkers);

        // Markers in full map view
        marker = road_trip_view.createMarker({
            'latlng': new google.maps.LatLng(milestoneDetails.latitude, milestoneDetails.longitude),
            'index': milestoneDetails.position
        });
        marker.setMap(this.fullViewMap);
        this.fullMapMarkers[milestoneDetails.id] = marker;

        utilities.fillDropDown(".__c_road_condition" + milestoneDetails.id, common.roadConditions, 'name', 'value');
        $(".__c_road_condition" + milestoneDetails.id).val(milestoneDetails.road_condition);

        // Add moments of this milestone
        for (var i = 0; i < milestoneDetails.moments.length; i++) {
            milestoneDetails.moments[i]['milestoneID'] = milestoneDetails.id;
            this.addMoment(milestoneDetails.moments[i]);
        }
    },

    fitBounds: function(map, markers) {
        var self = this;

        $.each(markers, function(key, value) {
            self.mapbounds.extend(value.getPosition());
        });
        map.fitBounds(this.mapbounds);
    },

    _doInactiveMarkers: function(markers) {
        $.each(markers, function(key, value) {
            value.setInactive();
        });
    },

    addMoment: function(momentDetails) {
        momentDetails.milestoneID = momentDetails.after_milestone || momentDetails.at_milestone;
        var momentsContainer = $("#momentsContainer" + momentDetails.milestoneID);
        var theTemplateScript = $("#moment_template").html();
        var theTemplate = Handlebars.compile(theTemplateScript);

        momentsContainer.append(theTemplate(momentDetails));
        if( this.newMoment !== null ) {
            var momentsContainer = $("#newtitlemoment" + this.newMoment );
            momentsContainer.find(".place-moment-heading-editor").focus();
            this.newMoment = null;
        }   
    },

    // Set the status of current roadtrip to Published
    publishTrip: function(publishCallback) {
        var self = this,
            $postdata = utilities.formToObject("#publishTripForm");

        delete $postdata.twitter_share;
        delete $postdata.fb_share;
        delete $postdata.fb_share;
        delete $postdata.gplus_share;
        delete $postdata.tags;

        this.isPrivate = $postdata.is_private;
        $postdata.is_published = true;

        $.ajax({
            type: "POST",
            async: true,
            data: {
                "trip": $postdata
            },
            headers: {
                "X-Auth-Token": utilities.cookies.getItem("auth_token")
            },
            url: Config.baseUrl + "/trips/" + this.tripID + "/publish_trip",
            success: function(data, textStatus, jqXHR) {
                if (publishCallback) publishCallback.apply(self, [data]);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log("unable to publish trip " + errorThrown);
            }
        });
    },

    onTripPublished: function(tripDetails) {
        if (this.isPrivate) {
            $('#privateLinkPopup').foundation('reveal', 'open');
        } else { // Redirect to User's profile page if not a private trip
            window.location = "road_trip_view.html?id=" + this.tripID;
        }
    },

    // Mark current trip as Draft
    saveTripDraft: function() {
        $.ajax({
            type: "POST",
            async: true,
            headers: {
                "X-Auth-Token": utilities.cookies.getItem("auth_token")
            },
            url: Config.baseUrl + "/trips/" + this.tripID + "/save_as_draft",
            success: function(data, textStatus, jqXHR) {
                // Redirect to User's profile page, once the trip is published
                window.location = "profile_view.html";
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log("unable to save trip as draft " + errorThrown);
            }
        });
    },

    // Discard the current trip
    cancelTrip: function() {
        $.ajax({
            type: "DELETE",
            async: true,
            headers: {
                "X-Auth-Token": utilities.cookies.getItem("auth_token")
            },
            url: Config.baseUrl + "/trips/" + this.tripID,
            success: function(data, textStatus, jqXHR) {
                // Redirect to User's profile page, once the trip is published
                window.location = "feed.html";
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log("unable to cancel trip " + errorThrown);
            }
        });
    }
};

var common = {
    initialize: function() {
        var self = this;

        // These methods are not required on public pages
        if (!Config.isPublicPage) {
            this.setSignout();
            this.notifications_dropdown.initialize();
            this.confirms();
            this.trip();

            // create new roadtrip modal
            var roadtripModal = this.createRoadtripModal();

            $(".modals").append(roadtripModal);
            $('#createNewRoadtripModal').on('opened', function() {
                var modal = $(this);

                // Clear form
                $(this).find("form input").val("");

                self.createModalMap();

                var startDatePick = $('.startDate').fdatepicker({}).on('changeDate', function(ev) {
/*                    if (ev.date.valueOf() > endDatePick.date.valueOf()) {
                        var newDate = new Date(ev.date)
                        newDate.setDate(newDate.getDate());
                        endDatePick.update(newDate);
                    }*/
                    endDatePick.update(ev.date);
                    startDatePick.hide();
                    $('.endDate')[0].focus();
                }).data('datepicker');

                var endDatePick = $('.endDate').fdatepicker({
                    onRender: function(date) {
                        return date.valueOf() < startDatePick.date.valueOf() ? 'disabled' : '';
                    }
                }).on('changeDate', function(ev) {
                    endDatePick.hide();
                }).data('datepicker');

                $("#showNumberOfDays").on("click", function(ev) {
                    ev.preventDefault();
                    $("#dateRow").hide();
                    $("#numberOfDaysRow").show();
                    $('.endDate').val("");
                    $('.startDate').val("");
                    $(this).hide();
                    $("#showSelectDates").show();
                    return false;
                });

                $("#showSelectDates").on("click", function(ev) {
                    ev.preventDefault();
                    $("#numberOfDaysRow").hide();
                    $("#dateRow").show();
                    $('.duration').val("");
                    $(this).hide();
                    $("#showNumberOfDays").show();
                    return false;

                });

                $(".reveal-modal-bg").one("click", function() {
                    $('#createNewRoadtripModal').foundation('reveal', 'close');
                });
            });

            $("#createNewRoadtripModal").foundation();
        }

        this.g_search();
        this.site_header();
        this.initCustomDropdowns();
    },

    setSignout: function() {
        $(".__g_logout").on("click", function(ev) {
            ev.preventDefault();
            utilities.local_storage.clear();


            

            $.ajax({
                type: "DELETE",
                async: true,
                headers: {
                    "X-Auth-Token": utilities.cookies.getItem("auth_token")
                },
                url: Config.baseUrl + "/sessions",
                success: function(data, textStatus, jqXHR) {
                    // remove auth cookie
                    utilities.cookies.removeItem("auth_token", "/");
                    // redirect somewhere
                    window.location = "../index.html";
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log("unable to sign out " + errorThrown);
                }
            });
        });
    },

    // Fetch profile data of current user
    fetchProfileData: function(fetchCallback) {
        var self = this;

        $.ajax({
            type: "GET",
            async: true,
            url: Config.baseUrl + "/users/" + utilities.local_storage.get("id"),
            headers: {
                "X-Auth-Token": utilities.cookies.getItem("auth_token")
            },
            success: function(data, textStatus, jqXHR) {
                if (fetchCallback) fetchCallback.apply(self, [data]);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log("unable to fetch profile data " + errorThrown);
            }
        });
    },

    // Fill up profile data of currently logged in user
    fillProfileData: function(profileData) {
        if (!profileData) profileData = {};
        if (profileData.userPic) {
            $(".__c_userPhoto").css("background-image", "url(" + profileData.userPic + ")");  
        }else{
            $(".__c_userPhoto").css("background-image", "url(../images/default_profile.png)"); 
        }

        $(".__c_useremail").text(profileData.email);

        if (utilities.local_storage.get('userPic')) {
            $('.account-img').css("background-image", "url(" + utilities.local_storage.get('userPic') + ")"); 
        }else{
            $('.account-img').css("background-image", "url(../images/default_profile.png)");
        }


        if (profileData.name) {
            $(".__c_userName").text(profileData.name);
        }
        else
        {
            $(".__c_userName").text(profileData.username);
        }
        if (profileData.bio)
        {
            $(".__c_userBio").text(profileData.bio || utilities.local_storage.get('bio') || "");          
        }
        if (profileData.weburl){
            if (profileData.weburl  && profileData.weburl.indexOf("http") == -1) {
                profileData.weburl = "http://" + profileData.weburl;
            } else if (utilities.local_storage.get('weburl')  && utilities.local_storage.get('weburl').indexOf("http") == -1) {
                profileData.weburl = "http://" + utilities.local_storage.get('weburl');
            }
            $(".__c_weburl").attr("href", profileData.weburl || utilities.local_storage.get('weburl') || "#");
        }
        else
        {
            $(".__c_weburl").attr("href","#");
        }

        var publishedTripsCount = profileData.published_trips || "0";
        if (+publishedTripsCount === 1) publishedTripsCount = publishedTripsCount + " ROADTRIP";
        else publishedTripsCount = publishedTripsCount + " ROADTRIPS";        
        $(".__c_publishedTripsCount").html(publishedTripsCount);
                
        $(".__c_savedDraftsCount").text(profileData.saved_drafts || utilities.local_storage.get('saved_drafts') || "");

        var miles = (profileData.distance_covered * 1.6 ).toFixed()|| "0";
        if (+miles === 1) miles = "<strong>" + miles + "</strong> km";
        else miles = "<strong>" + miles + "</strong> km";
        $(".__c_totalMiles").html(miles);

        var followers_cnt = profileData.followers || "0";
        if (+followers_cnt === 1) followers_cnt = "<strong>" + followers_cnt + "</strong> follower.";
        else followers_cnt = "<strong>" + followers_cnt + "</strong> Followers.";        
        $(".__c_followersCount").html(followers_cnt);

        var roadtrippersCount = profileData.following || "0";
        if (+roadtrippersCount === 1) $(".__c_roadtrippersTxt").text("roadtripper");
        else roadtrippersCount = $(".__c_roadtrippersTxt").text("roadtrippers");

        $(".__c_followingPeopleCount").text(profileData.following || "0");
        $(".__c_followingPlacesCount").text(profileData.following_locations || "0");

        var placesCount = profileData.following_locations || "0";
        if (+placesCount === 1) placesCount = placesCount + " PLACE";
        else placesCount = placesCount + " PLACES";
        $(".__c_followingPlacesCountTxt").text(placesCount);

        $(".__c_followingPeopleCount").text(profileData.following || "0");

        var authentications = profileData.authentications || utilities.local_storage.get('authentications') || [],
            l = authentications.length;

        if (l > 0) {
            for (var i = 0; i < l; i++) {
                switch (authentications[i].provider) {
                    case "Facebook":
                        if (authentications[i].is_visible) $(".__c_fb").attr("href", authentications[i].link);
                        else $(".__c_fb").hide();
                        break;

                    case "twitter":
                        if (authentications[i].is_visible) $(".__c_tw").attr("href", authentications[i].link);
                        else $(".__c_tw").hide();
                        break;

                    case "google-oauth2":
                        if (authentications[i].is_visible) $(".__c_gp").attr("href", authentications[i].link);
                        else $(".__c_gp").hide();
                        break;
                }
            }
        } else {
            $(".__c_fb,.__c_tw,.__c_gp").hide();
        }
    },

    // reduce header height on scroll
    site_header: function() {
        var self = this;

        if (utilities.cookies.hasItem("auth_token")) {
            $(".__c_unsigned_header").hide();
            $(".__c_signed_header").show();
        } else {
            $(".__c_unsigned_header").show();
            $(".__c_signed_header").hide();
        }

        $(document).scroll(Foundation.utils.throttle(function(ev) {
            var $aside = $("aside"),
                _bottomScrollCondition = true;

            if ($(document).scrollTop() > 100) {
                //$(".top-bar-container.sticky").addClass("top-bar-small-height");
                $(".top-bar-container:not(.landing-navbar,.static-pages-navbar) .top-bar .name").addClass("top-bar-small-height");
                $(".top-bar-container:not(.landing-navbar,.static-pages-navbar) .top-bar").addClass("top-bar-small-height top-bar-small-lheight");
                $(".top-bar-container:not(.landing-navbar,.static-pages-navbar) .top-bar .top-bar-section > ul > li > a, .top-bar-container.sticky  ul.title-area > li > h1 > a").addClass("top-bar-small-lheight");
                $(".top-bar-container:not(.landing-navbar,.static-pages-navbar) .top-bar .top-bar-section ul .account-img").addClass("top-bar-small-margin");
                $(".landing-navbar:not(.__c_unsigned_header)").addClass("scrolled");
                self.searchOverlay.css("top", "50px");

                if ($(".trip-footer-wrapper").length > 0) {
                    _bottomScrollCondition = ($(".trip-footer-wrapper").offset().top - $(document).scrollTop() >= $aside.data("scroll-upto"));
                } else {
                    _bottomScrollCondition = false;
                }

                if ($aside.length && $(window).scrollTop() >= $aside.data("scroll-top") && _bottomScrollCondition && ($aside.height() < $(window).height() - 100)) $aside.css({
                    'position': "fixed",
                    'top': "65px"
                });
                else $aside.length && $aside.css("position", "static");
            } else {
                $(".top-bar-container:not(.landing-navbar,.static-pages-navbar) .top-bar .name").removeClass("top-bar-small-height");
                $(".top-bar-container:not(.landing-navbar,.static-pages-navbar) .top-bar").removeClass("top-bar-small-height top-bar-small-lheight");
                $(".top-bar-container:not(.landing-navbar,.static-pages-navbar) .top-bar .top-bar-section > ul > li > a, .top-bar-container.sticky ul.title-area > li > h1 > a").removeClass("top-bar-small-lheight");
                $(".top-bar-container:not(.landing-navbar,.static-pages-navbar) .top-bar .top-bar-section ul .account-img").removeClass("top-bar-small-margin");
                $(".landing-navbar:not(.__c_unsigned_header)").removeClass("scrolled");
                self.searchOverlay.css("top", "75px");

                $aside.length && $aside.css("position", "static");
            }
        }, 200));
    },

    // Initialize notifications dropdown
    notifications_dropdown: {
        initialize: function() {
            var self = this;
            var $notificationDropdown = $(".notification-drop");

            this.fetch(this.print);

            $(".__c_showNotifications").on("click", function(ev) {
                ev.preventDefault();
                ev.stopPropagation();

                $notificationDropdown.toggle();

                if(!$notificationDropdown.is(":visible")) {
                    self.deleteNotifications(common.notifications_dropdown.fetch);
                }
            });

            $notificationDropdown.find(".fr").on("click", function(ev) {
                ev.stopPropagation();
                self.deleteNotifications(function() {
                    common.notifications_dropdown.fetch(common.notifications_dropdown.print);
                });
            });

            $notificationDropdown.on("click", function(ev) {
                ev.stopPropagation();
            });

            $(window).on("click", function(ev) {
                $notificationDropdown.hide();
                // $(".__c_showNotifications").text("0").hide();
            });
        },

        // Create a notification row
        create: function(notificationDetails) {
            var $row = utilities.createElement("<li/>", null, {
                'class': "notif"
            });

            if (notificationDetails.action === "followed you on Roadmojo.") {
                var $a_wrap = utilities.createElement("<a/>", $row, {
                    'href': 'profile_view.html?_loc=otherProfile&id=' + notificationDetails.actor_id
                });
            }else{
                var $a_wrap = utilities.createElement("<a/>", $row, {
                    'href': "road_trip_view.html?id=" + notificationDetails.subject_id
                });
            }
            if (notificationDetails.actor_img_url) {
                utilities.createElement("<i/>", null, {
                    'class': "account-img-actor",
                    'style': "background-image: url(" + notificationDetails.actor_img_url + ")"
                }).appendTo(utilities.createElement("<div/>", $a_wrap, {
                    'class': "img-box"
                }));
            }
            else{
                utilities.createElement("<i/>", null, {
                    'class': "account-img-actor",
                    'style': "background-image: url(../images/default_profile.png)"
                }).appendTo(utilities.createElement("<div/>", $a_wrap, {
                    'class': "img-box"
                }));
            }
            utilities.createElement("<p/>", null, {
                'class': "summry",
                'html': '<strong>' + notificationDetails.actor + '</strong> ' + notificationDetails.action + ' <strong>' + notificationDetails.subject + '</strong><br><span class="time"> ' + notificationDetails.created_at + '</span>'
            }).appendTo(utilities.createElement("<div/>", $a_wrap, {
                'class': "content-box"
            }));

            return $row;
        },

        // Fetch list of notifications
        fetch: function(fetchCallback) {
            var self = this;

            $.ajax({
                type: "GET",
                async: true,
                headers: {
                    "X-Auth-Token": utilities.cookies.getItem("auth_token")
                },
                url: Config.baseUrl + "/users/notifications",
                success: function(data, textStatus, jqXHR) {
                    // if (fetchCallback) fetchCallback.apply(self, [data]);
                    // else common.notifications_dropdown.print(data);
                    common.notifications_dropdown.print(data);
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log("Unable to fetch notifications. " + errorThrown);
                }
            });
        },

        // Fetch list of notifications
        deleteNotifications: function(fetchCallback) {
            var self = this;

            $.ajax({
                type: "PUT",
                async: true,
                data: {'notification_ids[]': self.notifications_list},
                headers: {
                    "X-Auth-Token": utilities.cookies.getItem("auth_token")
                },
                url: Config.baseUrl + "/users/mark_as_read_notifications",
                success: function(data, textStatus, jqXHR) {
                    if (fetchCallback) fetchCallback.apply(self, [data]);
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log("Unable to fetch notifications. " + errorThrown);
                }
            });
        },

        // Fetch list of notifications
        reset: function(fetchCallback) {
            var self = this;

            $.ajax({
                type: "PUT",
                async: true,
                data: {'notification_ids[]': self.notifications_list},
                headers: {
                    "X-Auth-Token": utilities.cookies.getItem("auth_token")
                },
                url: Config.baseUrl + "/users/mark_as_unread_notifications",
                success: function(data, textStatus, jqXHR) {
                    if (fetchCallback) fetchCallback.apply(self, [data]);
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log("Unable to fetch notifications. " + errorThrown);
                }
            });
        },

        notifications_list: [],

        // Display Notifications
        print: function(notificationsList) {
            this.notifications_list = [];
            notificationsList = notificationsList.notifications.reverse();
// 
            var i = 5,
                l = notificationsList.length,
                $container = $(".__c_activityNotifs");

            // Display Notifications count
            if (l > 0) $(".__c_showNotifications").show().text(l);
            else $(".__c_showNotifications").hide().text("0");

            for (; i >= 0 ; i--) {
                if (!notificationsList[i]) continue;
                if (notificationsList[i].subject_id !== null && notificationsList[i].actor !== null
                    && notificationsList[i].subject !== null ){
                    $container.prepend(this.create(notificationsList[i]));
                    this.notifications_list.push(notificationsList[i].id);
                }
            }
        }
    },

    // Initialize custom created dropdowns
    initCustomDropdowns: function() {
        $(window).on("click", function() {
            var $wrapper = $(".c-select-wrapper");

            $wrapper.find(".c-options-wrapper").slideUp();
            $wrapper.find(".selected span:last-child").removeClass("inverted");
        });

        this.bindCustomDropdown(".c-select-wrapper");
    },

    bindCustomDropdown: function($selector) {
        $($selector).on("click", function(ev) {
            ev.stopPropagation();

            var $wrapper = $(this);

            if (ev.target.nodeName == "LI") {
                var $targetLi = $(ev.target);
                $targetLi.parent().find("li").removeClass("active");

                $wrapper.find(".selected span:first-child").text($targetLi.text());
                $wrapper.find(".selected input[type='hidden']").val($targetLi.data('value'));
                $targetLi.addClass("active");

                // Trigger custom event for dropdown value change
                $(this).trigger("_change");
            }

            $wrapper.find(".c-options-wrapper").slideToggle();
            $wrapper.find(".selected span:last-child").toggleClass("inverted");
        });
    },

    // confirm & delete boxes
    confirms: function() {
        var self = this;

        // default configuration of confirmation modal
        $(".confirm").foundation("reveal", {
            animation: 'fadeAndFade',
            close_on_background_click: false
        });

        // add "CANCEL" handler to confirmation modal
        $(".cancel-confirm").on("click", function() {
            $(".confirm").foundation('reveal', 'close');
        });

        // add "DELETE" handler to confirmation modal
        $(".delete-confirm").on("click", function(ev, tripDetails) {
            // Send AJAX request to delete a trip
            $.ajax({
                type: "DELETE",
                async: true,
                headers: {
                    "X-Auth-Token": utilities.cookies.getItem("auth_token")
                },
                url: Config.baseUrl + "/trips/" + self._confirmsDelete.details.id,
                success: function(data, textStatus, jqXHR) {
                    /* If request was sucessful: */
                    // check & execute callback
                    self._confirmsDelete.callback && self._confirmsDelete.callback();
                    // Finally, close modal
                    $(".confirm").foundation('reveal', 'close');
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log("Unable to delete. Trip " + errorThrown);
                }
            });
        });

        // add "DELETE" handler to confirmation modal
        $(".delete-confirm-image").on("click", function(ev) {
            // Send AJAX request to delete a trip
            $.ajax({
                type: "DELETE",
                async: true,
                headers: {
                    "X-Auth-Token": utilities.cookies.getItem("auth_token")
                },
                url: Config.baseUrl + "/" + self._confirmsDeleteType + "/" + self._confirmsDelete + "/delete_image",
                success: function(data, textStatus, jqXHR) {
                    /* If request was sucessful: */
                    // check & execute callback
                    self._confirmsDelete.callback && self._confirmsDelete.callback();
                    create_roadtrip.getTripData(create_roadtrip.displayCompleteTripData);
                    // Finally, close modal
                    $(".confirm").foundation('reveal', 'close');
                    
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log("Unable to delete. Trip " + errorThrown);
                }
            });
        });

        // add "DELETE" handler to confirmation modal
        $(".delete-confirm-milestone").on("click", function(ev) {
            // Send AJAX request to delete a trip
            $.ajax({
                type: "DELETE",
                async: true,
                headers: {
                    "X-Auth-Token": utilities.cookies.getItem("auth_token")
                },
                url: Config.baseUrl + "/milestones/" + self._confirmsDelete,
                success: function(data, textStatus, jqXHR) {
                    /* If request was sucessful: */
                    // check & execute callback
                    self._confirmsDelete.callback && self._confirmsDelete.callback();
                    create_roadtrip.getTripData(create_roadtrip.displayCompleteTripData);
                    // Finally, close modal
                    $(".confirm").foundation('reveal', 'close');
                    
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log("Unable to delete. Trip " + errorThrown);
                }
            });
        });

        // add "DELETE" handler to confirmation modal
        $(".delete-confirm-moment").on("click", function(ev) {
            // Send AJAX request to delete a trip
            $.ajax({
                type: "DELETE",
                async: true,
                headers: {
                    "X-Auth-Token": utilities.cookies.getItem("auth_token")
                },
                url: Config.baseUrl + "/moments/" + self._confirmsDelete,
                success: function(data, textStatus, jqXHR) {
                    /* If request was sucessful: */
                    // check & execute callback
                    self._confirmsDelete.callback && self._confirmsDelete.callback();
                    create_roadtrip.getTripData(create_roadtrip.displayCompleteTripData);
                    // Finally, close modal
                    $(".confirm").foundation('reveal', 'close');
                    
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log("Unable to delete. Trip " + errorThrown);
                }
            });
        });
    },

    // Contains details of item to be deleted, and an associated callback function. This method is called when Delete button is pushed on a confirm dialog box
    _confirmsDelete: null,
    _confirmsDeleteType: null,

    // all trip commons are handled here
    trip: function() {
        // handle like click
        $(document).on("click", ".like", function() {
            if (utilities.local_storage.get("id") !== null && typeof utilities.local_storage.get("id") !== "undefined" &&
                utilities.local_storage.get("id") !== "") {

                var tripId = common.getTripId(this);
                if ($(this).hasClass("nav-text")) {
                    $(this).removeClass("nav-text");
                    $(this).find('.icon-pxl').removeClass("icon-pxl-like-red").addClass("icon-pxl-like-gray");
                    $(this).find('.icon-pxl').removeClass("has-hover-like");

                    /**Liked**/
                    // AJAX call: un-like this trip
                    $.ajax({
                        type: "GET",
                        async: true,
                        headers: {
                            "X-Auth-Token": utilities.cookies.getItem("auth_token")
                        },
                        url: Config.baseUrl + "/trips/" + tripId + "/unlike",
                        success: function(data) {
                            $(".feed.trip-" + data.id).find("span.like").children(".likeCount").html(data.likes_count)
                        }
                    });
                } else {
                    $(this).addClass("nav-text");
                    $(this).find('.icon-pxl').addClass("icon-pxl-like-red");
                    $(this).find('.icon-pxl').addClass("has-hover-gray-like");
                    /** Un-Liked **/
                    // AJAX call: like this trip
                    $.ajax({
                        type: "GET",
                        async: true,
                        headers: {
                            "X-Auth-Token": utilities.cookies.getItem("auth_token")
                        },
                        url: Config.baseUrl + "/trips/" + tripId + "/like",
                        success: function(data) {
                            $(".feed.trip-" + data.id).find("span.like").children(".likeCount").html(data.likes_count)
                        }
                    });
                }
            }
            
        });

        // handle comment click
        $(document).on("click", ".comment", function() {
            if (utilities.local_storage.get("id") !== null && 
                typeof utilities.local_storage.get("id") !== "undefined" &&
                utilities.local_storage.get("id") !== "") {

                var tripId = common.getTripId(this);
                window.location = "road_trip_view.html?id=" + tripId;   
            }
            
        });

        // Display profile pics of users - sidebar
        // Fetch & display
        this._fetchUsersPics(this._displayUsersPics);
    },

    // Fetch pics of users for sidebar
    _fetchUsersPics: function(fetchCallback) {
        var self = this;
       // Fetch & display Currently Following People
        var userID = utilities.local_storage.get("id");
        if ( utilities.getURLVar("_loc") === "otherProfile" && 
            utilities.getURLVar("id") != utilities.local_storage.get("id")) {
            userID = utilities.getURLVar("id")
    }
    $.ajax({
       type: "GET",
        async: true,
        headers: {
              "X-Auth-Token": utilities.cookies.getItem("auth_token")
        },
        url: Config.baseUrl + "/users/" + userID + "/following_people",
        success: function(data, textStatus, jqXHR) {
        fetchCallback && fetchCallback.apply(self, [data]);
        },
        error: function(jqXHR, textStatus, errorThrown) {
        console.log("unable to fetch following people " + errorThrown);
        }
    });

    },

    // Display pics of users in sidebar
    _displayUsersPics: function(arrayOfImages) {
        //debugger;
        var picsInARow = 6,
            maxRows = 2,
            imgCount = arrayOfImages.length,
            $imagesContainer = $(".profile-pics"),
            addPic = function(imgObject) {
                $link = utilities.createElement("<a/>", $imagesContainer, {
                    'href': 'profile_view.html?_loc=otherProfile&id=' + imgObject.id,
                    'target': '_blank'
                });
                $overlay = utilities.createElement("<span/>", $link, {
                    'class': ' overlay'
                });
                utilities.createElement("<img/>", $link, {
                    'class': "following-image-link",
                    'src': imgObject.userPic || "../images/default_profile.png",
                    'alt': ""
                });
                $link.tooltipster({
                    position: 'top',
                    interactive: true,
                    content: imgObject.name || imgObject.username,
                });
            },
            i = 0;

        if (imgCount <= picsInARow) {
            // Total number of followers is less than or equal to total number of pics required in a row

            // loop through imgCount and display images
            for (; i < imgCount; i++) {
                addPic(arrayOfImages[i]);
            }
        } else if (imgCount > picsInARow) {
            // Total number of followers is greater than total number of pics required all rows

            // calculate number of images that should be displayed and number of remaining images
            var remainingCount = (imgCount + 1) % picsInARow,
                displayCount = imgCount - remainingCount,
                rows = Math.ceil(displayCount / picsInARow),
                rCount = 0;

            // If number of rows that are going to be displayed exceed the maximum number of allowed rows
            if (rows > maxRows) {
                var excessPics = (rows - maxRows) * picsInARow;
                displayCount -= excessPics;
                remainingCount += excessPics;
            }

            // loop through picsInARow and display images
            for (; rCount < rows; rCount++) {
                for (; i < displayCount; i++) {
                    addPic(arrayOfImages[i]);
                }
            }

            // Display number of excess images
            utilities.createElement("<span/>", $imagesContainer, {
                'class': "more left text-center",
                'html': "+" + remainingCount
            });
        }
    },

    // custom tooltip
    tooltip: function(content) {
        $('.tooltipster').tooltipster({
            'position': 'bottom',
            'interactive': true,
            'content': $('<span>Hoorey tooltip !<br> dsfdsafasfasfasdf<br> fa fsdasdafasdf sdfdsf </span><a href="#">More <span class="gt">></span></a>')
        });
    },

    g_search: function() {
        var self = this;

        if (!this.searchOverlay) this.searchOverlay = this.elasticSearch();

        $(".elasticSearchTrigger").on("mouseenter", function() {
            $(this).find("icon-pxl icon-pxl-search-black").addClass("has-hover-search-gray");
        });

        $(".elasticSearchTrigger").on("mouseleave", function() {
            $(this).find("icon-pxl icon-pxl-search-black").removeClass("has-hover-search-gray");
        });

        $(".elasticSearchTrigger").on("click", function() {
            if (self.searchOverlayState == 0) {
                // Set overflow of body to be hidden - fix for not scrolling
                //$("body").css("overflow", "hidden");

                // Add search overlay to body
                $("body").append(self.searchOverlay);
                self.searchOverlay.slideDown(400);

                // Change search icon to cross
                var searchIcon = $(this).find("span");
                searchIcon.removeClass("icon-pxl icon-pxl-search-black");
                searchIcon.addClass("icon-pxl icon-pxl-search-close-black");

                /*var searchIcon = $(this).find(".icon-search-default");
                searchIcon.removeClass("icon-search-default");
                searchIcon.addClass("icon-search-close");
                */
                // Focus on search textbox for quick typing
                self.searchOverlay.find(".place-search").focus();
            } else {
                // Change cross icon to search icon again
                var searchIcon = $(this).find("span");
                searchIcon.removeClass("icon-pxl icon-pxl-search-close-black");
                searchIcon.addClass("icon-pxl icon-pxl-search-black has-hover-search-gray");

                /*var searchIcon = $(this).find(".icon-search-close");
                searchIcon.removeClass("icon-search-close");
                searchIcon.addClass("icon-search-default");*/

                self.searchOverlay.slideUp(400, function() {
                    // Remove search overlay from body
                    self.searchOverlay.remove();

                    // Set overflow of body to be visible
                    //$("body").css("overflow", "visible");
                });
            }
            self.searchOverlayState ^= 1;
        });
    },

    searchOverlayState: 0,

    searchOverlay: null,

    // Create elastic search box/overlay
    elasticSearch: function() {
        var self = this;

        var $searchWrap = utilities.createElement("<div/>", null, {
            'class': "search-wrapper hidden"
        });
        var $searchHeading = utilities.createElement("<div/>", $searchWrap, {
            'class': "search-heading"
        });
        var $row = utilities.createElement("<div/>", $searchHeading, {
            'class': "row collapse"
        });
        var $col = utilities.createElement("<div/>", $row, {
            'class': "small-10 medium-10 large-10 column"
        });
        var $searchInput = utilities.createElement("<input/>", $col, {
            'class': "place-search main",
            'type': "search",
            'placeholder': "Search for places and road trips..."
        });
        $searchInput.on("keyup", Foundation.utils.throttle(function(e) {
            self.getSearchResults($(this).val(), $searchWrap, self.displaySearchResults);
        }, 300));

        $col = utilities.createElement("<div/>", $row, {
            'class': "small-1 medium-1 large-1 column"
        });
        var $searchBtn = utilities.createElement("<a/>", $col, {
            'class': "button search-btn",
            'href': "#"
        });
        utilities.createElement("<span/>", $searchBtn, {
            'class': 'icon-pxl icon-pxl-search-red'
        });
        $searchBtn.on("click", function() {
            self.getSearchResults($searchInput.val(), $searchWrap, self.displaySearchResults);
        });

        utilities.createElement("<div/>", null, {
            'class': "search-content"
        }).appendTo(utilities.createElement("<div/>", $searchWrap, {
            'class': "search-container"
        }));

        return $searchWrap;
    },

    getSearchResults: function(searchKeys, $search, fetchCallback) {
        var self = this;

        // Fetch search results
        $.ajax({
            type: "GET",
            async: true,
            headers: {
                "X-Auth-Token": utilities.cookies.getItem("auth_token")
            },
            data: {
                'searchq': searchKeys
            },
            url: Config.baseUrl + "/search",
            success: function(data, textStatus, jqXHR) {
                // data array should be received here in JSON format

                if (fetchCallback) {
                    fetchCallback.apply(self, [$search.find('.search-content'), data]);
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log("unable to fetch list of search results " + errorThrown);
            }
        });

        //if (fetchCallback) fetchCallback.apply(self, [$search.find('.search-content'), arrayOfResults]);
    },

    displaySearchResults: function($searchResultsContainer, searchResults) {
        var i = 0;

        $searchResultsContainer.html("");

        for (; i < searchResults.locations.length; i++) {
            $searchResultsContainer.append(this.createSearchResult(searchResults.locations[i], 1));
        }

        i = 0;

        for (; i < searchResults.trips.length; i++) {
            $searchResultsContainer.append(this.createSearchResult(searchResults.trips[i], 2));
        }
    },

    // Create an individual search result row
    createSearchResult: function(resultDetails, type) {
        console.log(resultDetails);
        switch (type) {
            case 1: // Locations
                resultDetails.additionalText = "";
                break;

            case 2: // Trips
                resultDetails.additionalText = ", " + resultDetails.first_milestone + " to " + resultDetails.last_milestone;
                break;
        }
        var $contentWrapper = utilities.createElement("<div/>", null, {
            'class': "row content-wrapper"
        })
        var $contentInnerWrapper = utilities.createElement("<div/>", $contentWrapper, {
            'class': "small-6 medium-6 large-6 column content-inner-wrapper"
        });
        if (resultDetails.name) {
            utilities.createElement("<div/>", $contentInnerWrapper, {
                'class': "search-place-result left",
                'html': resultDetails.name
            });

        }else if (resultDetails.title) {
            var $tripTitleContainer = utilities.createElement("<div/>", $contentInnerWrapper, {
                'class': "search-place-result left"
            });
            utilities.createElement("<a/>", $tripTitleContainer, {
                'href': resultDetails.trip_url,
                'class': "search-place-result left",
                'html': resultDetails.title
            });
            var $tripDetailsConatiner = utilities.createElement("<div/>", $contentInnerWrapper, {
                'class': "search-place-result-by",
                'html': "&nbsp;by " 
            });
            utilities.createElement("<a/>", $tripDetailsConatiner, {
                'href': '/html/profile_view.html?_loc=otherProfile&id=' + resultDetails.user_id,
                'class': "search-place-result-by-name",
                'html': resultDetails.username || resultDetails.screenName
            });
            utilities.createElement("<span/>", $tripDetailsConatiner, {
                'html': resultDetails.additionalText
            });
        }



        $col = utilities.createElement("<div/>", $contentWrapper, {
            'class': "small-5 medium-5 large-5 column content-inner-wrapper"
        });
        $col = utilities.createElement("<div/>", null, {
            'class': "large-12 column clearfix"
        }).appendTo(utilities.createElement("<div/>", $col, {
            'class': "row"
        }));
        switch (type) {
            case 1: // Locations
                this.createFollowButton($col, {
                    'data': {
                        'id': resultDetails.id
                    }
                }, {
                    'data': {
                        'id': resultDetails.id
                    }
                }, !resultDetails.is_followed, resultDetails.is_followed, true);
                utilities.createElement("<div/>", $col, {
                    'class': "place-info text-right right",
                    'html': resultDetails.roadtripsCount + " Roadtrips, " + resultDetails.followersCount + " Followers"
                });
                break;

            case 2: // Trips
                var $rightContainer = utilities.createElement("<div/>", $col, {
                    'class': "roadtrip-view text-right"
                });
                utilities.createElement("<a/>", $rightContainer, {
                    'href': resultDetails.trip_url,
                    'class': "roadtrip-view-link",
                    'html': 'View Roadtrip <span class="gt">&gt;</span>'
                });
                break;
        }

        return $contentWrapper;
    },

    // The standard follow button
    createFollowButton: function($container, followCallbackData, unfollowCallbackData, showFollow, showFollowing, isForLocation) {
        /*
         * Button Styles (Reference):
         *
         *
         * Following
         * <a href="#" class="hidden tiny button tiny-ex button-following radius right"><span class="icon-tick-following links-text "></span>&nbsp;Following</a>
         *
         * Follow
         * <a href="#" class="tiny button tiny-ex button-follow radius right"><span class="icon-plus "></span>&nbsp;&nbsp;&nbsp;Follow</a>
         *
         * Unfollow
         * <a href="#" class="hidden tiny button tiny-ex button-unfollow radius right"><span class="icon-cross-unfollow text-red"></span>&nbsp;&nbsp;&nbsp;unfollow</a>
         */
        var self = this;

        $container = $($container);

        // Follow button - Green with + icon
        var $followButton = utilities.createElement("<a/>", $container, {
            'href': "#",
            'class': (showFollow ? "" : "hidden ") + "tiny button tiny-ex button-follow radius right",
            'html': '<span class="icon-pxl icon-pxl-plus-white vam "></span>&nbsp;&nbsp;&nbsp;Follow'
        });
        $followButton.on("click", function(ev) {
            ev.preventDefault();

            // Hide the Follow button & show the Following/Unfollow button
            $(this).hide();
            $followingButton.fadeIn();

            // Method to be called when Follow button is clicked
            if (isForLocation) self.followLocationCallback.apply(self, [followCallbackData]);
            else self.followUserCallback.apply(self, [followCallbackData]);
        });

        var $followingButton = utilities.createElement("<a/>", $container, {
            'href': "#",
            'class': (showFollowing ? "" : "hidden ") + "tiny button tiny-ex button-following radius right",
            'style': "padding-top: 7px;",
            'html': '<span class="icon-pxl icon-pxl-tick-bold-green  links-text "></span>&nbsp;Following'
        });
        $followingButton.on("mouseover", function() {
            // On mouseover, 'Following' Button changes to 'Unfollow' Button
            $(this).html('<span class="icon-pxl icon-pxl-close-bold-red vam icon-text-red" style="margin-top: -3px;"></span>&nbsp;&nbsp;Unfollow').removeClass('button-following').addClass('button-unfollow');

            // Add method to handle Unfollow button click
            $(this).on("click", function(ev) {
                ev.preventDefault();

                // Hide the Following/Unfollow button & show the Follow button
                $(this).hide();
                $followButton.show();

                // Method to be called when Unfollow button is clicked
                if (isForLocation) self.unfollowLocationCallback.apply(self, [unfollowCallbackData]);
                else self.unfollowUserCallback.apply(self, [unfollowCallbackData]);
            });
        });
        $followingButton.on("mouseout", function() {
            // On mouseout, 'Unfollow' Button changes to 'Following' Button
            $(this).html('<span class="icon-pxl icon-pxl-tick-bold-green  links-text "></span>&nbsp;Following').removeClass('button-unfollow').addClass('button-following');

            // Remove Unfollow button click handler
            $(this).off("click");
        });
    },

    followUserCallback: function(callbackData) {
        // AJAX call to follow goes here
        $.ajax({
            type: "GET",
            async: true,
            headers: {
                "X-Auth-Token": utilities.cookies.getItem("auth_token")
            },
            url: Config.baseUrl + "/users/" + callbackData.data.id + "/follow",
            success: function(data, textStatus, jqXHR) {
                // user followed

                callbackData.method && callbackData.method();
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log("User not found " + errorThrown);
            }
        });
    },

    followLocationCallback: function(callbackData) {
        // AJAX call to follow goes here
        $.ajax({
            type: "GET",
            async: true,
            headers: {
                "X-Auth-Token": utilities.cookies.getItem("auth_token")
            },
            url: Config.baseUrl + "/locations/" + callbackData.data.id + "/follow",
            success: function(data, textStatus, jqXHR) {
                // location followed

                callbackData.method && callbackData.method(callbackData.data);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log("User not found " + errorThrown);
            }
        });
    },

    unfollowUserCallback: function(callbackData) {
        // AJAX call to unfollow goes here
        $.ajax({
            type: "GET",
            async: true,
            headers: {
                "X-Auth-Token": utilities.cookies.getItem("auth_token")
            },
            url: Config.baseUrl + "/users/" + callbackData.data.id + "/unfollow",
            success: function(data, textStatus, jqXHR) {
                // user un-followed

                callbackData.method && callbackData.method();
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log("User not found " + errorThrown);
            }
        });
    },

    unfollowLocationCallback: function(callbackData) {
        // AJAX call to unfollow goes here
        $.ajax({
            type: "GET",
            async: true,
            headers: {
                "X-Auth-Token": utilities.cookies.getItem("auth_token")
            },
            url: Config.baseUrl + "/locations/" + callbackData.data.id + "/unfollow",
            success: function(data, textStatus, jqXHR) {
                // user un-followed

                callbackData.method && callbackData.method(callbackData.data);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log("User not found " + errorThrown);
            }
        });
    },

    // Notification type can be one of - error/success/warning
    notify: function(json) {
        new PNotify({
            title: json.title,
            text: json.text,
            hide: true,
            delay: 5000,
            remove: true,
            type: json.type,
            animation: false,
            shadow: false,
            buttons: {
                closer: true,
                closer_hover: false
            }
        });

        window.setTimeout(function() {
            $(".ui-pnotify-container").fadeOut();
        }, 8000);
    },

    createRoadtripModal: function() {
        var self = this;

        var $modal = utilities.createElement("<div/>", null, {
            'class': "reveal-modal trip-popup small confirm",
            'id': "createNewRoadtripModal",
            'data-reveal': ""
        });
        utilities.createElement("<a/>", $modal, {
            'class': "close-reveal-modal",
            'html': "&#215;"
        });
        var $form = utilities.createElement("<form/>", $modal, {
            'action': "#",
            'id': "roadTripForm"
        });

        utilities.createElement("<div/>", $form, {
            'class': "row reveal-modal-heading",
            'html': '<div class="small-12 medium-12 large-12 columns"><div class="modal-heading">Add basic details to start</div></div>'
        });

        /* Started at section */
        var $row = utilities.createElement("<div/>", $form, {
            'class': "row"
        });
        var $col = utilities.createElement("<div/>", $row, {
            'class': "small-4 medium-4 large-4 columns"
        });
        utilities.createElement("<label/>", $col, {
            'class': "inline",
            'html': "Started at"
        });
        $col = utilities.createElement("<div/>", $row, {
            'class': "small-8 medium-8 large-8 columns"
        });
        utilities.createElement("<input/>", $col, {
            'type': "text",
            'placeholder': "Start typing to add location",
            'name': "start_point",
            'id': "start_point"
        });

        // Start Location Error
        $row = utilities.createElement("<div/>", $form, {
            'class': "row modal-alert-sec alert-error __c_start_location_error",
            'style': "display: none;"
        });
        $col = utilities.createElement("<div/>", $row, {
            'class': "small-12 medium-12 large-12 columns"
        });
        var $section = utilities.createElement("<div/>", $col, {
            'class': "section has-no-border",
            'html': 'Please enter a valid start location. If you need any help, <a href="faq.html" class="link-text link-error">check FAQ</a>'
        });

        /* End at section */
        $row = utilities.createElement("<div/>", $form, {
            'class': "row"
        });
        $col = utilities.createElement("<div/>", $row, {
            'class': "small-4 medium-4 large-4 columns"
        });
        utilities.createElement("<label/>", $col, {
            'class': "inline",
            'html': "End Destination"
        });
        $col = utilities.createElement("<div/>", $row, {
            'class': "small-8 medium-8 large-8 columns"
        });
        utilities.createElement("<input/>", $col, {
            'type': "text",
            'placeholder': "Start typing to add location",
            'name': "end_point",
            'id': "end_point"
        });

        // End Location Error
        $row = utilities.createElement("<div/>", $form, {
            'class': "row modal-alert-sec alert-error __c_end_location_error",
            'style': "display: none;"
        });
        $col = utilities.createElement("<div/>", $row, {
            'class': "small-12 medium-12 large-12 columns"
        });
        var $section = utilities.createElement("<div/>", $col, {
            'class': "section has-no-border",
            'html': 'Please enter a valid end destination. If you need any help, <a href="faq.html" class="link-text link-error">check FAQ</a>'
        });


        /* Map section */
        $row = utilities.createElement("<div/>", $form, {
            'class': "row"
        });
        $col = utilities.createElement("<div/>", $row, {
            'class': "small-12 medium-12 large-12 columns no-padding"
        });
        utilities.createElement("<div/>", $col, {
            'class': "map-container",
            'id': "modalMap-1",
            'style': "width: 100%; height: 200px;"
        });

        /* Transport mode */
        $row = utilities.createElement("<div/>", $form, {
            'class': "row"
        });
        $col = utilities.createElement("<div/>", $row, {
            'class': "small-4 medium-4 large-4 columns"
        });
        utilities.createElement("<label/>", $col, {
            'class': "inline",
            'html': "Transport Mode"
        });
        $col = utilities.createElement("<div/>", $row, {
            'class': "small-8 medium-8 large-8 columns"
        });

        //var $transportMode = utilities.createElement("<select/>", $col, { 'name': "transport_mode" });

        // Custom Dropdown
        var $selectWrapper = utilities.createElement("<div/>", $col, {
            'class': "c-select-wrapper"
        });
        var $selected = utilities.createElement("<div/>", $selectWrapper, {
            'class': "selected"
        });
        utilities.createElement("<span/>", $selected, {
            'html': "Select transport mode"
        });
        utilities.createElement("<input/>", $selected, {
            'type': "hidden",
            'name': "transport_mode",
            'value': "0"
        });
        utilities.createElement("<span/>", $selected, {
            'html': '<img src="http://i.imgur.com/NJnqd5w.png" alt="arrow" />'
        });
        var $optionsWrapper = utilities.createElement("<div/>", $selectWrapper, {
            'class': "c-options-wrapper"
        });
        var $transportMode = utilities.createElement("<ul/>", $optionsWrapper, {
            'class': "__c_transport_modes"
        });
        utilities.fillDropDown4($transportMode, self.transportModes, "name", "value");
        //this.bindCustomDropdown($selectWrapper);

        // Start transport Error
        $row = utilities.createElement("<div/>", $form, {
            'class': "row modal-alert-sec alert-error __c_transport_mode_error",
            'style': "display: none;"
        });
        $col = utilities.createElement("<div/>", $row, {
            'class': "small-12 medium-12 large-12 columns"
        });
        var $section = utilities.createElement("<div/>", $col, {
            'class': "section has-no-border",
            'html': 'Please select a transport mode. If you need any help, <a href="faq.html" class="link-text link-error">check FAQ</a>'
        });

        /* End at section */

        /*trip details*/
        $row = utilities.createElement("<div/>", $form, {
            'class': "row collapse",
            'id' : "dateRow"
        });
        $col = utilities.createElement("<div/>", $row, {
            'class': "small-6 medium-6 large-6 columns"
        });
        var $row_inner = utilities.createElement("<div/>", $col, {
            'class': "row"
        });
        var $col_inner = utilities.createElement("<div/>", $row_inner, {
            'class': "small-6 medium-6 large-6 columns"
        });
        utilities.createElement("<label/>", $col_inner, {
            'class': "inline",
            'html': "Start Date"
        });
        $col_inner = utilities.createElement("<div/>", $row_inner, {
            'class': "small-6 medium-6 large-6 columns datepicker-wrapper padding-left-zero"
        });
        utilities.createElement("<input/>", $col_inner, {
            'type': "text",
            'class': "startDate",
            'id': "basicDetailsDp1",
            'placeholder': "dd/mm/yyyy",
            'name': "start_date",
            'data-date-format': 'dd/mm/yyyy'
        });

        $col = utilities.createElement("<div/>", $row, {
            'class': "small-6 medium-6 large-6 columns"
        });
        $row_inner = utilities.createElement("<div/>", $col, {
            'class': "row"
        });
        $col_inner = utilities.createElement("<div/>", $row_inner, {
            'class': "small-6 medium-6 large-6 columns"
        });
        utilities.createElement("<label/>", $col_inner, {
            'class': "inline",
            'html': "End Date"
        });
        $col_inner = utilities.createElement("<div/>", $row_inner, {
            'class': "small-6 medium-6 large-6 columns datepicker-wrapper padding-left-zero"
        });
        utilities.createElement("<input/>", $col_inner, {
            'type': "text",
            'class': "endDate",
            'id': "basicDetailsDp2",
            'placeholder': "dd/mm/yyyy",
            'name': "end_date",
            'data-date-format': 'dd/mm/yyyy'
        });

        /* Started at section */
        var $row = utilities.createElement("<div/>", $form, {
            'class': "row",
            'style': "display:none;",
            'id' : "numberOfDaysRow"
        });
        var $col = utilities.createElement("<div/>", $row, {
            'class': "small-4 medium-4 large-4 columns"
        });
        utilities.createElement("<label/>", $col, {
            'class': "inline",
            'html': "No. of Days"
        });
        $col = utilities.createElement("<div/>", $row, {
            'class': "small-8 medium-8 large-8 columns"
        });
        utilities.createElement("<input/>", $col, {
            'type': "text",
            'placeholder': "Enter Number of Days",
            'name': "duration",
            'id': "duration"
        });

        // Date Select Error
        $row = utilities.createElement("<div/>", $form, {
            'class': "row modal-alert-sec alert-error __c_date_error",
            'style': "display: none;"
        });
        $col = utilities.createElement("<div/>", $row, {
            'class': "small-12 medium-12 large-12 columns"
        });
        var $section = utilities.createElement("<div/>", $col, {
            'class': "section has-no-border",
            'html': 'Please enter a valid date or duration. If you need any help, <a href="faq.html" class="link-text link-error">check FAQ</a>'
        });

        $row = utilities.createElement("<div/>", $form, {
            'class': "row modal-info",
            'id' : "showNumberOfDays"
        });
        $col = utilities.createElement("<div/>", $row, {
            'class': "small-12 medium-12 large-12 columns"
        });
        utilities.createElement("<div/>", $col, {
            'class': "section modal-info-text",
            'html': '<a href="#">Want to add number of days instead ?</a>'
        });

        $row = utilities.createElement("<div/>", $form, {
            'class': "row modal-info",
            'id' : "showSelectDates",
            'style' : "display:none"
        });
        $col = utilities.createElement("<div/>", $row, {
            'class': "small-12 medium-12 large-12 columns"
        });
        utilities.createElement("<div/>", $col, {
            'class': "section modal-info-text",
            'html': '<a href="#">Want to select dates instead ?</a>'
        });

        var $footer = utilities.createElement("<div/>", $form, {
            'class': "modal-footer"
        });
        var $button = utilities.createElement("<button/>", $footer, {
            'class': "button button-fill has-border-bottom",
            'type': "button",
            'html': '<span class="icon-pxl icon-pxl-tick-thin-white"></span>&nbsp;CREATE ROADTRIP'
        });

        utilities.createElement("<input/>", $form, {
            'type': "hidden",
            'name': "hasErrors",
            'class': "__c_errorFlag",
            'value': 1
        });

        $button.on("click", function() {
            var postdata = utilities.formToObject($form);

            // Hide all validation messages
            $(".__c_date_error,__c_transport_mode_error,__c_start_location_error,__c_end_location_error").hide();

            if (postdata["start_point"].length == 0)  {
                $(".__c_start_location_error").show();
                postdata.hasErrors = 1;
            }

            if (postdata["end_point"].length == 0)  {
                $(".__c_end_location_error").show();
                postdata.hasErrors = 1;
            }

            if (postdata["transport_mode"].length == 0) {
                $(".__c_transport_mode_error").show();
                postdata.hasErrors = 1;
            }

            if (postdata["start_date"].length > 0 && postdata["end_date"].length > 0) {
                try {
                    if(postdata["duration"] == "") {
                        postdata["start_date"] = utilities.getFormattedDate(postdata["start_date"]);
                        postdata["end_date"] = utilities.getFormattedDate(postdata["end_date"]);
                    }
                } catch (ex) {
                    $(".__c_date_error").show();
                    postdata.hasErrors = 1;
                } 

                if (postdata["duration"] == "" && postdata["start_date"] == "" && postdata["end_date"] == "") {
                    $(".__c_date_error").show();
                    postdata.hasErrors = 1;
                }
            } else {
                if( postdata["duration"].length == 0 || postdata["duration"] == 0 || postdata["duration"] == "0") {
                    $(".__c_date_error").show();
                    postdata.hasErrors = 1;
                }
            }

            if (postdata.hasErrors == 1) return;
            else delete postdata.hasErrors;

            // Hide all validation messages
            $(".__c_date_error,__c_transport_mode_error,__c_start_location_error,__c_end_location_error").hide();

            self.createNewRoadTrip(postdata);
        });

        return $modal;
    },

    // Create a new roadtrip - The Modal Request
    createNewRoadTrip: function(postdata) {
        $.ajax({
            type: "POST",
            async: true,
            headers: {
                "X-Auth-Token": utilities.cookies.getItem("auth_token")
            },
            data: {
                "trip": postdata
            },
            url: Config.baseUrl + "/trips",
            success: function(data) {
                window.location = "create_road_trip.html?id=" + data.id
            },
            error: function() {
                console.log("Unable to create a new roadtrip.");
            }
        });
    },

    createModalMap: function() {
        var map = utilities.createGMap(document.getElementById("modalMap-1")),
            startMarker = null,
            endMarker = null,
            startValidation = {
                error: function() {
                    setFlag(1);
                    $(".__c_start_location_error").show();
                },

                success: function() {
                    setFlag(0);
                    $(".__c_start_location_error").hide();
                }
            },
            endValidation = {
                error: function() {
                    setFlag(1);
                    $(".__c_end_location_error").show();
                },

                success: function() {
                    setFlag(0);
                    $(".__c_end_location_error").hide();
                }
            },
            setFlag = function(value) {
                $(".__c_errorFlag").val(value);
            };

        var momentStartAutocomplete = utilities.setAutocompleteSearch(map, document.getElementById("start_point"));
        var addMarker = function(latLng, map) {
            //@latLng: new google.maps.LatLng(latitude, longitude)
            return new google.maps.Marker({
                'position': latLng,
                'map': map,
                'draggable': false
            })
        };
        google.maps.event.addListener(momentStartAutocomplete, 'place_changed', function() {
            var place = momentStartAutocomplete.getPlace();

            if (!place.geometry) {
                return;
            }
            startValidation.success();

            if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
            } else {
                map.setCenter(place.geometry.location);
                map.setZoom(17); // Why 17? Because it looks good.
            }
            startMarker = addMarker(place.geometry.location, map);
        });

        $("#start_point").on("change", function() {
            if ($(this).val() == "") startMarker = null;
        });

        $("#start_point").on("blur", function() {
            if (!startMarker) {
                startValidation.error();
            } else {
                startValidation.success();
            }
        });

        var momentEndAutocomplete = utilities.setAutocompleteSearch(map, document.getElementById("end_point"));
        google.maps.event.addListener(momentEndAutocomplete, 'place_changed', function() {
            var place = momentEndAutocomplete.getPlace();
            if (!place.geometry) {
                return;
            }
            endValidation.success();

            if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
            } else {
                map.setCenter(place.geometry.location);
                map.setZoom(17); // Why 17? Because it looks good.
            }
            endMarker = addMarker(place.geometry.location, map);
        });

        $("#end_point").on("change", function() {
            if ($(this).val() == "") endMarker = null;
        });

        $("#end_point").on("blur", function() {
            if (!endMarker) {
                endValidation.error();
            } else {
                endValidation.success();
            }
        });

        google.maps.event.trigger(map, "resize");
    },

    getTripId: function(el) {
        return $(el).closest(".feed").data("trip-id")
    },

    transportModes: [{
        'value': "1",
        'name': "Car",
        'iconClass': "icon-pxl-transport-mode-car"
    }, {
        'value': "2",
        'name': "Motorbike",
        'iconClass': "icon-pxl-transport-mode-bike"
    }, {
        'value': "3",
        'name': "Bicycle",
        'iconClass': "icon-pxl-transport-mode-bycyle"
    }, {
        'value': "4",
        'name': "Bus",
        'iconClass': "icon-pxl-transport-mode-bus"
    }, {
        'value': "5",
        'name': "Other",
        'iconClass': "icon-pxl-transport-mode-bike"
    }],

    roadConditions: [{
        'value': "1",
        'name': "Trail"
    }, {
        'value': "2",
        'name': "Bad"
    }, {
        'value': "3",
        'name': "Average"
    }, {
        'value': "4",
        'name': "Good"
    }, {
        'value': "5",
        'name': "Great!"
    }]
};